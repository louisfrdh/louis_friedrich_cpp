#include "triangle.h"
#include <iostream>
#include <cmath>

// constructeurs
Triangle::Triangle()
{
    m_a = *new Point();
    m_b = *new Point();
    m_c = *new Point();
}

Triangle::Triangle(const Point &a, const Point &b, const Point &c)
{
    m_a = a;
    m_b = b;
    m_c = c;
}

// setters
void Triangle::SetA(const Point &value)
{
    m_a = value;
}

void Triangle::SetB(const Point &value)
{
    m_b = value;
}

void Triangle::SetC(const Point &value)
{
    m_c = value;
}

// méthodes et fonctions
void Triangle::Afficher()
{
    std::cout << "Triangle defini par les points : " << std::endl
              << GetA().GetCoordonnees()
              << GetB().GetCoordonnees()
              << GetC().GetCoordonnees() << std::endl;
    std::cout << "\t base : " << GetLongueurBase() << std::endl
              << "\t hauteur : " << GetLongueurHauteur() << std::endl
              << "\t surface : " << GetSurface() << std::endl;
    std::cout << "\t cotes : "
              << getLongueurCote(GetA(),GetB()) << " - "
              << getLongueurCote(GetA(),GetC()) << " - "
              << getLongueurCote(GetB(),GetC()) << std::endl;
    if(IsRectangle()) std::cout << "\t = triangle rectangle" << std::endl;
    if(IsIsocele()) std::cout << "\t = triangle isocele" << std::endl;
    if(IsEquilateral()) std::cout << "\t = triangle equilateral" << std::endl;
}

double Triangle::GetLongueurBase()
{
    double max;
    Point a = GetA();
    Point b = GetB();
    Point c = GetC();
    double coteAB = getLongueurCote(a,b);
    double coteBC = getLongueurCote(c,b);
    double coteAC = getLongueurCote(a,c);
    max = coteAB > coteBC ? coteAB : coteBC;
    max = max > coteAC ? max : coteAC;
    return max;
}

double Triangle::GetLongueurHauteur()
{
    return 2*GetSurface()/GetLongueurBase();
}

double Triangle::GetSurface()
{
    Point a = GetA();
    Point b = GetB();
    Point c = GetC();
    double coteAB = getLongueurCote(a,b);
    double coteBC = getLongueurCote(c,b);
    double coteAC = getLongueurCote(a,c);
    // Forumule de Heron
    double demiPerim = (coteAB + coteAC + coteBC)/2;
    return std::sqrt(demiPerim*(demiPerim-coteAB)
                              *(demiPerim-coteBC)
                              *(demiPerim-coteAC));
}

bool Triangle::IsIsocele()
{
    return (getLongueurCote(GetA(),GetB()) == getLongueurCote(GetA(),GetC()) ||
            getLongueurCote(GetA(),GetB()) == getLongueurCote(GetB(),GetC()) ||
            getLongueurCote(GetA(),GetC()) == getLongueurCote(GetB(),GetC()));
    /*return GetLongueurBase()*GetLongueurHauteur()/2 ==GetSurface();*/
}

bool Triangle::IsRectangle()
{
    /*return pow(GetLongueurBase(),2) == pow(getLongueurCote(GetA(),GetB()),2)
                                     + pow(getLongueurCote(GetA(),GetC()),2) ||
           pow(GetLongueurBase(),2) == pow(getLongueurCote(GetA(),GetB()),2)
                                     + pow(getLongueurCote(GetB(),GetC()),2) ||
           pow(GetLongueurBase(),2) == pow(getLongueurCote(GetA(),GetC()),2)
                                     + pow(getLongueurCote(GetB(),GetC()),2);*/
    return (GetSurface() == getLongueurCote(GetA(),GetB()) * getLongueurCote(GetA(),GetC()) / 2 ||
            GetSurface() == getLongueurCote(GetA(),GetB()) * getLongueurCote(GetB(),GetC()) / 2 ||
            GetSurface() == getLongueurCote(GetA(),GetA()) * getLongueurCote(GetB(),GetC()) / 2);
}

bool Triangle::IsEquilateral()
{
    return (getLongueurCote(GetA(),GetB()) == getLongueurCote(GetA(),GetC()) &&
            getLongueurCote(GetA(),GetB()) == getLongueurCote(GetB(),GetC()));
}

double Triangle::getLongueurCote(Point a, Point b)
{
    return std::sqrt( pow(b.x-a.x, 2) + pow(b.y-a.y, 2) );
}

