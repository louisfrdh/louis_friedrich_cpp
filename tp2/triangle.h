#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "point.h"

class Triangle
{
public:
    Triangle();
    Triangle(const Point &a, const Point &b, const Point &c) ;

    inline Point GetA(){return m_a;}
    inline Point GetB(){return m_b;}
    inline Point GetC(){return m_c;}

    void SetA(const Point &value);
    void SetB(const Point &value);
    void SetC(const Point &value);

    void Afficher();
    double GetLongueurBase();
    double GetLongueurHauteur();
    double GetSurface();
    bool IsIsocele();
    bool IsRectangle();
    bool IsEquilateral();

private:
    Point m_a, m_b, m_c;
    double getLongueurCote(Point a, Point b);
};


#endif // TRIANGLE_H
