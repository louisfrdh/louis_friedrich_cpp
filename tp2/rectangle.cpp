#include "rectangle.h"
#include <iostream>

// constructeurs
Rectangle::Rectangle()
{
    m_coinHautGauche = *(new Point());
    m_longueur = 1;
    m_largeur = 1;
}

Rectangle::Rectangle(double longueur, double largeur)
{
    m_coinHautGauche = *(new Point());
    Rectangle::SetLongueur(longueur);
    Rectangle::SetLargeur(largeur);
}

Rectangle::Rectangle(Point coinHautGauche, double longueur, double largeur)
{
    m_coinHautGauche = coinHautGauche;
    Rectangle::SetLongueur(longueur);
    Rectangle::SetLargeur(largeur);
}

// setters
void Rectangle::SetLongueur(double longueur)
{
    m_longueur = longueur > 0 ? longueur : 1;
}
void Rectangle::SetLargeur(double largeur)
{
    m_largeur = largeur > 1 ? largeur : 1;
}
void Rectangle::SetCoinHautGauche(const Point &coinHautGauche)
{
    m_coinHautGauche = coinHautGauche;
}

// méthodes et fonctions
void Rectangle::Afficher()
{
    std::cout << "Rectangle de longueur " << GetLongueur()
              << ", de largeur " << GetLargeur() << std::endl;
    std::cout << "\t coin haut gauche : " << GetCoinHautGauche().GetCoordonnees() << std::endl;
    std::cout << "\t aire : " << GetArea() << " / perimetre : " << GetPerimeter() << std::endl;
}

double Rectangle::GetArea()
{
    return m_longueur * m_largeur;
}

double Rectangle::GetPerimeter()
{
    return m_longueur*2 + m_largeur*2;
}

