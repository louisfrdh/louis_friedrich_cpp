TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
        rectangle.cpp \
        triangle.cpp

HEADERS += \
    point.h \
    rectangle.h \
    triangle.h
