#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "point.h"


class Rectangle
{
public:
    Rectangle();
    Rectangle(double longueur, double largeur);
    Rectangle(Point coinGauche, double longueur, double largeur);

    inline double GetLongueur(){return m_longueur;}
    inline double GetLargeur(){return m_largeur;}
    inline Point GetCoinHautGauche(){return m_coinHautGauche;}

    void SetLongueur(double longueur);
    void SetLargeur(double largeur);
    void SetCoinHautGauche(const Point &coinHautGauche);

    void Afficher();
    double GetArea();
    double GetPerimeter();

private:
    double m_longueur, m_largeur;
    Point m_coinHautGauche;
};

#endif // RECTANGLE_H
