#include <iostream>
#include "rectangle.h"
#include "triangle.h"

int main()
{
    Point p1;
    Point p2(2,3);

    Rectangle r1;
    Rectangle r2(8,4);
    Rectangle r3(p2,5,3);

    // triangles rectangle, isocele, et equilateral
    Triangle t1(*new Point(0,0), *new Point(0,8), *new Point(4,0));
    Triangle t2(*new Point(4,4), *new Point(0,4), *new Point(4,0));
    Triangle t3(*new Point(1,1), *new Point(2,2), *new Point(3,1));

    std::cout << p1.GetCoordonnees() << p2.GetCoordonnees() << std::endl;
    r1.Afficher();
    r2.Afficher();
    r3.Afficher();
    t1.Afficher();
    t2.Afficher();
    t3.Afficher();

    return 0;
}
