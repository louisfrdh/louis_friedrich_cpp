#ifndef POINT_H
#define POINT_H

#include <string>

struct Point
{
    int x, y;
    Point()
    {
        x = 0;
        y = 0;
    }
    Point(int a, int b)
    {
        x = a;
        y = b;
    }
    std::string GetCoordonnees()
    {
        return "{" + std::to_string(x) + ";" + std::to_string(y) + "}";
    }
};

#endif // POINT_H
