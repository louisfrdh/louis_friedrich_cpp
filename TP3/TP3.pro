TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        case.cpp \
        colonne.cpp \
        diagonale.cpp \
        ensemblelineairecases.cpp \
        grille.cpp \
        grillemorpion.cpp \
        grillepuissance4.cpp \
        jeu.cpp \
        ligne.cpp \
        main.cpp

HEADERS += \
    case.h \
    colonne.h \
    diagonale.h \
    ensemblelineairecases.h \
    grille.h \
    grillemorpion.h \
    grillepuissance4.h \
    jeu.h \
    ligne.h
