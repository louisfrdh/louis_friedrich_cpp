#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H

#include "grille.h"

class GrilleMorpion : public Grille
{
public:
    GrilleMorpion();
    const static int TAILLE = 3;
    void DeposerJeton(int i, int idJoueur);
    bool VictoireJoueur(int idJoueur);
};

#endif // GRILLEMORPION_H
