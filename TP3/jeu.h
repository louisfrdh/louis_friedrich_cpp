#ifndef JEU_H
#define JEU_H

#include <iostream>
#include <vector>
#include "grillemorpion.h"

class Jeu
{
public:
//    enum TypeJeu {Morpion, Puissance4};
    Jeu(Grille* grilleJeu);
    void Jouer();

private:
//    std::unique_ptr<Grille> grille;
    Grille* grille;
    bool tourJoueur(int idJoueur);
    bool testFin();
};

#endif // JEU_H
