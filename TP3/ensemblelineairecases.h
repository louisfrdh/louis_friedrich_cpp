#ifndef ENSEMBLELINEAIRECASES_H
#define ENSEMBLELINEAIRECASES_H

#include "case.h"
#include <vector>
#include <memory>

class EnsembleLineaireCases
{
public:
    EnsembleLineaireCases();
    std::vector<std::shared_ptr<Case>> Cases;
    bool Complete(int idJoueur);
    bool ValidePuissance4(int idJoueur);
private:
    int longueur;
};

#endif // ENSEMBLELINEAIRECASES_H
