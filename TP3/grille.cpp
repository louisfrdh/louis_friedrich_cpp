#include "grille.h"
#include <iostream>

Grille::Grille(int nbLignes, int nbColonnes)
{
    this->nbLignes = nbLignes;
    this->nbColonnes = nbColonnes;
    iniVectors();
    iniCases();
}

bool Grille::CaseVide(int i)
{
    return cases[i]->value == 0;
}

bool Grille::CaseVide(int i, int j)
{
    return lignes[i].Cases[j]->value == 0;
}

bool Grille::LigneComplete(int indiceLigne, int idJoueur)
{
    if(indiceLigne <= (int)lignes.size()) return lignes[indiceLigne].Complete(idJoueur);
    else return false;
}

bool Grille::ColonneComplete(int indiceColonne, int idJoueur)
{
    if(indiceColonne <= (int)colonnes.size()) return colonnes[indiceColonne].Complete(idJoueur);
    else return false;
}

bool Grille::DiagonaleComplete(const Diagonale& diagonale, int idJoueur)
{
    for(std::shared_ptr<Case> c : diagonale.Cases)
    {
        if (c->value != idJoueur) return false;
    }
    return true;
}

bool Grille::Pleine()
{
    for(std::shared_ptr<Case> c : cases)
    {
        if (c->value == 0) return false;
    }
    return true;
}

void Grille::Afficher()
{
    for(Ligne l : lignes)
    {
        for(int i=0; i<nbColonnes; i++) std::cout << "--";
        std::cout << std::endl << "|";
        for(std::shared_ptr<Case> c : l.Cases)
        {
            std::cout << c->value << "|";
        }
        std::cout << std::endl;
    }
    for(int i=0; i<nbColonnes; i++) std::cout << "--";
    std::cout << std::endl;
}

int Grille::NombreCases()
{
    return cases.size();
}

void Grille::iniVectors()
{
    lignes.assign(nbLignes, Ligne());
    colonnes.assign(nbColonnes, Colonne());
    diagonales.assign(nbLignes+nbColonnes-1, Diagonale());
}

void Grille::iniCases()
{
    for(int i=0; i<nbLignes; i++)
    {
        for(int j=0; j<nbColonnes; j++)
        {
            std::shared_ptr<Case> sptrCase = std::make_shared<Case>(Case(i, j));
            cases.push_back(sptrCase);
            lignes[i].Cases.push_back(sptrCase);
            colonnes[j].Cases.push_back(sptrCase);
            diagonales[i+j].Cases.push_back(sptrCase);
        }
    }
}
