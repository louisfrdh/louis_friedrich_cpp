#ifndef CASE_H
#define CASE_H

class Case
{
public:
    Case(int i, int j);
    // valeur attribuée à la case
    int value;
private:
    // coordonnées de la case
    int x,y;
};

#endif // CASE_H
