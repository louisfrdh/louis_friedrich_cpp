#include "jeu.h"

Jeu::Jeu(Grille* grilleJeu) : grille(grilleJeu) {}

void Jeu::Jouer()
{
    std::cout << "Debut du jeu !" << std::endl;
    bool fini;
    do {
        fini = tourJoueur(1);
        if(!fini) fini = tourJoueur(2);
    } while(!fini);
}

bool Jeu::tourJoueur(int idJoueur)
{
    int numCase;
    do{
        std::cout << "J" << idJoueur << " renseignez la case dans laquelle vous souhaitez" <<
                     " jouer (indices de 0 a 8) >>>" << std::endl;
        std::cin >> numCase;
    } while (numCase < 0 || numCase > grille->NombreCases());
    grille->DeposerJeton(numCase, idJoueur);
    grille->Afficher();
    return testFin();
}

bool Jeu::testFin()
{
    if(grille->VictoireJoueur(1))
    {
        std::cout << "J1 remporte la partie !" << std::endl;
        return true;
    }
    if(grille->VictoireJoueur(2))
    {
        std::cout << "J2 remporte la partie !" << std::endl;
        return true;
    }
    if(grille->Pleine())
    {
        std::cout << "Match nul !" << std::endl;
        int veutRejouer;
        std::cout << "Entrez \"1\" si vous souhaitez rejouez" << std::endl;
        std::cin >> veutRejouer;
        if(veutRejouer == 1) Jeu(new GrilleMorpion()).Jouer();
        return true;
    }
    return false;
}
