#ifndef GRILLE_H
#define GRILLE_H

#include "case.h"
#include "ligne.h"
#include "colonne.h"
#include "diagonale.h"
#include <vector>
#include <memory>


class Grille
{
public:
    Grille(int nbLignes, int nbColonnes);
    bool CaseVide(int i);
    bool CaseVide(int i, int j);
    bool LigneComplete(int indiceLigne, int idJoueur);
    bool ColonneComplete(int indiceColonne, int idJoueur);
    bool DiagonaleComplete(const Diagonale& diagonale, int idJoueur);
    bool Pleine();
    virtual void DeposerJeton(int i, int idJoueur) = 0;
    virtual bool VictoireJoueur(int idJoueur) = 0;
    void Afficher();
    int NombreCases();
protected:
    std::vector<std::shared_ptr<Case>> cases;
    std::vector<Ligne> lignes;
    std::vector<Colonne> colonnes;
    std::vector<Diagonale> diagonales;
private:
    int nbLignes, nbColonnes;
    void iniVectors();
    void iniCases();
};

#endif // GRILLE_H
