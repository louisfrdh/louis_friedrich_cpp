#include "grillepuissance4.h"

GrillePuissance4::GrillePuissance4() : Grille(NB_LIGNES, NB_COLONNES)
{

}

void GrillePuissance4::DeposerJeton(int i, int idJoueur)
{
    if(idJoueur != 1 && idJoueur != 2) return;
    for(int j=colonnes[i].Cases.size()-1; j>=0 ;j--)
    {
        if(colonnes[i].Cases[j]->value == 0)
        {
            colonnes[i].Cases[j]->value = idJoueur;
            return;
        }
    }
}

bool GrillePuissance4::VictoireJoueur(int idJoueur)
{
    if (idJoueur != 1 && idJoueur != 2) return false;
    for(Colonne colonne : colonnes)
    {
        return colonne.ValidePuissance4(idJoueur);
    }
    for(Ligne ligne : lignes)
    {
        return ligne.ValidePuissance4(idJoueur);
    }
    for(Diagonale diago : diagonales)
    {
        return diago.ValidePuissance4(idJoueur);
    }
    return false;
}

