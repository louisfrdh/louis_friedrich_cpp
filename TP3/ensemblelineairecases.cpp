#include "ensemblelineairecases.h"

EnsembleLineaireCases::EnsembleLineaireCases()
{
}

bool EnsembleLineaireCases::Complete(int idJoueur)
{
    for(std::shared_ptr<Case> c : Cases)
    {
        if (c->value != idJoueur ) return false;
    }
    return true;
}

bool EnsembleLineaireCases::ValidePuissance4(int idJoueur)
{
    int cpt = 0;
    for(std::shared_ptr<Case> c : Cases)
    {
        if(c->value == idJoueur) cpt++;
        else cpt = 0;
        if(cpt >= 4) return true;
    }
    return false;
}
