#ifndef GRILLEPUISSANCE4_H
#define GRILLEPUISSANCE4_H

#include "grille.h"

class GrillePuissance4 : public Grille
{
public:
    GrillePuissance4();
    const static int NB_COLONNES = 7;
    const static int NB_LIGNES = 4;
    void DeposerJeton(int i, int idJoueur);
    bool VictoireJoueur(int idJoueur);
};

#endif // GRILLEPUISSANCE4_H
