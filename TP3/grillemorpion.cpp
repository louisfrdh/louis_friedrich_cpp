#include "grillemorpion.h"

GrilleMorpion::GrilleMorpion() : Grille(TAILLE, TAILLE)
{

}

void GrilleMorpion::DeposerJeton(int i, int idJoueur)
{
    if(CaseVide(i) && (idJoueur == 1 || idJoueur == 2))
    {
        cases[i]->value = idJoueur;
    }
}

bool GrilleMorpion::VictoireJoueur(int idJoueur)
{
    if (idJoueur != 1 && idJoueur != 2) return false;
    for(int i=0; i<TAILLE; i++)
    {
        if(LigneComplete(i, idJoueur)) return true;
        if(ColonneComplete(i, idJoueur)) return true;
    }
    for(Diagonale diago : diagonales)
    {
        if(diago.Cases.size() == TAILLE)
        {
            if(DiagonaleComplete(diago, idJoueur)) return true;
        }
    }
    return false;
}
