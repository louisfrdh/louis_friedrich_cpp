#include <cstdlib>
#include <iostream>
#include <time.h>
#include "ex1.h"

using namespace std;

int Somme(int a, int b)
{
    return a + b;
}

void Inverse(int* pa, int* pb)
{
    int tmp = *pa;
    *pa = *pb;
    *pb = tmp;
}

void Somme(int a, int b, int* pc)
{
    *pc = Somme(a, b);
}

void TriTab()
{
    srand (time(NULL));
    int* tab = new int[5];
    for(int i=0 ; i<5 ; i++)
    {
        do
        {
            tab[i] = rand() % 100;
        }
        while(tab[i] < 0);
    }

    cout << "Tableau dans le desordre : " << endl;
    for(int i=0 ; i<5 ; i++)
    {
        cout << tab[i] << ' ';
    }
    cout << endl;

    for(int i=0 ; i<5 ; i++)
    {
        for(int j=0 ; j<5 ; j++)
        {
            if(tab[j] > tab[i]) Inverse(&tab[i], &tab[j]);
        }
    }

    cout << "Tableau dans l'ordre croissant : " << endl;
    for(int i=0 ; i<5 ; i++)
    {
        cout << tab[i] << ' ';
    }
    cout << endl;
}
