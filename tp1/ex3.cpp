#include <iostream>
#include <time.h>
#include "ex3.h"

using namespace std;

void AskFullname()
{
    string nom;
    string prenom;
    cout << "Entrez nom et prénom" << endl;
    cin >> nom >> prenom;
    cout << "Bonjour " << nom << " " << prenom << " !" << endl;
}

void AskNumber()
{
    srand (time(NULL));
    int x = rand() % 1000;
    int y;
    string guess;
    cout << "Devinez le chiffre (entre 0 et 1000)" << endl;
    do
    {
        cin >> y;
        //y = stoi(guess);
        if(y > x) cout << "C'est moins !" << endl;
        else if(y < x) cout << "C'est plus !" << endl;
    }
    while(y != x);
    cout << "Bien joué !" << endl;
}
