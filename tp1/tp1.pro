TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        ex1.cpp \
        ex2.cpp \
        ex3.cpp \
        main.cpp

HEADERS += \
    ex1.h \
    ex2.h \
    ex3.h
