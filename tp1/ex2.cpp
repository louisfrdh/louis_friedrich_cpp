#include <iostream>
#include "ex2.h"

using namespace std;

void ScoreTennis(int a, int b)
{
    string score1 = "", score2 = "";
    int vainqueur = 0;
    string correspondances[4] = {"0", "15", "30", "40"};


    cout << "Pour " << a << "-" << b << endl;

    if(a < 0 || b < 0)
    {
        cout << "Valeurs incorrectes" << endl << endl;
        return;
    }

    if(a <= 3 && b <= 3)
    {
        score1 = correspondances[a];
        score2 = correspondances[b];
    }
    else if((a >= 3 && b > 3) || (a > 3 && b >= 3))
    {
        if(abs(a-b) == 2) vainqueur = a-b > 0 ? 1 : 2;
        else if(abs(a-b) == 1)
        {
            score1 = a-b > 0 ? "40A" : "40";
            score2 = b-a > 0 ? "40A" : "40";
        }
        else
        {
            cout << "Valeurs incorrectes" << endl << endl;
            return;
        }
    }
    else vainqueur = a-b > 0 ? 1 : 2;

    if(vainqueur != 0) cout << "Tennisman " << vainqueur << " remporte le jeu." << endl << endl;
    else cout << "Score : " << score1 << "-" << score2 << endl << endl;
}
