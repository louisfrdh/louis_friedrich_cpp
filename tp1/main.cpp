#include <iostream>
#include "ex1.h"
#include "ex2.h"
#include "ex3.h"

using namespace std;

int main()
{
    // Décommenter pour tester

    //  Exercice 1
    int c;
    int* pc = &c;
    cout << "Somme de 8+17 :"<< Somme(8,17) << endl;
    Somme(2,9,pc);
    cout << "Somme de 2+9 avec pointeur : "<< c << endl;
    Somme(4,-5,&c);
    cout << "Somme de 4+(-5) avec reference : "<< c << endl;


    //TriTab();

    //  Exercice 2
    /**
    ScoreTennis(7,10);
    ScoreTennis(1,-2);
    ScoreTennis(1,3);
    ScoreTennis(3,3);
    ScoreTennis(0,1);
    ScoreTennis(1,2);
    ScoreTennis(1,3);
    ScoreTennis(5,3);
    ScoreTennis(4,3);
    ScoreTennis(24,26);
    **/

    //  Exercice 3
    //AskFullname();
    //AskNumber();

    return 0;
}

