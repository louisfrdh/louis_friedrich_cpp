#include "pionothello.h"

PionOthello::PionOthello(std::shared_ptr<Joueur> joueur)
{
    m_joueur = joueur;
}
/*
void PionOthello::Afficher() const
{
    if(m_joueur != NULL)
    {
        std::string affPion = m_joueur->RetourneId() == 1 ? "b" : "n";
        std::cout << affPion;
    }
    else std::cout << " ";
}*/

void PionOthello::ChangeProprietaire(std::shared_ptr<Joueur> joueur)
{
    m_joueur = joueur;
}

std::string PionOthello::EnTexte() const
{
    if(m_joueur != NULL){
        return m_joueur->RetourneId() == 1 ? "(b)" : "(n)";
    }
    else return "   ";
}
