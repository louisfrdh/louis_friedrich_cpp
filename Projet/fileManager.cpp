#include "fileManager.h"

fileManager::fileManager()
{
}

fileManager::~fileManager()
{
}

void fileManager::saveGame(std::string saveString, std::string path)
{
    if (path.length() > 0)
    {
        std::ofstream file;
        file.open(path);
        file << saveString;
        file.close();
    }
}

Importer fileManager::loadGame(std::string path)
{
    return Importer(path);
}
