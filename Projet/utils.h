#ifndef UTILS_H
#define UTILS_H

#include "ensemblelineairecases.h"
#include <vector>
#include <memory>

class utils
{
private:
	/* data */
public:
	utils();
	~utils();
	static bool intInRange(const int value, const int minValue, const int maxValue);
	static void removeLastChar(std::string &texte);
	static int ConversionLettreColonne(const char lettreDeColonne);
	static char ConversionColonneLettre(const int numeroVersChar, const char maxChar);
};

#endif // UTILS_H
