#include "pionsuperdame.h"

PionSuperDame::PionSuperDame(std::shared_ptr<Joueur> joueur)
{
    m_joueur = joueur;
}

std::string PionSuperDame::EnTexte() const
{
    std::string textePion = "   ";
    if (m_joueur != NULL)
    {
        textePion = m_joueur->RetourneId() == 1 ? "(B)" : "(N)";
    }

    return textePion;
}
int  PionSuperDame::RetourneValeur() const {
    if(m_joueur->RetourneId() == 1)
    {
        return 'B';
    }
    else return 'N';
}
