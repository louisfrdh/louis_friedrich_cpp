#ifndef JEU_H
#define JEU_H

#include "grillemorpion.h"
#include "joueur.h"

class Jeu
{
public:
    virtual void Jouer() = 0;

private:
    virtual bool testFin() const = 0;
};

#endif // JEU_H
