#include "constantes.h"
#include "grillepuissance4.h"
#include "pionpuissance4.h"
#include "afficheurpuissance4.h"

GrillePuissance4::GrillePuissance4() : Grille(NB_LIGNES, NB_COLONNES, NB_JOUEURS, "Puissance4")
{
    initialiseVectors();
    initialiseCases();
}

GrillePuissance4::GrillePuissance4(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import) : Grille(NB_LIGNES, NB_COLONNES, NB_JOUEURS, "Puissance4")
{
    initialiseVectors();
    initialiseCasesImport(j1, j2, import);
}

void GrillePuissance4::DeposerJeton(int i, std::shared_ptr<Joueur> joueur)
{
    for (int j = m_lignes.size() - 1; j >= 0; j--)
    {
        if (m_colonnes[i].CaseEstVide(j))
        {
            m_colonnes[i].PlacePion(j, std::make_shared<PionPuissance4>(PionPuissance4(joueur)));
            return;
        }
    }
}

void GrillePuissance4::TourJoueur(std::shared_ptr<Joueur> joueur)
{
    AfficheurPuissance4::Affiche(this);
    int abscisse;
#ifdef IHM
    AdaptateurGraphique::AttendInteraction();
     abscisse = AdaptateurGraphique::FournitAbscisse();
    int tmp = AdaptateurGraphique::FournitOrdonne();
#else
    abscisse = AfficheurPuissance4::DemandeCordonnee(joueur, NB_COLONNES);
#endif //IHM
    DeposerJeton(abscisse, joueur);
}

void GrillePuissance4::initialiseCasesImport(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import)
{
    int g = m_nbLignes - 1 + m_nbLignes - 1 + m_nbColonnes;
    for (int x = 0; x < m_nbLignes; x++)
    {
        for (int y = 0; y < m_nbColonnes; y++)
        {
            std::shared_ptr<CaseMonoPion> sptrCase = std::make_shared<CaseMonoPion>(CaseMonoPion(x, y));
            int importValue = import.Valeur(x, y, m_nbColonnes);
            if (importValue != 0)
            {
                std::shared_ptr<PionPuissance4> p;
                if (importValue == 1)
                {
                    p = std::make_shared<PionPuissance4>(PionPuissance4(j1));
                }
                else if (importValue == 2)
                {
                    p = std::make_shared<PionPuissance4>(PionPuissance4(j2));
                }

                if (p != nullptr)
                {
                    sptrCase->PlacePion(p);
                }
            }
            m_cases.push_back(sptrCase);
            m_lignes[x].AjouterCase(sptrCase);
            m_colonnes[y].AjouterCase(sptrCase);
            m_diagonales[x + y].AjouterCase(sptrCase);
            m_diagonales[g + y].AjouterCase(sptrCase);
        }
        g--;
    }
}

bool GrillePuissance4::TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const
{
    for (auto ligneCourante : m_lignes)
    {
        if (ligneCourante.EstComplete(joueur, NB_LIGNES))
        {
            return true;
        }
    }

    for (auto colonneCourante : m_colonnes)
    {
        if (colonneCourante.EstComplete(joueur))
        {
            return true;
        }
    }

    for (auto diagonaleCourante : m_diagonales)
    {
        if (diagonaleCourante.EstComplete(joueur, NB_LIGNES))
        {
            return true;
        }
    }

    return false;
}

std::string GrillePuissance4::EnTexte() const
{
    std::string grilleEnTexte = "";
    for (Ligne ligneCourante : m_lignes)
    {
        grilleEnTexte += ligneCourante.EnTexte() + "\n";
    }

    for (int i = 0; i < NB_COLONNES; i++)
    {
        grilleEnTexte += "-----";
    }

    return grilleEnTexte;
}
