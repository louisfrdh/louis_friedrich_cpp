#ifndef IHMOTHELLO_H
#define IHMOTHELLO_H

#include "ihmjeugrille.h"
#include "threadjeu.h"
class IHMOthello: public IHMJeuGrille
{
public:
    IHMOthello(QWidget *parent);
    IHMOthello(QWidget *parent, Importer import);
    ~IHMOthello();
protected:
    virtual void InitialiseDesCases() override;
    virtual void MiseEnFormeCase(IHMCaseMonoPion* nouvelleCase) override;
    virtual IHMCaseMonoPion* DefinitionCase(int abcisse,int ordonne) override;
    virtual void AssocieImagePion(std::shared_ptr<Pion> pion,IHMCaseMonoPion* caseCible) override;
};

#endif // IHMOTHELLO_H
