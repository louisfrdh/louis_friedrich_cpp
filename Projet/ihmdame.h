#ifndef IHMDAME_H
#define IHMDAME_H

#include "ihmjeugrille.h"

class IHMDame: public IHMJeuGrille
{
    Q_OBJECT
public:
    IHMDame(QWidget *parent);
    IHMDame(QWidget *parent, Importer import);
    ~IHMDame();

protected:
    virtual void InitialiseDesCases() override;
    virtual void MiseEnFormeCase(IHMCaseMonoPion* nouvelleCase) override;
    virtual void MiseEnFormeCase(IHMCaseMonoPion* nouvelleCase,int i,int ordonne);
    virtual IHMCaseMonoPion* DefinitionCase(int abcisse,int ordonne) override;
    virtual void AssocieImagePion(std::shared_ptr<Pion> pion ,IHMCaseMonoPion* caseCible) override;

};

#endif // IHMDAME_H
