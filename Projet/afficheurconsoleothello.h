#ifndef AFFICHEURCONSOLEOTHELLO_H
#define AFFICHEURCONSOLEOTHELLO_H

#include "outilsconsole.h"
#include "joueur.h"

class AfficheurConsoleOthello : OutilsConsole
{
public:
    static int DemandeLigne(std::shared_ptr<Joueur> UnJoueur);
    static int DemandeColonne(std::shared_ptr<Joueur> UnJoueur);
    static void Affiche(Grille* grille);
    static char ConversionColonneLettre(int numeroColonne);
};

#endif // AFFICHEURCONSOLEOTHELLO_H
