#ifndef IHMCASEMONOPION_H
#define IHMCASEMONOPION_H


#include <QPushButton>

class IHMCaseMonoPion : public QPushButton
{
    Q_OBJECT
public:
    IHMCaseMonoPion(QWidget *parent, int abcisse, int ordonne);

    int RenvoiAbcisse() const;
    int RenvoiOrdonne() const;

private:
    int m_abcisse;
    int m_ordonne;
};

#endif // IHMCASEMONOPION_H
