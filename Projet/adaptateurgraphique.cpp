#include "adaptateurgraphique.h"

int AdaptateurGraphique::m_ordonne = 0;
int AdaptateurGraphique::m_abcisse = 0;
bool AdaptateurGraphique::m_abcisseFournit = false;
bool AdaptateurGraphique::m_ordonneFournit = false;

void AdaptateurGraphique::AttendInteraction()
{
	while ((!(m_abcisseFournit)) || (!(m_ordonneFournit)))
	{
	}
}

void AdaptateurGraphique::EnregistreAbscisse(int x)
{
	m_abcisse = x;
	m_abcisseFournit = true;
}

void AdaptateurGraphique::EnregistreOrdonne(int y)
{
	m_ordonne = y;
	m_ordonneFournit = true;
}

int AdaptateurGraphique::FournitAbscisse()
{
	m_abcisseFournit = false;
	return m_abcisse;
}

int AdaptateurGraphique::FournitOrdonne()
{
	m_ordonneFournit = false;
	return m_ordonne;
}
