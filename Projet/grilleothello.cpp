#include "constantes.h"
#include "utils.h"
#include "grilleothello.h"

GrilleOthello::GrilleOthello(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2)
    : Grille(NB_LIGNES, NB_COLONNES, NB_JOUEURS, "Othello")
{
    initialiseVectors();
    initialiseCases(j1, j2);
}
GrilleOthello::GrilleOthello(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import)
    : Grille(NB_LIGNES, NB_COLONNES, NB_JOUEURS, "Othello")
{
    initialiseVectors();
    initialiseCasesImport(j1, j2, import);
}

void GrilleOthello::TourJoueur(std::shared_ptr<Joueur> joueur)
{
    AfficheurConsoleOthello::Affiche(this);
    m_joueur = joueur;
    remplitCasesSelectionnables();
    if (!m_casesSelectionnables.empty())
    {
        demandeCaseCible();
        placeNouveauPion(m_caseCible, m_joueur);
        remplitCasesAffectees();
        retournePionsCasesAffectees();
        m_casesSelectionnables.clear();
    }
}

bool GrilleOthello::TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const
{
    int conteurPion;

    if (this->EstPleine())
    {
        conteurPion = differenceDePion(joueur);
        if (conteurPion>0) return true;
        else return false;

    }
    else return false;
}

int GrilleOthello::differenceDePion(std::shared_ptr<Joueur> joueur) const
{
    int conteurPion=0;
    for (auto caseCourante : m_cases)
    {
        if (!caseCourante->EstVide() && caseCourante->RetourneIdJoueur() == joueur->RetourneId())
        {
            conteurPion++;
        }
        else if(!caseCourante->EstVide())
        {
            conteurPion--;
        }
    }
    return conteurPion;
}

void GrilleOthello::demandeCaseCible()
{
    OutilsConsole::InformeUtilisateurDe("Cases atteignables :");
    for (auto caseCourante : m_casesSelectionnables)
    {
        std::cout << "[" << utils::ConversionColonneLettre(caseCourante->RetourneY(), 'J') << caseCourante->RetourneX() + 1 << "] ";
    }
    std::cout << std::endl;

    int x, y;
    std::shared_ptr<Case> caseCible;
    do
    {
#ifdef IHM

        AdaptateurGraphique::AttendInteraction();
        y = AdaptateurGraphique::FournitAbscisse();
        x = AdaptateurGraphique::FournitOrdonne();
#else

        y = AfficheurConsoleOthello::DemandeColonne(m_joueur);
        x = AfficheurConsoleOthello::DemandeLigne(m_joueur);
#endif //IHM

        caseCible = m_lignes[x].RetourneCasePtr(y);
    } while (!testPresenceCaseDansVector(x, y, m_casesSelectionnables));
    m_caseCible = caseCible;
}

void GrilleOthello::remplitCasesSelectionnables()
{
    for (auto caseCourante : m_cases)
    {
        /// on cherche toutes les cases contenant un pion du joueur
        if (caseCourante->RetourneIdJoueur() == m_joueur->RetourneId())
        {
            remplitCasesSelectionnablesGraceACetteCase(caseCourante);
        }
    }
}

void GrilleOthello::remplitCasesSelectionnablesGraceACetteCase(std::shared_ptr<Case> caseCible)
{
    int x = caseCible->RetourneX();
    int y = caseCible->RetourneY();
    
    std::shared_ptr<Ligne> ligne = std::make_shared<Ligne>(m_lignes[x]);
    std::shared_ptr<Colonne> colonne = std::make_shared<Colonne>(m_colonnes[y]);
    std::shared_ptr<Diagonale> diago1 = std::make_shared<Diagonale>(m_diagonales[x + y]);
    std::shared_ptr<Diagonale> diago2 = std::make_shared<Diagonale>(m_diagonales[22 - x + y]);
    
    remplitCasesSelectionnablesDansEnsembleCases(ligne, x, y);
    remplitCasesSelectionnablesDansEnsembleCases(colonne, x, y);
    remplitCasesSelectionnablesDansEnsembleCases(diago1, x, y);
    remplitCasesSelectionnablesDansEnsembleCases(diago2, x, y);
}

void GrilleOthello::remplitCasesSelectionnablesDansEnsembleCases(std::shared_ptr<EnsembleLineaireCases> ensemble, int x, int y)
{
    int iCaseDansEnsemble = ensemble->RetourneIndiceCase(x, y);
    ///test en partant de la case vers un côté de l'ensemble
    for (int i = iCaseDansEnsemble + 1; i < ensemble->RetourneLongueur(); i++)
    {
        if (caseEmpecheSelection(ensemble, i, iCaseDansEnsemble))
            break;
        ///si la case est vide on l'ajoute à la liste de cases sélectionnables
        else if (ensemble->RetourneIdJoueur(i) == -1)
        {
            m_casesSelectionnables.push_back(ensemble->RetourneCasePtr(i));
            break;
        }
    }
    ///même test en partant de la case vers l'autre côté de l'ensemble
    for (int i = iCaseDansEnsemble - 1; i >= 0; i--)
    {
        if (caseEmpecheSelection(ensemble, i, iCaseDansEnsemble))
        {
            break;
        }
        else if (ensemble->RetourneIdJoueur(i) == -1)
        {
            m_casesSelectionnables.push_back(ensemble->RetourneCasePtr(i));
            break;
        }
    }
}

bool GrilleOthello::caseEmpecheSelection(std::shared_ptr<EnsembleLineaireCases> ensemble,
                                         int indiceCase, int indiceCaseOrigine) const
{
    int idJoueurCase = ensemble->RetourneIdJoueur(indiceCase);
    ///si la case appartient déjà au joueur
    if (idJoueurCase == m_joueur->RetourneId())
    {
        return true;
    } ///si la case est vide mais adjacente à la case du joueur (pas de pion à manger)
    else if (idJoueurCase == -1 &&
             ((indiceCase == indiceCaseOrigine + 1) || (indiceCase == indiceCaseOrigine - 1)))
    {
        return true;
    }
    return false;
}

void GrilleOthello::remplitCasesAffectees()
{
    int x = m_caseCible->RetourneX();
    int y = m_caseCible->RetourneY();

    std::shared_ptr<Ligne> ligne = std::make_shared<Ligne>(m_lignes[x]);
    std::shared_ptr<Colonne> colonne = std::make_shared<Colonne>(m_colonnes[y]);
    std::shared_ptr<Diagonale> diago1 = std::make_shared<Diagonale>(m_diagonales[x + y]);
    std::shared_ptr<Diagonale> diago2 = std::make_shared<Diagonale>(m_diagonales[22 - x + y]);

    remplitCasesAffecteesDansEnsembleCases(ligne, x, y);
    remplitCasesAffecteesDansEnsembleCases(colonne, x, y);
    remplitCasesAffecteesDansEnsembleCases(diago1, x, y);
    remplitCasesAffecteesDansEnsembleCases(diago2, x, y);
}

void GrilleOthello::remplitCasesAffecteesDansEnsembleCases(
    std::shared_ptr<EnsembleLineaireCases> ensemble, int x, int y)
{
    int iCaseDansEnsemble = ensemble->RetourneIndiceCase(x, y);
    std::vector<std::shared_ptr<Case>> tmpCasesAffectees = std::vector<std::shared_ptr<Case>>();
    ///test en partant de la case vers un côté de l'ensemble
    for (int i = iCaseDansEnsemble + 1; i < ensemble->RetourneLongueur(); i++)
    {
        ///si on rencontre un pion du joueur, on insère les cases présentes dans notre
        /// vecteur dans la liste des cases affectées
        if (ensemble->RetourneIdJoueur(i) == m_joueur->RetourneId())
        {
            insererVecteurDeCasesAffectees(tmpCasesAffectees);
        }
        ///si on rencontre une case vide avant un pion du même joueur aucune case
        /// n'est affectée de ce côté de l'ensemble
        else if (ensemble->RetourneIdJoueur(i) == -1)
        {
            break;
        } ///si on rencontre un pion du joueur adverse, il est potentiellement affecté,
        /// on l'ajoute à notre vecteur temporaire et on poursuit la recherche
        else if (ensemble->RetourneIdJoueur(i) != m_joueur->RetourneId())
        {
            tmpCasesAffectees.push_back(ensemble->RetourneCasePtr(i));
        }
    }
    tmpCasesAffectees.clear();
    ///même test en partant de la case vers l'autre côté de l'ensemble
    for (int i = iCaseDansEnsemble - 1; i >= 0; i--)
    {
        if (ensemble->RetourneIdJoueur(i) == m_joueur->RetourneId())
        {
            insererVecteurDeCasesAffectees(tmpCasesAffectees);
        }
        else if (ensemble->RetourneIdJoueur(i) == -1)
        {
            break;
        }
        else if (ensemble->RetourneIdJoueur(i) != m_joueur->RetourneId())
        {
            tmpCasesAffectees.push_back(ensemble->RetourneCasePtr(i));
        }
    }
}

void GrilleOthello::insererVecteurDeCasesAffectees(std::vector<std::shared_ptr<Case>> vectorCase)
{
    for (std::shared_ptr<Case> caseCourante : vectorCase)
    {
        m_casesAffectees.push_back(caseCourante);
    }
}

void GrilleOthello::retournePionsCasesAffectees()
{
    for (std::shared_ptr<Case> caseCourante : m_casesAffectees)
    {
        std::shared_ptr<PionOthello> pionOthello = std::dynamic_pointer_cast<PionOthello>(caseCourante->RetournePionPtr());
        pionOthello->ChangeProprietaire(m_joueur);
    }
    m_casesAffectees.clear();
}

bool GrilleOthello::testPresenceCaseDansVector(int x, int y,
                                               std::vector<std::shared_ptr<Case>> cases) const
{
    for (auto caseCourante : cases)
    {
        if (caseCourante->RetourneX() == x && caseCourante->RetourneY() == y)
        {
            return true;
        }
    }
    return false;
}

void GrilleOthello::initialiseGrille()
{
    initialiseVectors();
}

void GrilleOthello::initialiseCases(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2)
{
    int iLignePourDiagonalesSensInverse = m_nbLignes - 1 + m_nbLignes - 1 + m_nbColonnes;
    for (int i = 0; i < m_nbLignes; i++)
    {
        for (int j = 0; j < m_nbColonnes; j++)
        {
            std::shared_ptr<CaseMonoPion> sptrCase = std::make_shared<CaseMonoPion>(CaseMonoPion(i, j));
            if (estCaseInitiale(i, j, j1))
            {
                placeNouveauPion(sptrCase, j1);
            }
            if (estCaseInitiale(i, j, j2))
            {
                placeNouveauPion(sptrCase, j2);
            }
            m_cases.push_back(sptrCase);
            m_lignes[i].AjouterCase(sptrCase);
            m_colonnes[j].AjouterCase(sptrCase);
            m_diagonales[i + j].AjouterCase(sptrCase);
            m_diagonales[iLignePourDiagonalesSensInverse + j].AjouterCase(sptrCase);
        }
        iLignePourDiagonalesSensInverse--;
    }
}

void GrilleOthello::initialiseCasesImport(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import)
{
    std::cout << "On entre dans le initialiseCase de l'othello" << std::endl;
    int iLignePourDiagonalesSensInverse = m_nbLignes-1 + m_nbLignes-1 + m_nbColonnes;
    for(int i=0; i<m_nbLignes; i++)
    {
        for(int j=0; j<m_nbColonnes; j++)
        {
            std::shared_ptr<CaseMonoPion> sptrCase = std::make_shared<CaseMonoPion>(CaseMonoPion(i, j));
            int importValue = import.Valeur(i*m_nbColonnes + j);
            if(importValue != 0)
            {
                if(importValue == 1) placeNouveauPion(sptrCase, j1);
                else if(importValue == 2) placeNouveauPion(sptrCase, j2);
            }
            m_cases.push_back(sptrCase);
            m_lignes[i].AjouterCase(sptrCase);
            m_colonnes[j].AjouterCase(sptrCase);
            m_diagonales[i+j].AjouterCase(sptrCase);
            m_diagonales[iLignePourDiagonalesSensInverse+j].AjouterCase(sptrCase);
        }
        iLignePourDiagonalesSensInverse--;
    }
}

void GrilleOthello::placeNouveauPion(std::shared_ptr<Case> c, std::shared_ptr<Joueur> j)
{
    std::shared_ptr<PionOthello> p;
    p = std::make_shared<PionOthello>(PionOthello(j));
    c->PlacePion(p);
}

bool GrilleOthello::estCaseInitiale(int x, int y, std::shared_ptr<Joueur> j) const
{
    if (j->RetourneId() == 1)
    {
        return (y == (m_nbColonnes - 1) / 2 && x == (m_nbLignes - 1) / 2) || (y == (m_nbColonnes - 1) / 2 + 1 && x == (m_nbLignes - 1) / 2 + 1);
    }
    else if (j->RetourneId() == 2)
    {
        return (y == (m_nbColonnes - 1) / 2 && x == (m_nbLignes - 1) / 2 + 1) || (y == (m_nbColonnes - 1) / 2 + 1 && x == (m_nbLignes - 1) / 2);
    }
    return false;
}

std::string GrilleOthello::EnTexte() const
{
    int i = 1;
    std::string grilleEnTexte = "----------------------------------------\n";
    for (Ligne l : m_lignes)
    {
        grilleEnTexte = grilleEnTexte + l.EnTexte() + " [" + std::to_string(i) + "]\n";
        i++;
        grilleEnTexte = grilleEnTexte + "----------------------------------------\n";
    }
    return grilleEnTexte;
}
