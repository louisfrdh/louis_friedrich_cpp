#include "afficheurmorpion.h"

int AfficheurMorpion::DemandeCordonnee(const std::shared_ptr<Joueur> UnJoueur, const std::string ligneOuColonne)
{
    int choix;
    bool choixConforme = false;
    while (!choixConforme)
    {
        choix = AttendUnEntier(UnJoueur->RetourneNom() + ",merci de saisir un numéro de " + ligneOuColonne + "[1,2 ou 3]");
        if (utils::intInRange(choix, 1, 3))
        {
            choixConforme = true;
        }
        else
        {
            InformeUtilisateurDe(">>Etes vous sur de saisir un chiffre entre 1 et 3?");
        }
    }
    choix--; // convertit en la valeur correspondante pour le programme
    return choix;
}

void AfficheurMorpion::Affiche(const Grille *grille)
{
    NettoyageConsole();
    std::cout << "=== [MORPION] ===" << std::endl
              << std::endl;
    std::cout << grille->EnTexte() << std::endl;
}
