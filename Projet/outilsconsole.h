#ifndef OUTILSCONSOLE_H
#define OUTILSCONSOLE_H

#include <string>
#include <iostream>
#include "grille.h"

class OutilsConsole
{
public:
    static void NettoyageConsole();
    static void InformeUtilisateurDe(const std::string message);

    static int AttendUnEntier();
    static int AttendUnEntier(const std::string messagePourUtilisateur);

    static std::string AttendUnMot(const std::string messagePourUtilisateur);
    static std::string AttendUnNomPrenom();

protected:
    static std::string DemandeUnElement();
    static std::string DemandeUnePhrase();

    static bool EstUnEntierUniquement(const std::string chaine);
    static bool EstUnMotUniquement(const std::string chaine);

    static bool VerifierCaractereIncorrecte(const char caractere);
};

#endif // OUTILSCONSOLE_H
