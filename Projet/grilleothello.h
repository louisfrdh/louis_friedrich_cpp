#ifndef GRILLEOTHELLO_H
#define GRILLEOTHELLO_H

#include "grille.h"
#include "pionothello.h"
#include "casemonopion.h"
#include "afficheurconsoleothello.h"
#include <algorithm>

class GrilleOthello : public Grille
{
public:
    const static int NB_COLONNES = 8;
    const static int NB_LIGNES = 8;
    const static int NB_JOUEURS = 2;

    GrilleOthello();
    GrilleOthello(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2);
    GrilleOthello(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import);

    void TourJoueur(std::shared_ptr<Joueur> joueur) override;
    bool TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const override;

    std::string EnTexte() const override;

private:
    /// joueur pour le tour en cours
    std::shared_ptr<Joueur> m_joueur;
    std::shared_ptr<Case> m_caseCible;
    std::vector<std::shared_ptr<Case>> m_casesSelectionnables, m_casesAffectees;

    void demandeCaseCible();
    void remplitCasesSelectionnables();
    void remplitCasesSelectionnablesGraceACetteCase(std::shared_ptr<Case> c);
    void remplitCasesSelectionnablesDansEnsembleCases(std::shared_ptr<EnsembleLineaireCases> ensemble, int x, int y);


    void remplitCasesAffectees();
    void remplitCasesAffecteesDansEnsembleCases(std::shared_ptr<EnsembleLineaireCases> ensemble, int x, int y);
    void insererVecteurDeCasesAffectees(std::vector<std::shared_ptr<Case>> v);
    void retournePionsCasesAffectees();

    void initialiseGrille();
    void initialiseCases(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2);
    void initialiseCasesImport(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import) override;

    void placeNouveauPion(std::shared_ptr<Case> c, std::shared_ptr<Joueur> j);
    
    bool caseEmpecheSelection(std::shared_ptr<EnsembleLineaireCases> ensemble, int indiceCase, int indiceCaseOrigine) const;
    
    bool testPresenceCaseDansVector(int x, int y, std::vector<std::shared_ptr<Case>> cases) const;
    bool caseContientUnPionDuJoueur(std::shared_ptr<Case> aCase) const;

    bool estCaseInitiale(int x, int y, std::shared_ptr<Joueur> j) const;
    int differenceDePion(std::shared_ptr<Joueur> joueur) const ;
};

#endif // GRILLEOTHELLO_H
