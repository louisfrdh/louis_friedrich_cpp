#include "outilsconsole.h"

void OutilsConsole::InformeUtilisateurDe(const std::string unMessage)
{
    std::cout << unMessage << std::endl;
}

std::string OutilsConsole::DemandeUnElement()
{
    std::cout << "-> ";

    std::string reponseUtilisateur;
    std::cin >> reponseUtilisateur;

    return reponseUtilisateur;
}

std::string OutilsConsole::DemandeUnePhrase()
{
    std::cout << "-> ";

    std::string reponseUtilisateur;
    std::getline(std::cin, reponseUtilisateur);

    return reponseUtilisateur;
}

std::string OutilsConsole::AttendUnNomPrenom()
{
    bool reponseNonConforme = true;

    std::string reponseUtilisateur;

    while (reponseNonConforme)
    {
        InformeUtilisateurDe("Merci de saisir votre pseudo");
        reponseUtilisateur = DemandeUnElement();

        if (EstUnMotUniquement(reponseUtilisateur))
        {
            reponseNonConforme = false;
        }
    }
    return reponseUtilisateur;
}

int OutilsConsole::AttendUnEntier()
{
    return AttendUnEntier("Merci de saisir un entier naturel");
}

int OutilsConsole::AttendUnEntier(const std::string messagePourUtilisateur)
{
    bool reponseNonConforme = true;
    std::string reponseUtilisateur;

    while (reponseNonConforme)
    {
        InformeUtilisateurDe(messagePourUtilisateur);
        reponseUtilisateur = DemandeUnElement();
        if (EstUnEntierUniquement(reponseUtilisateur))
        {
            reponseNonConforme = false;
        }
        else
        {
            InformeUtilisateurDe(">>Etes vous sur de saisir un nombre ?");
        }
    }
    return stoi(reponseUtilisateur);
}

std::string OutilsConsole::AttendUnMot(const std::string messagePourUtilisateur)
{
    bool reponseNonConforme = true;
    std::string reponseUtilisateur;

    while (reponseNonConforme)
    {
        InformeUtilisateurDe(messagePourUtilisateur);
        reponseUtilisateur = DemandeUnElement();
        if (EstUnMotUniquement(reponseUtilisateur))
        {
            reponseNonConforme = false;
        }
        else
        {
            InformeUtilisateurDe(">>Etes vous sur de saisir une lettre ?");
        }
    }
    return reponseUtilisateur;
}

bool OutilsConsole::EstUnEntierUniquement(const std::string chaine)
{
    int longueur = chaine.length();
    for (int i = 0; i < longueur; i++)
    {
        if (!isdigit(chaine[i]))
        {
            return false;
        }
    }
    return true; // Nombre uniquement
}

bool OutilsConsole::VerifierCaractereIncorrecte(const char caractere)
{
    return isdigit(caractere) || ispunct(caractere) || isblank(caractere) || isspace(caractere);
}

bool OutilsConsole::EstUnMotUniquement(const std::string chaine)
{
    int longueur = chaine.length();

    for (int i = 0; i < longueur; i++)
    {
        if (VerifierCaractereIncorrecte(chaine[i]))
        {
            return false;
        }
    }
    return true;
}

void OutilsConsole::NettoyageConsole()
{
    try
    {
        std::system("cls");
    }
    catch (const std::exception &e)
    {
        std::cout << std::endl
                  << std::endl
                  << std::endl
                  << std::endl
                  << std::endl
                  << std::endl;
    }
}
