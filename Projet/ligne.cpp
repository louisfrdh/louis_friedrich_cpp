#include "utils.h"

#include "ligne.h"

std::string Ligne::EnTexte() const
{
    std::string ligneEnTexte = "";
    for (auto colonneCourante : m_cases)
    {
        ligneEnTexte += colonneCourante->EnTexte();
    }
    return ligneEnTexte;
}

std::string Ligne::getLineDescription() const
{
    std::string description = "";
    for (auto caseCourante : m_cases)
    {
        description += caseCourante->getDescription() + ",";
    }
    utils::removeLastChar(description);
    return description;
}
