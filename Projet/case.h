#ifndef CONTENANT_PION_H
#define CONTENANT_PION_H

#include "pion.h"
#include <memory>

class Case
{
public:
    virtual bool EstVide() const = 0;
    virtual bool EstIdDuJoueur(const int id) const = 0;

    virtual void PlacePion(std::shared_ptr<Pion> pion) = 0;
    virtual void RetirePion() = 0;

    virtual int RetourneX() const = 0;
    virtual int RetourneY() const = 0;
    virtual int RetourneIdJoueur() const = 0;
    virtual int RetourneCaseValue() const =0;

    virtual std::shared_ptr<Joueur> RetourneJoueurPtr() const = 0;
    virtual std::shared_ptr<Pion> RetournePionPtr() const = 0;
    //virtual void Afficher() const =0;
    virtual std::string EnTexte() const = 0;
    virtual std::string getDescription() const = 0;
};

#endif // CONTENANT_PION_H
