QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += console c++11  #Mode IHM : c++11   #Mode console : #Mode IHM : console c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    adaptateurgraphique.cpp \
    afficheurconsoleothello.cpp \
    afficheurdame.cpp \
    afficheurmorpion.cpp \
    afficheurpuissance4.cpp \
    casemonopion.cpp \
    colonne.cpp \
    diagonale.cpp \
    ensemblelineairecases.cpp \
    fileManager.cpp \
    grille.cpp \
    grilledame.cpp \
    grillemorpion.cpp \
    grilleothello.cpp \
    grillepuissance4.cpp \
    ihmcasemonopion.cpp \
    ihmdame.cpp \
    ihmjeugrille.cpp \
    ihmmenu.cpp \
    ihmmorpion.cpp \
    ihmothello.cpp \
    ihmpuissance4.cpp \
    importer.cpp \
    jeugrille.cpp \
    joueur.cpp \
    ligne.cpp \
    main.cpp \
    mainwindow.cpp \
    outilsconsole.cpp \
    piondame.cpp \
    pionmorpion.cpp \
    pionothello.cpp \
    pionpuissance4.cpp \
    pionsuperdame.cpp \
    threadjeu.cpp \
    utils.cpp 

HEADERS += \
    adaptateurgraphique.h \
    afficheurconsoleothello.h \
    afficheurdame.h \
    afficheurmorpion.h \
    afficheurpuissance4.h \
    autorisedeplacementpions.h \
    case.h \
    casemonopion.h \
    colonne.h \
    contenantpion.h \
    constantes.h \
    diagonale.h \
    ensemblelineairecases.h \
    fileManager.h \
    grille.h \
    grilledame.h \
    grillemorpion.h \
    grilleothello.h \
    grillepuissance4.h \
    ihmcasemonopion.h \
    ihmdame.h \
    ihmjeugrille.h \
    ihmmenu.h \
    ihmmorpion.h \
    ihmothello.h \
    ihmpuissance4.h \
    importer.h \
    jeu.h \
    jeugrille.h \
    joueur.h \
    ligne.h \
    mainwindow.h \
    outilsconsole.h \
    pion.h \
    piondame.h \
    pionmorpion.h \
    pionothello.h \
    pionpuissance4.h \
    pionsuperdame.h \
    threadjeu.h \
    utils.h

FORMS += \
    ihmmenu.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    .gitignore \
    JeuCPP-IHM.pro.user

RESOURCES += \
    Ressource.qrc
