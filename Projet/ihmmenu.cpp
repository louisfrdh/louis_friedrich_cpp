#include "constantes.h"

#ifdef IHM
#include "ihmmenu.h"
#include "ui_ihmmenu.h"

IHMMenu::IHMMenu(QWidget *parent) : QMainWindow(parent), ui(new Ui::IHMMenu)
{
    ui->setupUi(this);

    this->setWindowTitle("EA");
    //this->setFixedSize(HAUTEUR_MOYENNE,HAUTEUR_MOYENNE);
    this->setWindowIcon(QIcon(":/Graphics/ea.png"));

    // ==== Menu Fichier ====
    menuFichier = menuBar()->addMenu("Fichier");
    menuFichier->addAction(actionOuvrir);
    menuFichier->addAction(actionSauvegarder);
    menuFichier->addAction(actionSauvegarderSous);
    menuFichier->addAction(actionQuitter);

    connect(actionQuitter, SIGNAL(triggered()), qApp, SLOT(quit()));
    connect(actionOuvrir, SIGNAL(triggered()), this, SLOT(ActiveOuvrir()));
    connect(actionSauvegarderSous, SIGNAL(triggered()), this, SLOT(ActiveSauvegarderSous()));
    actionQuitter->setShortcut(QKeySequence("Ctrl+Q"));

    // ==== Menu Choix de Jeu ====
    menuChoixJeu = menuBar()->addMenu("Choix de jeu");
    menuJeuDeGrille = menuChoixJeu->addMenu("Jeux de plateau");
    menuJeuDeGrille->addAction(commenceJeuDeMorpion);
    menuJeuDeGrille->addAction(commenceJeuDePuissance4);
    menuJeuDeGrille->addAction(commenceJeuOthello);
    menuJeuDeGrille->addAction(commenceJeuDeDame);

    connect(commenceJeuDeMorpion, SIGNAL(triggered()), this, SLOT(ActiveJeuDeMorpion()));
    connect(commenceJeuDePuissance4, SIGNAL(triggered()), this, SLOT(ActiveJeuDePuissance4()));
    connect(commenceJeuOthello, SIGNAL(triggered()), this, SLOT(ActiveJeuDeOthello()));
    connect(commenceJeuDeDame, SIGNAL(triggered()), this, SLOT(ActiveJeuDeDame()));

    // ==== Menu Affichage ====//
    menuAffichage = menuBar()->addMenu("Affichage");
    menuAffichage->addAction(actionTailleMinimumFenetre);
    menuAffichage->addAction(actionTailleMoyenneFenetre);
    menuAffichage->addAction(actionTailleGrandFenetre);

    connect(actionTailleMinimumFenetre, SIGNAL(triggered()), this, SLOT(ActiveTailleMinimum()));
    connect(actionTailleMoyenneFenetre, SIGNAL(triggered()), this, SLOT(ActiveTailleMoyenne()));
    connect(actionTailleGrandFenetre, SIGNAL(triggered()), this, SLOT(ActiveTailleMaximum()));
}

void IHMMenu::ActiveOuvrir()
{
    m_emplacementFichierSauvegarde = QFileDialog::getOpenFileName(this, "Ouvrir un fichier", QString(), "Textes (*.txt)");
    if (m_emplacementFichierSauvegarde == "")
    {
        return;
    }
    Importer import(m_emplacementFichierSauvegarde.toUtf8().constData()); //QString en std::string

    std::string type = import.getType();
    if (type == "Morpion")
    {
        vueJeuGrille = new IHMMorpion(this, import);
    }
    else if (type == "Puissance4")
    {
        vueJeuGrille = new IHMPuissance4(this, import);
    }
    else if(type == "Othello")
    {
        vueJeuGrille = new IHMOthello(this, import);
    }
    else if(type == "Dames")
    {
        vueJeuGrille = new IHMDame(this, import);
    }

    miseEnPageCentrale->addWidget(vueJeuGrille);
    afficheurCentrale->setLayout(miseEnPageCentrale);
    setCentralWidget(vueJeuGrille);
    afficheurCentrale->adjustSize();
    this->adjustSize();
}
void IHMMenu::ActiveJeuDeMorpion()
{
    vueJeuGrille = new IHMMorpion(this);
    miseEnPageCentrale->addWidget(vueJeuGrille);
    afficheurCentrale->setLayout(miseEnPageCentrale);
    setCentralWidget(vueJeuGrille);
    afficheurCentrale->adjustSize();
    this->adjustSize();
}

void IHMMenu::ActiveJeuDePuissance4()
{
    vueJeuGrille = new IHMPuissance4(this);
    miseEnPageCentrale->addWidget(vueJeuGrille);
    afficheurCentrale->setLayout(miseEnPageCentrale);
    setCentralWidget(vueJeuGrille);
    afficheurCentrale->adjustSize();
    this->adjustSize();
}

void IHMMenu::ActiveJeuDeOthello()
{
    vueJeuGrille = new IHMOthello(this);
    miseEnPageCentrale->addWidget(vueJeuGrille);
    afficheurCentrale->setLayout(miseEnPageCentrale);
    setCentralWidget(vueJeuGrille);
    afficheurCentrale->adjustSize();
    this->adjustSize();
}
void IHMMenu::ActiveJeuDeDame()
{
    vueJeuGrille = new IHMDame(this);
    miseEnPageCentrale->addWidget(vueJeuGrille);
    afficheurCentrale->setLayout(miseEnPageCentrale);
    setCentralWidget(vueJeuGrille);
    afficheurCentrale->adjustSize();
    this->adjustSize();
}

void IHMMenu::ActiveTailleMinimum()
{
    this->setFixedSize(HAUTEUR_MINIMUM, HAUTEUR_MINIMUM);
}
void IHMMenu::ActiveTailleMoyenne()
{
    this->setFixedSize(HAUTEUR_MOYENNE, HAUTEUR_MOYENNE);
}
void IHMMenu::ActiveTailleMaximum()
{
    this->setFixedSize(HAUTEUR_MAXIMUM, HAUTEUR_MAXIMUM);
}
void IHMMenu::ActiveSauvegarderSous()
{
    QString m_emplacementFichierSauvegarde = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    m_emplacementFichierSauvegarde += "/Jeu.txt";
}

IHMMenu::~IHMMenu()
{
    delete ui;
}
#endif //IHM
