#ifndef PION_MORPION_H
#define PION_MORPION_H

#include "pion.h"

class PionMorpion : public Pion
{
public:
    PionMorpion(std::shared_ptr<Joueur> joueur);
    std::string EnTexte() const override;
};

#endif // PION_MORPION_H
