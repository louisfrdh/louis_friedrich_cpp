#include "constantes.h"

#include "utils.h"
#include "grilledame.h"

GrilleDame::GrilleDame(std::shared_ptr<Joueur> joueur1, std::shared_ptr<Joueur> joueur2)
    : Grille(NB_LIGNES, NB_COLONNES, NB_JOUEURS, "Dames")
{
    initialiseVectors();
    initialiseCases(joueur1, joueur2);
}

GrilleDame::GrilleDame(std::shared_ptr<Joueur> joueur1, std::shared_ptr<Joueur> joueur2, Importer import)
    : Grille(NB_LIGNES, NB_COLONNES, NB_JOUEURS, "Dames")
{
    initialiseVectors();
    initialiseCasesImport(joueur1, joueur2, import);
}
void GrilleDame::TourJoueur(std::shared_ptr<Joueur> joueur)
{
    AfficheurDame::Affiche(this);
    m_joueur = joueur;
    remplitCasesSelectionnables();
    demandeCaseOrigine();
    remplitMapCasesCibles();
    demandeCaseCible();
    deplacePion();
}

bool GrilleDame::TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const
{
    for (auto caseCourante : m_cases)
    {
        int idJoueur = joueur->RetourneId();
        bool PasJoueurCourant = Joueur::joueurExiste(caseCourante->RetourneJoueurPtr()) && !caseCourante->EstIdDuJoueur(idJoueur);

        if (PasJoueurCourant)
        {
            return false;
        }
    }
    return true;
}

void GrilleDame::demandeCaseOrigine()
{
    /// affiche possibilités
    OutilsConsole::InformeUtilisateurDe("Les cases selectionnables :");

    for (auto caseCourante : m_casesSelectionnables)
    {
        std::cout << "[" << utils::ConversionColonneLettre(caseCourante->RetourneY(), 'J') << caseCourante->RetourneX() + 1 << "] ";
    }
    std::cout << std::endl;

    int x, y;
    std::shared_ptr<Case> caseOrigine;
    do
    {
#ifdef IHM
        AdaptateurGraphique::AttendInteraction();
        y = AdaptateurGraphique::FournitAbscisse();
        x = AdaptateurGraphique::FournitOrdonne();
#else
        y = AfficheurDame::DemandeColonne(m_joueur);
        x = AfficheurDame::DemandeLigne(m_joueur);
#endif //IHM

        caseOrigine = m_lignes[x].RetourneCasePtr(y);
    } while (!testPresenceCaseDansVector(x, y, m_casesSelectionnables));
    m_caseOrigine = caseOrigine;
}

void GrilleDame::demandeCaseCible()
{
    OutilsConsole::InformeUtilisateurDe("Cases atteignables :");
    for (auto caseCourante : m_mapCasesCibles)
    {
        std::cout << "[" << AfficheurDame::ConversionColonneLettre(caseCourante.first->RetourneY()) << caseCourante.first->RetourneX() + 1 << "] ";
    }
    std::cout << std::endl;

    int x, y;
    std::shared_ptr<Case> caseCible;
    do
    {
#ifdef IHM
        AdaptateurGraphique::AttendInteraction();
        y = AdaptateurGraphique::FournitAbscisse();
        x = AdaptateurGraphique::FournitOrdonne();
#else
        y = AfficheurDame::DemandeColonne(m_joueur);
        x = AfficheurDame::DemandeLigne(m_joueur);
#endif
        caseCible = m_lignes[x].RetourneCasePtr(y);
    } while (!testPresenceCaseDansMap(x, y, m_mapCasesCibles));
    m_caseCible = caseCible;
    std::cout << caseCible->RetourneX() << m_caseCible->RetourneY() << std::endl;
    std::cout << m_caseCible->RetourneX() << m_caseCible->RetourneY() << std::endl;
}

void GrilleDame::deplacePion()
{
    bool aMange = false;
    std::shared_ptr<Pion> p = m_caseOrigine->RetournePionPtr();
    m_caseOrigine->RetirePion();
    /// si c'est une capture on retire le pion adverse
    if (m_mapCasesCibles[m_caseCible] != nullptr)
    {
        m_mapCasesCibles[m_caseCible]->RetirePion();
        aMange = true;
    }
    m_caseCible->PlacePion(p);
    m_caseOrigine = m_caseCible;
    m_casesSelectionnables.clear();
    m_mapCasesCibles.clear();
    /// on test si le joueur doit manger plusieurs pions d'affilée
    if (aMange && caseContientPionPouvantManger(m_caseOrigine))
    {
        AfficheurDame::Affiche(this);
        std::cout << "Vous devez continuez à manger !" << std::endl;
        remplitMapCasesCibles();
        demandeCaseCible();
        deplacePion();
    }
    else if (doitDevenirDame())
    {
        m_caseOrigine->RetirePion();
        m_caseOrigine->PlacePion(std::make_shared<PionSuperDame>(PionSuperDame(m_joueur)));
    }
}

void GrilleDame::remplitCasesSelectionnables()
{
    /// on cherche toutes les cases contenant un pion pouvant manger un pion adverse
    for (auto caseCourante : m_cases)
    {

        if (caseContientPionPouvantManger(caseCourante))
        {
            m_casesSelectionnables.push_back(caseCourante);
        }
    }
    /// si aucun pion n'a d'obligation de capture, on cherche les pions pouvant avancer
    if (m_casesSelectionnables.empty())
    {
        for (auto caseCourante : m_cases)
        {
            if (caseContientPionPouvantAvancer(caseCourante))
            {
                m_casesSelectionnables.push_back(caseCourante);
            }
        }
    }
}

/// test si la case suivante dans la diagonale contient un pion adverse
/// et si celle encore après est vide pour manger
bool GrilleDame::caseContientPionPouvantManger(std::shared_ptr<Case> caseCourante) const
{

    /// récupération des coordonnées de la case
    int x = caseCourante->RetourneX();
    int y = caseCourante->RetourneY();

    /// les diagonales dans lesquelles se trouve la case
    std::shared_ptr<Diagonale> diagonale1 = std::make_shared<Diagonale>(m_diagonales[x + y]);
    std::shared_ptr<Diagonale> diagonale2 = std::make_shared<Diagonale>(m_diagonales[28 - x + y]);

    /// indice de la case dans chacune des diagonales
    int indiceDiagonale1 = diagonale1->RetourneIndiceCase(x, y);
    int indiceDiagonale2 = diagonale2->RetourneIndiceCase(x, y);

    /// test les possibilités sur les deux diagonales
    bool casePouvantMangerD1 = caseContientPionPouvantMangerDansDiagonale(diagonale1, indiceDiagonale1);
    bool casePouvantMangerD2 = caseContientPionPouvantMangerDansDiagonale(diagonale2, indiceDiagonale2);

    return casePouvantMangerD1 || casePouvantMangerD2;
}

bool GrilleDame::caseContientPionPouvantMangerDansDiagonale(std::shared_ptr<Diagonale> diagonale, int iCase) const
{

    std::shared_ptr<Case> caseTestee = diagonale->RetourneCasePtr(iCase);

    if (caseContientUnPionDuJoueur(caseTestee))
    {

        for (int j = -1; j <= 1; j += 2)
        {
            std::shared_ptr<Case> casePtr = diagonale->RetourneCasePtr(iCase + j);
            std::shared_ptr<Joueur> joueurCible = diagonale->RetourneJoueur(iCase + j);

            bool joueurExiste = Joueur::joueurExiste(joueurCible);
            bool joueurNEstPasJoueurCourant = m_joueur != joueurCible;
            bool prochaineCaseEstVide = diagonale->CaseEstVide(iCase + 2 * j);

            if (joueurExiste && joueurNEstPasJoueurCourant && prochaineCaseEstVide)
            {
                return true;
            }
        }
    }
    else if (caseContientUneDameDuJoueur(caseTestee))
    {
        for (int j = iCase - 1; j >= 1; j--)
        {
            if (diagonale->RetourneIdJoueur(j) != m_joueur->RetourneId() && diagonale->CaseEstVide(j-1) &&
                    diagonale->RetourneIdJoueur(j) != -1)
            {
                return true;
            }
            else if (!diagonale->CaseEstVide(j))
            {
                break;
            }
        }

        for (int j = iCase + 1; j < diagonale->RetourneLongueur() - 1; j++)
        {
            if (diagonale->RetourneIdJoueur(j) != m_joueur->RetourneId() && diagonale->CaseEstVide(j+1) &&
                    diagonale->RetourneIdJoueur(j) != -1)
            {
                return true;
            }
            else if (!diagonale->CaseEstVide(j))
            {
                break;
            }
        }
    }
    return false;
}

bool GrilleDame::caseContientPionPouvantAvancer(std::shared_ptr<Case> caseCourante) const
{
    /// récupération des coordonnées de la case
    int x = caseCourante->RetourneX();
    int y = caseCourante->RetourneY();

    /// les diagonales dans lesquelles se trouve la case
    std::shared_ptr<Diagonale> diagonale1 = std::make_shared<Diagonale>(m_diagonales[x + y]);
    std::shared_ptr<Diagonale> diagonale2 = std::make_shared<Diagonale>(m_diagonales[28 - x + y]);

    /// indice de la case dans chacune des diagonales
    int indiceDiagonale1 = diagonale1->RetourneIndiceCase(x, y);
    int indiceDiagonale2 = diagonale2->RetourneIndiceCase(x, y);

    /// test les possibilités sur les deux diagonales
    bool diagonaleValide = caseContientPionPouvantAvancerDansDiago(diagonale1, indiceDiagonale1) || caseContientPionPouvantAvancerDansDiago(diagonale2, indiceDiagonale2);
    return diagonaleValide;
}

/// test si la case suivante est vide
bool GrilleDame::caseContientPionPouvantAvancerDansDiago(std::shared_ptr<Diagonale> diagonale, int iCase) const
{
    auto caseTestee = diagonale->RetourneCasePtr(iCase);
    if (caseContientUneDameDuJoueur(caseTestee))
    {
        for (int j = 0; j < iCase; j++)
        {
            if (diagonale->CaseEstVide(j))
            {
                return true;
            }
            else if (diagonale->RetourneJoueur(j) == m_joueur)
            {
                break;
            }
        }

        for (int j = iCase + 1; j < diagonale->RetourneLongueur(); j++)
        {
            if (diagonale->CaseEstVide(j))
            {
                return true;
            }
            else if (diagonale->RetourneJoueur(j) == m_joueur)
            {
                break;
            }
        }
    }
    else if (caseContientUnPionDuJoueur(caseTestee))
    {
        int j = m_joueur->RetourneId() == 1 ? 1 : -1;
        return diagonale->CaseEstVide(iCase + j);
    }
    return false;
}

void GrilleDame::remplitMapCasesCibles()
{
    /// récupération des coordonnées de caseOrigine
    int x = m_caseOrigine->RetourneX();
    int y = m_caseOrigine->RetourneY();

    /// les diagonales dans lesquelles se trouve caseOrigine
    std::shared_ptr<Diagonale> diagonale1 = std::make_shared<Diagonale>(m_diagonales[x + y]);
    std::shared_ptr<Diagonale> diagonale2 = std::make_shared<Diagonale>(m_diagonales[28 - x + y]);

    /// indice de la case dans chacune des diagonales
    int indiceDiagonale1 = diagonale1->RetourneIndiceCase(x, y);
    int indiceDiagonale2 = diagonale2->RetourneIndiceCase(x, y);

    if (caseContientPionPouvantManger(m_caseOrigine))
    {
        remplitCasesMangeablesDansDiago(diagonale1, indiceDiagonale1);
        remplitCasesMangeablesDansDiago(diagonale2, indiceDiagonale2);
    }
    else
    {
        remplitCasesAtteignablesDansDiago(diagonale1, indiceDiagonale1);
        remplitCasesAtteignablesDansDiago(diagonale2, indiceDiagonale2);
    }
}

void GrilleDame::remplitCasesMangeablesDansDiago(std::shared_ptr<Diagonale> diagonale, int iCase)
{
    auto caseTestee = m_caseOrigine;

    if (caseContientUnPionDuJoueur(caseTestee))
    {
        for (int j = -1; j <= 1; j += 2)
        {

            bool joueurExiste = Joueur::joueurExiste(diagonale->RetourneJoueur(iCase + j));
            if (joueurExiste && diagonale->RetourneJoueur(iCase + j) != m_joueur && diagonale->CaseEstVide(iCase + 2 * j))
            {
                std::shared_ptr<Case> caseCible = diagonale->RetourneCasePtr(iCase + 2 * j);
                std::shared_ptr<Case> caseCourante = diagonale->RetourneCasePtr(iCase + j);

                m_mapCasesCibles.emplace(caseCible, caseCourante);
            }
        }
    }
    else if (caseContientUneDameDuJoueur(caseTestee))
    {
        for (int j = iCase - 1; j >= 1; j--)
        {
            if (diagonale->RetourneIdJoueur(j) != m_joueur->RetourneId() && diagonale->CaseEstVide(j-1) &&
                    diagonale->RetourneIdJoueur(j) != -1)
            {
                m_mapCasesCibles.emplace(diagonale->RetourneCasePtr(j-1), diagonale->RetourneCasePtr(j));
                break;
            }
            else if (!diagonale->CaseEstVide(j))
            {
                break;
            }
        }
        for (int j = iCase + 1; j < diagonale->RetourneLongueur() - 1; j++)
        {
            if (diagonale->RetourneIdJoueur(j) != m_joueur->RetourneId() && diagonale->CaseEstVide(j+1) &&
                    diagonale->RetourneIdJoueur(j) != -1)
            {
                m_mapCasesCibles.emplace(diagonale->RetourneCasePtr(j+1), diagonale->RetourneCasePtr(j));
                break;
            }
            else if (!diagonale->CaseEstVide(j))
            {
                break;
            }
        }
    }
}

void GrilleDame::remplirListePossible(std::shared_ptr<Diagonale> diagonale, const int indexMin, const int indexMax)
{
    if(indexMin ==  0)
    {
        for (int j = indexMax - 1; j >= indexMin; j--)
        {
            if (diagonale->CaseEstVide(j))
            {
                std::shared_ptr<Case> pointeurCaseCourante = diagonale->RetourneCasePtr(j);
                m_mapCasesCibles.emplace(pointeurCaseCourante, nullptr);
            }
            if (diagonale->RetourneJoueur(j) == m_joueur)
            {
                break;
            }
        }
    }
    else
    {
        for (int j = indexMin; j < indexMax; j++)
        {
            if (diagonale->CaseEstVide(j))
            {
                std::shared_ptr<Case> pointeurCaseCourante = diagonale->RetourneCasePtr(j);
                m_mapCasesCibles.emplace(pointeurCaseCourante, nullptr);
            }
            if (diagonale->RetourneJoueur(j) == m_joueur)
            {
                break;
            }
        }
    }

}

void GrilleDame::remplitCasesAtteignablesDansDiago(std::shared_ptr<Diagonale> diagonale, int iCase)
{
    auto caseTestee = diagonale->RetourneCasePtr(iCase);
    if (caseContientUnPionDuJoueur(caseTestee))
    {
        int j = m_joueur->RetourneId() == 1 ? 1 : -1;

        if (diagonale->CaseEstVide(iCase + j))
        {
            m_mapCasesCibles.emplace(diagonale->RetourneCasePtr(iCase + j), nullptr);
        }
    }
    else if (caseContientUneDameDuJoueur(caseTestee))
    {
        remplirListePossible(diagonale, 0, iCase);
        remplirListePossible(diagonale, iCase + 1, diagonale->RetourneLongueur());
    }
}

bool GrilleDame::testPresenceCaseDansVector(int x, int y, std::vector<std::shared_ptr<Case>> cases) const
{
    for (auto caseCourante : cases)
    {
        bool EstCoordXBonne = caseCourante->RetourneX() == x;
        bool EstCoordYBonne = caseCourante->RetourneY() == y;

        if (EstCoordXBonne && EstCoordYBonne)
        {
            return true;
        }
    }
    return false;
}

bool GrilleDame::testPresenceCaseDansMap(int x, int y,
                                         std::map<std::shared_ptr<Case>, std::shared_ptr<Case>> cases) const
{
    for (auto caseCourante : cases)
    {
        bool EstCoordXBonne = caseCourante.first->RetourneX() == x;
        bool EstCoordYBonne = caseCourante.first->RetourneY() == y;

        if (EstCoordXBonne && EstCoordYBonne)
        {
            return true;
        }
    }
    return false;
}

bool GrilleDame::doitDevenirDame() const
{
    int x = m_caseOrigine->RetourneX();
    int y = m_caseOrigine->RetourneY();

    int idJoueurCourant = m_joueur->RetourneId();

    if (idJoueurCourant == 1)
    {
        return m_lignes[9].RetourneIndiceCase(x, y) != -1;
    }
    else if (idJoueurCourant == 2)
    {
        return m_lignes[0].RetourneIndiceCase(x, y) != -1;
    }
    else
    {
        return false;
    }
}

void GrilleDame::initialiseGrille()
{
    initialiseVectors();
}

void GrilleDame::initialiseCases(std::shared_ptr<Joueur> joueur1, std::shared_ptr<Joueur> joueur2)
{
    /// cf readme pour plus de détails
    int iLignePourDiagonalesSensInverse = m_nbLignes - 1 + m_nbLignes - 1 + m_nbColonnes;
    int iCase = 2; /// permet de gérer le placement initial des pions
    int iDerniereCaseBlancs = 44;
    int iPremiereCaseNoirs = 68;
    for (int i = 0; i < m_nbLignes; i++)
    {
        for (int j = 0; j < m_nbColonnes; j++)
        {
            std::shared_ptr<CaseMonoPion> sptrCase = std::make_shared<CaseMonoPion>(CaseMonoPion(i, j));
            if (iCase % 2 == 0)
            {
                std::shared_ptr<PionDame> p;
                if (iCase <= iDerniereCaseBlancs)
                {
                    p = std::make_shared<PionDame>(PionDame(joueur1));
                }
                else if (iCase >= iPremiereCaseNoirs)
                {
                    p = std::make_shared<PionDame>(PionDame(joueur2));
                }

                if (p != nullptr)
                {
                    sptrCase->PlacePion(p);
                }
            }
            m_cases.push_back(sptrCase);
            m_lignes[i].AjouterCase(sptrCase);
            m_colonnes[j].AjouterCase(sptrCase);
            m_diagonales[i + j].AjouterCase(sptrCase);
            m_diagonales[iLignePourDiagonalesSensInverse + j].AjouterCase(sptrCase);
            iCase++;
        }
        iLignePourDiagonalesSensInverse--;
        iCase++;
    }
}

void GrilleDame::initialiseCasesImport(std::shared_ptr<Joueur> joueur1, std::shared_ptr<Joueur> joueur2, Importer import)
{

    int iLignePourDiagonalesSensInverse = m_nbLignes - 1 + m_nbLignes - 1 + m_nbColonnes;
    int iCase = 2; /// permet de gérer le placement initial des pions
    for (int i = 0; i < m_nbLignes; i++)
    {
        for (int j = 0; j < m_nbColonnes; j++)
        {
            std::shared_ptr<CaseMonoPion> sptrCase = std::make_shared<CaseMonoPion>(CaseMonoPion(i, j));
            int importValue = import.Valeur(i * m_nbColonnes + j);
            if (importValue != 0)
            {
                std::shared_ptr<Pion> p;
                if(importValue == 'b')
                {
                     p = std::make_shared<PionDame>(PionDame(joueur1));
                }
                else if(importValue == 'B')
                {
                    p = std::make_shared<PionSuperDame>(PionSuperDame(joueur1));
                }
                else if(importValue == 'n')
                {
                    p = std::make_shared<PionDame>(PionDame(joueur2));
                }
                else if(importValue == 'N')
                {
                    p = std::make_shared<PionSuperDame>(PionSuperDame(joueur2));
                }
                if(p != nullptr) sptrCase->PlacePion(p);

            }
            m_cases.push_back(sptrCase);
            m_lignes[i].AjouterCase(sptrCase);
            m_colonnes[j].AjouterCase(sptrCase);
            m_diagonales[i + j].AjouterCase(sptrCase);
            m_diagonales[iLignePourDiagonalesSensInverse + j].AjouterCase(sptrCase);
            iCase++;
        }
        iLignePourDiagonalesSensInverse--;
        iCase++;
    }
}

std::string GrilleDame::EnTexte() const
{
    int i = 1;
    std::string grilleEnTexte = "--------------------------------------------------\n";
    for (Ligne ligneCourante : m_lignes)
    {
        grilleEnTexte = grilleEnTexte + ligneCourante.EnTexte() + "\t[" + std::to_string(i) + "]\n";
        grilleEnTexte = grilleEnTexte + "--------------------------------------------------\n";
        i++;
    }
    return grilleEnTexte;
}

bool GrilleDame::caseContientUnPionDuJoueur(std::shared_ptr<Case> laCase) const
{
    return ((laCase->RetourneJoueurPtr() == m_joueur) && (std::dynamic_pointer_cast<PionDame>(laCase->RetournePionPtr())));
}

bool GrilleDame::caseContientUneDameDuJoueur(std::shared_ptr<Case> laCase) const
{
    return (laCase->RetourneJoueurPtr() == m_joueur) && (std::dynamic_pointer_cast<PionSuperDame>(laCase->RetournePionPtr()));
}
