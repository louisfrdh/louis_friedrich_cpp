#include "utils.h"

bool utils::intInRange(const int value, const int minValue, const int maxValue)
{
	return maxValue >= minValue ? //
			   value <= maxValue && value >= minValue
								: //
			   value <= minValue && value >= maxValue;
}

void utils::removeLastChar(std::string &texte)
{
	texte = texte.substr(0, texte.size() - 1);
}

int utils::ConversionLettreColonne(char lettreDeColonne)
{ // ASCII CONVERSION
	int x = static_cast<int>(std::toupper(lettreDeColonne) - 'A' + 1);
	return x - 1;
}

char utils::ConversionColonneLettre(const int numeroVersChar, const char maxChar)
{
	char convertedNum = numeroVersChar + 'A';
	return utils::intInRange(convertedNum, 'A', maxChar) ? convertedNum : -1;
}
