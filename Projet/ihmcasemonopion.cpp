#include "constantes.h"

#ifdef IHM
#include "ihmcasemonopion.h"

IHMCaseMonoPion::IHMCaseMonoPion(QWidget *parent, int abcisse, int ordonne) : QPushButton(parent), m_abcisse(abcisse), m_ordonne(ordonne)
{
}

int IHMCaseMonoPion::RenvoiAbcisse() const
{
	return m_abcisse;
}

int IHMCaseMonoPion::RenvoiOrdonne() const
{
	return m_ordonne;
}

#endif // IHM
