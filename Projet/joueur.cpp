#include "joueur.h"

std::string Joueur::RetourneNom() const
{
	return m_nom;
};

std::string Joueur::EnTexte() const
{
	return std::to_string(m_id);
};

std::string Joueur::getSaveString() const
{
	return EnTexte();
};

int Joueur::RetourneId() const
{
	return m_id;
};

bool Joueur::joueurEstLePremier(const std::shared_ptr<Joueur> joueur)
{
	if (joueur != NULL)
	{
		return (joueur->RetourneId() == 1);
	}
	else
	{
		return false;
	}
};

bool Joueur::joueurExiste(const std::shared_ptr<Joueur> joueur)
{
	if (joueur != NULL)
	{
		return (joueur->RetourneId() != -1);
	}
	else
	{
		return false;
	}
};
