#include "afficheurpuissance4.h"

int AfficheurPuissance4::DemandeCordonnee(const std::shared_ptr<Joueur> UnJoueur, const int size)
{
    int choix;
    bool choixNonconforme = true;
    while (choixNonconforme)
    {
        choix = AttendUnEntier(UnJoueur->RetourneNom() + ",merci de saisir un numéro de colonne");
        if (utils::intInRange(choix, 1, size))
        {
            choixNonconforme = false;
        }
        else
        {
            InformeUtilisateurDe(">>Etes vous sur de saisir un chiffre dans les bornes ?");
        }
    }
    choix--; // convertit en la valeur correspondante pour le programme
    return choix;
}

void AfficheurPuissance4::Affiche(const Grille *grille)
{
    NettoyageConsole();
    std::cout << "=== [PUISSANCE 4] ===" << std::endl
              << std::endl;
    std::cout << grille->EnTexte() << std::endl;
    std::cout << "--------------------------------" << std::endl;
}
