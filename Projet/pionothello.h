#ifndef PIONOTHELLO_H
#define PIONOTHELLO_H

#include "pion.h"

class PionOthello : public Pion
{
public:
    PionOthello(std::shared_ptr<Joueur> joueur);
    //void Afficher() const override;
    void ChangeProprietaire(std::shared_ptr<Joueur> joueur);
    std::string EnTexte() const override;
};

#endif // PIONOTHELLO_H
