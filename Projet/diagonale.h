#ifndef DIAGONALE_H
#define DIAGONALE_H

#include "ensemblelineairecases.h"
#include <vector>
#include <memory>

class Diagonale : public EnsembleLineaireCases
{
public:
    std::string EnTexte() const override;

private:
};

#endif // DIAGONALE_H
