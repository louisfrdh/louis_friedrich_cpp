#ifndef CONTENANT_PION_H
#define CONTENANT_PION_H

#include "pion.h"
#include <memory>

class ContenantPion
{
public:
    virtual bool EstVide() const = 0;

    virtual void PlacePion(std::shared_ptr<Pion> pion) = 0;
    virtual void RetirePion() = 0;
    virtual void Afficher() const = 0;

    virtual std::shared_ptr<Joueur> RetourneJoueur() const = 0;
    virtual std::string EnTexte() const = 0;
};

#endif // CONTENANT_PION_H
