#ifndef IHMPUISSANCE4_H
#define IHMPUISSANCE4_H

#include "ihmjeugrille.h"

class IHMPuissance4 : public IHMJeuGrille
{
    Q_OBJECT
public:
    IHMPuissance4(QWidget *parent);
    IHMPuissance4(QWidget *parent, Importer import);
    ~IHMPuissance4();

protected:
    virtual void InitialiseDesCases() override;
    virtual void MiseEnFormeCase(IHMCaseMonoPion* nouvelleCase) override;
    virtual IHMCaseMonoPion* DefinitionCase(int abcisse,int ordonne) override;
    virtual void AssocieImagePion(std::shared_ptr<Pion> pion,IHMCaseMonoPion* caseCible) override;
};

#endif // IHMPUISSANCE4_H
