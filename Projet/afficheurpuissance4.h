#ifndef AFFICHEURPUISSANCE4_H
#define AFFICHEURPUISSANCE4_H

#include "outilsconsole.h"
#include "joueur.h"
#include "grillepuissance4.h"
#include "utils.h"


class AfficheurPuissance4 : OutilsConsole
{
public:
    static int DemandeCordonnee(const std::shared_ptr<Joueur> UnJoueur, const int size);
    
    static void Affiche(const Grille *grille);
};

#endif // AFFICHEURPUISSANCE4_H
