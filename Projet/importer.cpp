#include "importer.h"
#include "outilsconsole.h"
#include <sstream>
#include <fstream>
#include <stdlib.h>

Importer::Importer(std::string cheminVersFichier)
{
    std::filebuf fb;
    if (fb.open(cheminVersFichier, std::ios::in))
    {
        std::istream is(&fb);
        std::istream &isConst = is;
        std::vector<std::string> line = Importer::ProchaineLigneEtSeparationEnToken(isConst);
        type = line[0];
        idJoueurTour = stoi(line[line.size() - 1]);
        for (int i = 2; i < (int)line.size() - 1; i++)
        {
            std::string cell = line[i];
            try
            {
                valeurs.push_back(stoi(cell, nullptr));
            }
            catch (const std::exception &e)
            {
                OutilsConsole::InformeUtilisateurDe("");
                OutilsConsole::InformeUtilisateurDe("/!\\ Problème dans la sauvegarde : le fichier est corrompu");
                OutilsConsole::InformeUtilisateurDe("");

                exit(0);
            }
        }
    }
    else
    {
        std::cout << "Tentative de lecture le fichier échouée" << std::endl;
    }
}

std::vector<std::string> Importer::ProchaineLigneEtSeparationEnToken(std::istream &str)
{
    std::vector<std::string> result;
    std::string line;
    std::getline(str, line);

    std::stringstream lineStream(line);
    std::string cell;

    while (std::getline(lineStream, cell, ','))
    {
        result.push_back(cell);
    }

    // Check si il y a une virgule sans valeur derriere
    if (!lineStream && cell.empty())
    {
        result.push_back("");
    }
    return result;
}

int Importer::Valeur(int i) const
{
    return valeurs[i];
}

int Importer::Valeur(int x, int y, int nbColonnes) const
{
    return valeurs[x * nbColonnes + y];
}

int Importer::IdJoueurTour() const
{
    return idJoueurTour;
}

std::string Importer::getType() const
{
    return type;
}
