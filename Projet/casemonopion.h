#ifndef CASEMONOPION_H
#define CASEMONOPION_H

#include <memory>
#include "case.h"

class CaseMonoPion : public Case
{
public:
    CaseMonoPion(){};
    CaseMonoPion(int x, int y);
    CaseMonoPion(std::shared_ptr<Pion> pion) : m_pion(pion){};

    bool EstVide() const override;
    bool EstIdDuJoueur(const int id) const override;

    void PlacePion(std::shared_ptr<Pion> pion) override;
    void RetirePion() override;

    int RetourneX() const override;
    int RetourneY() const override;

    int RetourneIdJoueur() const override;

    int RetourneCaseValue() const override {return m_pion->RetourneIdJoueur();};

    std::shared_ptr<Joueur> RetourneJoueurPtr() const override;
    std::shared_ptr<Pion> RetournePionPtr() const override;

    std::string EnTexte() const override;
    std::string getDescription() const override;

private:
    int m_x;
    int m_y;

    std::shared_ptr<Pion> m_pion;
};

#endif // CASEMONOPION_H
