#include "constantes.h"
#include "grillemorpion.h"
#include "pionmorpion.h"
#include "afficheurmorpion.h"

GrilleMorpion::GrilleMorpion() : Grille(NB_LIGNES, NB_COLONNES, NB_JOUEURS, "Morpion")
{
    initialiseVectors();
    initialiseCases();
}

GrilleMorpion::GrilleMorpion(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import) : Grille(NB_LIGNES, NB_COLONNES, NB_JOUEURS, "Morpion")
{
    initialiseVectors();
    initialiseCasesImport(j1, j2, import);
}

void GrilleMorpion::DeposerJeton(int x, int y, std::shared_ptr<Joueur> joueur)
{
    if (CaseEstVide(x, y))
    {
        m_lignes[x].PlacePion(y, std::make_shared<PionMorpion>(PionMorpion(joueur)));
    }
}

void GrilleMorpion::TourJoueur(std::shared_ptr<Joueur> joueur)
{
    AfficheurMorpion::Affiche(this);

    int x = -1;
    int y = -1;
    bool conditionInvalide = true;

    while (conditionInvalide)
    {
#ifdef IHM
        AdaptateurGraphique::AttendInteraction();
        x = AdaptateurGraphique::FournitOrdonne();
        y = AdaptateurGraphique::FournitAbscisse();
#else
        x = AfficheurMorpion::DemandeCordonnee(joueur, "ligne");
        y = AfficheurMorpion::DemandeCordonnee(joueur, "colonne");
#endif //IHM

        bool xInvalide = !utils::intInRange(x, 0, NB_LIGNES - 1);
        bool yInvalide = !utils::intInRange(y, 0, NB_COLONNES - 1);
        conditionInvalide = xInvalide || yInvalide || !CaseEstVide(x, y);
    }

    DeposerJeton(x, y, joueur);
}

bool GrilleMorpion::TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const
{
    for (auto ligneCourante : m_lignes)
    {
        if (ligneCourante.EstComplete(joueur))
        {
            return true;
        }
    }

    for (auto colonneCourante : m_colonnes)
    {
        if (colonneCourante.EstComplete(joueur))
        {
            return true;
        }
    }

    for (auto diagonaleCourante : m_diagonales)
    {
        if (diagonaleCourante.EstComplete(joueur, NB_LIGNES))
        {
            return true;
        }
    }

    return false;
}

void GrilleMorpion::initialiseCasesImport(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import)
{
    int g = m_nbLignes - 1 + m_nbLignes - 1 + m_nbColonnes;
    for (int x = 0; x < m_nbLignes; x++)
    {
        for (int y = 0; y < m_nbColonnes; y++)
        {
            std::shared_ptr<CaseMonoPion> sptrCase = std::make_shared<CaseMonoPion>(CaseMonoPion(x, y));
            int importValue = import.Valeur(x, y, m_nbColonnes);
            if (importValue != 0)
            {
                std::shared_ptr<PionMorpion> p;

                if (importValue == 1)
                {
                    p = std::make_shared<PionMorpion>(PionMorpion(j1));
                }
                else if (importValue == 2)
                {
                    p = std::make_shared<PionMorpion>(PionMorpion(j2));
                }

                if (p != nullptr)
                {
                    sptrCase->PlacePion(p);
                }
            }

            m_cases.push_back(sptrCase);
            m_lignes[x].AjouterCase(sptrCase);
            m_colonnes[y].AjouterCase(sptrCase);
            m_diagonales[x + y].AjouterCase(sptrCase);
            m_diagonales[g + y].AjouterCase(sptrCase);
        }

        g--;
    }    
}

std::string GrilleMorpion::EnTexte() const
{
    std::string grilleEnTexte = "---------------\n";
    for (Ligne ligneCourante : m_lignes)
    {
        grilleEnTexte += ligneCourante.EnTexte() + "\n" + "---------------\n";
    }

    return grilleEnTexte;
}
