#ifndef COLONNE_H
#define COLONNE_H

#include "ensemblelineairecases.h"
#include <vector>
#include <memory>

class Colonne : public EnsembleLineaireCases
{
public:
    std::string EnTexte() const override;
};

#endif // COLONNE_H
