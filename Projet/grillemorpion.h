#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H

#include "grille.h"
#include "pionmorpion.h"
#include "casemonopion.h"
#include "utils.h"

class GrilleMorpion : public Grille
{
public:
    const static int NB_COLONNES = 3;
    const static int NB_LIGNES = 3;
    const static int NB_JOUEURS = 2;

    GrilleMorpion();
    GrilleMorpion(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import);

    void TourJoueur(std::shared_ptr<Joueur> joueur) override;
    bool TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const override;
    std::string EnTexte() const override;

private:
    void DeposerJeton(int x, int y, std::shared_ptr<Joueur> joueur);
    void initialiseCasesImport(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import) override;
};

#endif // GRILLEMORPION_H
