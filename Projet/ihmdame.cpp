#include "constantes.h"
#ifdef IHM

#include "ihmdame.h"

IHMDame::IHMDame(QWidget *parent) : IHMJeuGrille(parent, GrilleDame::NB_COLONNES, GrilleDame::NB_LIGNES)
{
    InitialiseDesCases();
    IHMJeuGrille::m_modelJeuGrille = new ThreadJeu(3);
    m_modelJeuGrille->start(QThread::HighPriority);
    RafraichirToutesLesCases();
}

IHMDame::IHMDame(QWidget *parent, Importer import) : IHMJeuGrille(parent, import, GrilleDame::NB_COLONNES, GrilleDame::NB_LIGNES)
{
    InitialiseDesCases();
    m_modelJeuGrille = new ThreadJeu(import);
    m_modelJeuGrille->start(QThread::HighPriority);
    RafraichirToutesLesCases();
}

IHMDame::~IHMDame()
{
}

void IHMDame::InitialiseDesCases()
{
    int abcisse, ordonne;
    int caseTotale = IHMJeuGrille::m_caseLignes * IHMJeuGrille::m_casesColonnes;
    for (int i = 0; i < caseTotale; i++)
    {
        abcisse = i % IHMJeuGrille::m_casesColonnes;
        ordonne = static_cast<int>(i / IHMJeuGrille::m_casesColonnes);
        IHMCaseMonoPion *nouvelleCase = DefinitionCase(abcisse, ordonne);
        MiseEnFormeCase(nouvelleCase, i, ordonne);
        IHMJeuGrille::m_affichageGrille->addWidget(nouvelleCase, ordonne, abcisse, Qt::AlignCenter);
    }
    m_affichageGrille->setSpacing(1);
    this->setLayout(IHMJeuGrille::m_affichageGrille);
}

void IHMDame::MiseEnFormeCase(IHMCaseMonoPion *nouvelleCase, int indexCase, int ordonne)
{
    nouvelleCase->setFlat(true);
    nouvelleCase->setIconSize(QSize(70, 70));
    nouvelleCase->setFixedSize(70, 70);
    //nouvelleCase->setStyleSheet("IHMCaseMonoPion {border: 2px solid;}");
    QPalette pal = nouvelleCase->palette();
    if (ordonne % 2 == 0)
    {
        if (indexCase % 2 == 0)
            pal.setColor(QPalette::Button, QColor(160, 100, 0, 100));
        else
            pal.setColor(QPalette::Button, QColor(230, 150, 0, 100));
    }
    else
    {
        if (indexCase % 2 == 0)
            pal.setColor(QPalette::Button, QColor(230, 150, 0, 100));
        else
            pal.setColor(QPalette::Button, QColor(160, 100, 0, 100));
    }

    nouvelleCase->setAutoFillBackground(true);
    nouvelleCase->setPalette(pal);
}
void IHMDame::MiseEnFormeCase(IHMCaseMonoPion *nouvelleCase)
{
}

void IHMDame::AssocieImagePion(std::shared_ptr<Pion> pion, IHMCaseMonoPion *caseCible){
    if(pion == nullptr) caseCible->setIcon(QIcon(""));
    else if (pion->RetourneIdJoueur() == 1)
    {
        if (typeid(*pion)== typeid(PionSuperDame)) caseCible->setIcon(QIcon(":/Graphics/Pion/Dame/SuperDameBlanche.png"));
        else caseCible->setIcon(QIcon(":/Graphics/Pion/Dame/DameBlanche.png"));
    }
    else if (pion->RetourneIdJoueur() ==2)
    {
        if (typeid(*pion)== typeid(PionSuperDame)) caseCible->setIcon(QIcon(":/Graphics/Pion/Dame/SuperDameNoire.png"));
        else caseCible->setIcon(QIcon(":/Graphics/Pion/Dame/DameNoir.png"));
    }
}

IHMCaseMonoPion *IHMDame::DefinitionCase(int abcisse, int ordonne)
{
    IHMCaseMonoPion *nouvelleCase = new IHMCaseMonoPion(this, abcisse, ordonne);
    IHMJeuGrille::m_ensembleDeCase.push_back(nouvelleCase);
    QObject::connect(nouvelleCase, SIGNAL(pressed()), this, SLOT(EnvoiCordonne()));
    QObject::connect(nouvelleCase, SIGNAL(clicked()), this, SLOT(RafraichirToutesLesCases()),Qt::QueuedConnection);
    QObject::connect(nouvelleCase,SIGNAL(clicked()),this,SLOT(NotificationDeVictoire()));
    return nouvelleCase;
}
#endif //IHM
