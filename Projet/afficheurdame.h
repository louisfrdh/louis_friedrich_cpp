#ifndef AFFICHEURDAME_H
#define AFFICHEURDAME_H

#include "outilsconsole.h"
#include "grilledame.h"
#include "utils.h"

class AfficheurDame : OutilsConsole
{
public:
    static int DemandeLigne(const std::shared_ptr<Joueur> UnJoueur);
    static int DemandeColonne(const std::shared_ptr<Joueur> UnJoueur);

    static char ConversionColonneLettre(const int numeroColonne);

    static void Affiche(const Grille *grille);
};

#endif // AFFICHEURDAME_H
