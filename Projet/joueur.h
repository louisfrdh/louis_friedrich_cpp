#ifndef JOUEUR_H
#define JOUEUR_H

#include <iostream>
#include <string>
#include <memory>

class Joueur
{
public:
    std::string m_nom;

    Joueur(int id) : m_id(id){};
    Joueur(int id, std::string nomDuJoueur) : m_nom(nomDuJoueur), m_id(id){};

    int RetourneId() const;

    std::string RetourneNom() const;
    std::string EnTexte() const;
    std::string getSaveString() const;

    static bool joueurEstLePremier(const std::shared_ptr<Joueur> joueur);
    static bool joueurExiste(const std::shared_ptr<Joueur> joueur);

    bool operator==(const Joueur &joueur) { return (joueur.m_id == this->m_id); };

private:
    int m_id;
};

#endif // JOUEUR_H
