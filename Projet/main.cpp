#include "outilsconsole.h"
#include "jeugrille.h"
#include "grillepuissance4.h"
#include "grilledame.h"
#include "grilleothello.h"
#include "utils.h"

#include "constantes.h"
#include "fileManager.h"

#ifdef IHM
#include "ihmmenu.h"

#include "adaptateurgraphique.h"
#include <QApplication>
#endif //IHM
int main(int argc, char *argv[])
{
#ifdef IHM

    QApplication application(argc, argv);
    IHMMenu menuPrincipale;
    menuPrincipale.show();

    return application.exec();
#else
    OutilsConsole::InformeUtilisateurDe("hello");

    JeuGrille jeu = JeuGrille(2);
    int choixJeu;
    do
    {
        OutilsConsole::NettoyageConsole();
        OutilsConsole::InformeUtilisateurDe("A quel jeu souhaitez vous jouer ?");
        OutilsConsole::InformeUtilisateurDe("1 - Morpion");
        OutilsConsole::InformeUtilisateurDe("2 - Puissance 4");
        OutilsConsole::InformeUtilisateurDe("3 - Dame");
        OutilsConsole::InformeUtilisateurDe("4 - Othello");
        OutilsConsole::InformeUtilisateurDe("5 - Importer");
        OutilsConsole::InformeUtilisateurDe("0 - Quitter");
        choixJeu = OutilsConsole::AttendUnEntier();
    } while (!utils::intInRange(choixJeu, 0, 5));
    switch (choixJeu)
    {
    case 1:
        jeu.AttribuerGrille(std::make_shared<GrilleMorpion>(GrilleMorpion()));
        break;
    case 2:
        jeu.AttribuerGrille(std::make_shared<GrillePuissance4>(GrillePuissance4()));
        break;
    case 3:
        jeu.AttribuerGrille(std::make_shared<GrilleDame>(GrilleDame(jeu.RetourneJoueur(1), jeu.RetourneJoueur(2))));
        break;
    case 4:
        jeu.AttribuerGrille(std::make_shared<GrilleOthello>(GrilleOthello(jeu.RetourneJoueur(1), jeu.RetourneJoueur(2))));
        break;
    case 5:
        jeu.AttribuerGrilleImporte(Importer("text.grid.txt"));
        break;
    case 0:
        return 1;
    }
    jeu.Jouer();
    main(argc, argv);
#endif //IHM
}
