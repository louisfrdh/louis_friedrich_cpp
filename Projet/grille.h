#ifndef GRILLE_H
#define GRILLE_H

#include "case.h"
#include "casemonopion.h"
#include "ligne.h"
#include "colonne.h"
#include "diagonale.h"
#include "joueur.h"
#include "importer.h"
#include "adaptateurgraphique.h"
#include <vector>
#include <memory>
#include "utils.h"


class Grille
{
public:
    Grille(int nbLignes, int nbColonnes, int nbJOueurs, std::string gameName);

    virtual void TourJoueur(std::shared_ptr<Joueur> joueur) = 0;
    virtual bool TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const = 0;

    virtual std::string EnTexte() const = 0;

    int RetourneNbJoueurs() const;
    int RetourneNbLignes() const;
    int RetourneNbColonnes() const;
    int JoueurDansLaCase(int x, int y) const;
    std::shared_ptr<Pion> PionDansLaCase(int x, int y) const;
    bool EstPleine() const;
    bool CaseEstVide(int x, int y) const;

    std::string getSaveString() const;

protected:
    std::string getGridDescription() const;
    std::string getGameName() const;

    std::vector<std::shared_ptr<Case>> m_cases;
    std::vector<Ligne> m_lignes;
    std::vector<Colonne> m_colonnes;
    std::vector<Diagonale> m_diagonales;

    virtual void initialiseCasesImport(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import) = 0;

    void initialiseVectors();
    void initialiseCases();

    int demanderIndiceCase(const std::shared_ptr<Joueur> joueur, const std::string ligneOuColonne);

    const int m_nbLignes;
    const int m_nbColonnes;
    const int m_nbJoueurs;

    std::string m_name;
};

#endif // GRILLE_H
