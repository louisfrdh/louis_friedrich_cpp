#include "piondame.h"
#include <algorithm>

PionDame::PionDame(std::shared_ptr<Joueur> joueur)
{
    m_joueur = joueur;
}

std::string PionDame::EnTexte() const
{
    std::string valeurRetour = "   ";

    if (m_joueur != NULL)
    {
        valeurRetour = (m_joueur->RetourneId() == 1) ? "(b)" : "(n)";
    }

    return valeurRetour;
}
int  PionDame::RetourneValeur() const {
    if(m_joueur->RetourneId() == 1)
    {
        return 'b';
    }
    else return 'n';
}
