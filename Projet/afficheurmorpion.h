#ifndef AFFICHEURMORPION_H
#define AFFICHEURMORPION_H

#include "outilsconsole.h"
#include "joueur.h"
#include "grillemorpion.h"
#include "utils.h"


class AfficheurMorpion : OutilsConsole
{
public:
    static int DemandeCordonnee(const std::shared_ptr<Joueur> UnJoueur, const std::string messagePourJoueur);

    static void Affiche(const Grille *grille);
};

#endif // AFFICHEURMORPION_H
