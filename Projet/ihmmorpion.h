#ifndef IHMMORPION_H
#define IHMMORPION_H

#include <QWidget>
#include <QLabel>
#include <QGridLayout>
#include <iostream>
#include <QTimer>

#include <QtDebug>

#include "threadjeu.h"
#include "ihmjeugrille.h"
#include "adaptateurgraphique.h"

class IHMMorpion  : public IHMJeuGrille
{
    Q_OBJECT
public:
    IHMMorpion(QWidget *parent);
    IHMMorpion(QWidget *parent, Importer import);
    ~IHMMorpion();
protected:
    virtual void InitialiseDesCases() override;
    virtual void MiseEnFormeCase(IHMCaseMonoPion* nouvelleCase) override;
    virtual IHMCaseMonoPion* DefinitionCase(int abcisse,int ordonne) override;
    virtual void AssocieImagePion(std::shared_ptr<Pion> pion,IHMCaseMonoPion* caseCible) override;
};

#endif // IHMMORPION_H
