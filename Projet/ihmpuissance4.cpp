#include "constantes.h"
#ifdef IHM
#include "ihmpuissance4.h"

IHMPuissance4::IHMPuissance4(QWidget *parent) : IHMJeuGrille(parent,GrillePuissance4::NB_COLONNES,GrillePuissance4::NB_LIGNES)
{
    InitialiseDesCases();
    IHMJeuGrille::m_modelJeuGrille = new ThreadJeu(std::make_shared<GrillePuissance4>(GrillePuissance4()));
    m_modelJeuGrille->start(QThread::HighPriority);
    RafraichirToutesLesCases();
}

IHMPuissance4::IHMPuissance4(QWidget *parent, Importer import) : IHMJeuGrille(parent,import,GrillePuissance4::NB_COLONNES,GrillePuissance4::NB_LIGNES)
{
    InitialiseDesCases();
    m_modelJeuGrille = new ThreadJeu(import);
    m_modelJeuGrille->start(QThread::HighPriority);
    RafraichirToutesLesCases();
}

IHMPuissance4::~IHMPuissance4()
{
}

void IHMPuissance4::InitialiseDesCases()
{
    int abcisse,ordonne;
    for(int i=0;i < IHMJeuGrille::m_caseLignes*IHMJeuGrille::m_casesColonnes; i++){
        abcisse = i%IHMJeuGrille::m_casesColonnes;
        ordonne = static_cast<int>(i/IHMJeuGrille::m_casesColonnes);
        IHMCaseMonoPion* nouvelleCase = DefinitionCase(abcisse,ordonne);
        MiseEnFormeCase(nouvelleCase);
        IHMJeuGrille::m_affichageGrille->addWidget(nouvelleCase,ordonne,abcisse,Qt::AlignCenter);
    }
    m_affichageGrille->setSpacing(1);
    this->setLayout(IHMJeuGrille::m_affichageGrille);
}

void IHMPuissance4::MiseEnFormeCase(IHMCaseMonoPion* nouvelleCase)
{
    nouvelleCase->setFlat(true);
    nouvelleCase->setIconSize(QSize(70, 70));
    nouvelleCase->setFixedSize(70,70);
    nouvelleCase->setStyleSheet("IHMCaseMonoPion {border: 2px solid;border-top-width: 0;border-bottom-width: 0;}");
    QPalette pal = nouvelleCase->palette();
    pal.setColor(QPalette::Button, QColor(255, 255,255,0));
    nouvelleCase->setAutoFillBackground(true);
    nouvelleCase->setPalette(pal);
}

void IHMPuissance4::AssocieImagePion(std::shared_ptr<Pion> pion,IHMCaseMonoPion* caseCible)
{
    if(pion == nullptr)return;
    else if(pion->RetourneIdJoueur() == 1)caseCible->setIcon(QIcon(":/Graphics/Pion/Puissance4/jetonA.png"));
    else if(pion->RetourneIdJoueur() == 2)caseCible->setIcon(QIcon(":/Graphics/Pion/Puissance4/jetonB.png"));
}


IHMCaseMonoPion* IHMPuissance4::DefinitionCase(int abcisse,int ordonne)
{
    IHMCaseMonoPion* nouvelleCase = new IHMCaseMonoPion(this,abcisse,ordonne);
    IHMJeuGrille::m_ensembleDeCase.push_back(nouvelleCase);
    QObject::connect(nouvelleCase, SIGNAL(pressed()),this,SLOT(EnvoiCordonne()));
    QObject::connect(nouvelleCase, SIGNAL(clicked()),this,SLOT(RafraichirToutesLesCases()));
    QObject::connect(nouvelleCase,SIGNAL(clicked()),this,SLOT(NotificationDeVictoire()));
    return nouvelleCase;
}

#endif //IHM
