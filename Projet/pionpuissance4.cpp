#include "pionpuissance4.h"

PionPuissance4::PionPuissance4(std::shared_ptr<Joueur> joueur)
{
    m_joueur = joueur;
}

std::string PionPuissance4::EnTexte() const
{
    std::string textePion = "   ";
    if (m_joueur != NULL)
    {
        textePion = "(" + m_joueur->EnTexte() + ")";
    }

    return textePion;
}
