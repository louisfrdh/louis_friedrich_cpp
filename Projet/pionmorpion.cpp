#include "pionmorpion.h"

PionMorpion::PionMorpion(std::shared_ptr<Joueur> joueur)
{
    m_joueur = joueur;
}

std::string PionMorpion::EnTexte() const
{
    std::string textePion = " ";

    if (m_joueur != NULL)
    {
        textePion = m_joueur->RetourneId() == 1 ? " X " : " O ";
    }

    return textePion;
}
