#include "afficheurconsoleothello.h"
#include "utils.h"

int AfficheurConsoleOthello::DemandeLigne(std::shared_ptr<Joueur> UnJoueur)
{
    int choix;
    bool choixNonconforme = true;
    while (choixNonconforme)
    {
        choix = AttendUnEntier(UnJoueur->RetourneNom() + ",merci de saisir un numero de ligne");
        if ((choix >= 1) && (choix <= 10))
            choixNonconforme = false;
        else
            InformeUtilisateurDe(">>Etes vous sur de saisir un chiffre entre 1 et 10?");
    }
    choix = choix - 1; // convertit en la valeur correspondante pour le programme
    return choix;
}
int AfficheurConsoleOthello::DemandeColonne(std::shared_ptr<Joueur> UnJoueur)
{
    std::string choix;
    bool choixNonconforme = true;
    while (choixNonconforme)
    {
        choix = AttendUnMot(UnJoueur->RetourneNom() + ", merci de saisir une lettre pour les colonnes ");
        if ((choix.length() == 1) && (utils::intInRange(std::toupper(choix[0]), 'A', 'J')))
        {
            choixNonconforme = false;
        }
        else
        {
            InformeUtilisateurDe(">>Etes vous sur de saisir une lettre entre A et H?");
        }
    }
    return utils::ConversionLettreColonne(choix[0]); // convertit en la valeur correspondante pour le programme
}

void AfficheurConsoleOthello::Affiche(Grille *grille)
{
    NettoyageConsole();
    std::cout << "=== [OTHELLO] ===" << std::endl
              << std::endl;

    for (int i = 'A'; i < 'H'; i++)
    {
        std::cout << "| " << char(i) << " |";
    }

    std::cout << std::endl;
    std::cout << grille->EnTexte() << std::endl;
    std::cout << "----------------------------" << std::endl;
}