#ifndef LIGNE_H
#define LIGNE_H

#include "ensemblelineairecases.h"
#include "utils.h"

#include <vector>
#include <memory>

class Ligne : public EnsembleLineaireCases
{
public:
    std::string EnTexte() const override;
    std::string getLineDescription() const;

private:
};

#endif // LIGNE_H
