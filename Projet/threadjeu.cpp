#include "constantes.h"
#ifdef IHM
#include "threadjeu.h"

ThreadJeu::ThreadJeu(std::shared_ptr<Grille> grille)
{
    m_jeu.AttribuerGrille(grille);
    if ((typeid(grille) == typeid(GrilleMorpion)) || (typeid(grille) == typeid(GrillePuissance4)))
    {
        m_jeu.AttribuerGrille(grille);
    }
}

ThreadJeu::ThreadJeu(int choix)
{
    switch (choix)
    {
    case 1:
        m_jeu.AttribuerGrille(std::make_shared<GrilleMorpion>(GrilleMorpion()));
        break;
    case 2:
        m_jeu.AttribuerGrille(std::make_shared<GrillePuissance4>(GrillePuissance4()));
        break;
    case 3:
        m_jeu.AttribuerGrille(std::make_shared<GrilleDame>(GrilleDame(m_jeu.RetourneJoueur(1), m_jeu.RetourneJoueur(2))));
        break;
    case 4:
        m_jeu.AttribuerGrille(std::make_shared<GrilleOthello>(GrilleOthello(m_jeu.RetourneJoueur(1), m_jeu.RetourneJoueur(2))));
        break;
    }
}

ThreadJeu::ThreadJeu(Importer import)
{
    m_jeu.AttribuerGrilleImporte(import);
}

ThreadJeu::~ThreadJeu()
{
    this->quit();
    this->terminate();
    delete this;
}

void ThreadJeu::run()
{
    m_jeu.Jouer();
}

int ThreadJeu::FournitInformationJoueur(const int abcisse, const int ordonne)
{
    return m_jeu.RetourneJoueurDansCase(abcisse, ordonne);
}
std::shared_ptr<Pion> ThreadJeu::FournitPion(const int abcisse,const int ordonne)
{
    return m_jeu.RetournePionDansCase(abcisse,ordonne);
}
bool ThreadJeu::PartieEstFinie()
{
    return m_jeu.testFin();
}
#endif //IHM
