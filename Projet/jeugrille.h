#ifndef JEUGRILLE_H
#define JEUGRILLE_H

#include <vector>
#include "jeu.h"
#include "afficheurmorpion.h"
#include "afficheurdame.h"
#include "grillepuissance4.h"
#include "grilleothello.h"

class JeuGrille : Jeu
{
public:
    JeuGrille(int nbJoueurs);

    void AttribuerGrille(std::shared_ptr<Grille> grille);
    int RetourneJoueurDansCase(int abcisse, int ordonne);
    std::shared_ptr<Joueur> RetourneJoueur(int id) const;

    virtual void Jouer() override;

    void AttribuerGrilleImporte(Importer import);
    bool testFin() const override;
    std::string getSaveString() const;
    std::shared_ptr<Pion> RetournePionDansCase(int abcisse,int ordonne);
private:
    std::shared_ptr<Grille> m_grille;
    std::shared_ptr<Joueur> m_joueurCourant;
    std::vector<std::shared_ptr<Joueur>> m_joueurs;

    void tourJoueur(std::shared_ptr<Joueur> joueur);
    std::shared_ptr<Joueur> prochainJoueur(std::shared_ptr<Joueur> joueur);

};

#endif // JEUGRILLE_H
