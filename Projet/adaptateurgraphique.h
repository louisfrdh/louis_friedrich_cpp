#ifndef ADAPTATEURGRAPHIQUE_H
#define ADAPTATEURGRAPHIQUE_H

#include <iostream>

class AdaptateurGraphique
{
public:
    static void AttendInteraction();
    static void EnregistreAbscisse(int x);
    static void EnregistreOrdonne(int y);

    static int FournitAbscisse();
    static int FournitOrdonne();
    
    static int m_abcisse;
    static int m_ordonne;

    static bool m_abcisseFournit;
    static bool m_ordonneFournit;
};

#endif // ADAPTATEURGRAPHIQUE_H
