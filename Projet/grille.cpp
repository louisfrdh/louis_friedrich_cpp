#include "utils.h"
#include "grille.h"
#include <iostream>

Grille::Grille(int nbLignes, int nbColonnes, int nbJoueurs, std::string gameName) : m_nbLignes(nbLignes), m_nbColonnes(nbColonnes), m_nbJoueurs(nbJoueurs), m_name(gameName)
{
}

bool Grille::CaseEstVide(int x, int y) const
{
    return m_lignes[x].CaseEstVide(y);
}

int Grille::JoueurDansLaCase(int x, int y) const
{
    return m_lignes[y].RetourneIdJoueur(x);
}
std::shared_ptr<Pion> Grille::PionDansLaCase(int x, int y) const
{
     return m_lignes[y].RetournePionPtr(x);
}
bool Grille::EstPleine() const
{
    for (auto c : m_cases)
    {
        if (c->EstVide())
        {
            return false;
        }
    }
    return true;
}

void Grille::initialiseVectors()
{
    m_lignes.assign(m_nbLignes, Ligne());
    m_colonnes.assign(m_nbColonnes, Colonne());
    m_diagonales.assign((m_nbLignes + m_nbColonnes - 1) * 2, Diagonale());
}

void Grille::initialiseCases()
{
    int iLignePourDiagonalesSensInverse = m_nbLignes - 1 + m_nbLignes - 1 + m_nbColonnes;
    for (int i = 0; i < m_nbLignes; i++)
    {
        for (int j = 0; j < m_nbColonnes; j++)
        {
            std::shared_ptr<CaseMonoPion> sptrCase = std::make_shared<CaseMonoPion>(CaseMonoPion());
            m_cases.push_back(sptrCase);
            m_lignes[i].AjouterCase(sptrCase);
            m_colonnes[j].AjouterCase(sptrCase);
            m_diagonales[i + j].AjouterCase(sptrCase);
            m_diagonales[iLignePourDiagonalesSensInverse + j].AjouterCase(sptrCase);
        }
        iLignePourDiagonalesSensInverse--;
    }
}

int Grille::RetourneNbJoueurs() const
{
    return m_nbJoueurs;
}

int Grille::RetourneNbLignes() const
{
    return m_nbLignes;
}

int Grille::RetourneNbColonnes() const
{
    return m_nbColonnes;
}

int Grille::demanderIndiceCase(const std::shared_ptr<Joueur> joueur, const std::string ligneOuColonne)
{
    int indice;
    int maxColonne = RetourneNbColonnes() - 1;
    std::string nomJoueur = joueur->RetourneNom();

    std::cout << nomJoueur << " renseignez la " << ligneOuColonne << " dans laquelle vous souhaitez jouer (entre 0 et " << maxColonne
              << ") >>>" << std::endl;
    std::cin >> indice;
    return indice;
}

std::string Grille::getSaveString() const
{
    std::string nombreJoueur = std::to_string(RetourneNbJoueurs());

    return getGameName() + "," + nombreJoueur + "," + getGridDescription();
}

std::string Grille::getGridDescription() const
{
    std::string description = "";

    for (auto ligneCourante : m_lignes)
    {
        description += ligneCourante.getLineDescription() + ",";
    }
    utils::removeLastChar(description);
    return description;
}

std::string Grille::getGameName() const
{
    return m_name;
}
