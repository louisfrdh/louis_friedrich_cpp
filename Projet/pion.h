#ifndef PION_H
#define PION_H

#include "joueur.h"
#include <memory>

class Pion
{ // Class abstraite
public:
    inline virtual int RetourneIdJoueur() const {return m_joueur->RetourneId();};
    inline virtual int RetourneValeur() const {return m_joueur->RetourneId();};
    virtual std::shared_ptr<Joueur> RetourneJoueur() const {return m_joueur;};
    virtual std::string EnTexte() const = 0;

protected:
    std::shared_ptr<Joueur> m_joueur;
};
#endif // PION_H
