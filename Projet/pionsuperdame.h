#ifndef PIONSUPERDAME_H
#define PIONSUPERDAME_H

#include "pion.h"

class PionSuperDame : public Pion
{
public:
    PionSuperDame(std::shared_ptr<Joueur> joueur);
    std::string EnTexte() const override;
    virtual int RetourneValeur() const override;
};

#endif // PIONSUPERDAME_H
