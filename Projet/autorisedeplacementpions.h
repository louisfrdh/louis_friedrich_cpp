#ifndef AUTORISEDEPLACEMENTPIONS_H
#define AUTORISEDEPLACEMENTPIONS_H

#include "case.h"
#include <vector>
#include <memory>

class AutoriseDeplacementPions
{
protected:
    virtual void demandeCaseOrigine() = 0;
    virtual void demandeCaseCible() = 0;
    virtual void deplacePion() = 0;
};

#endif // AUTORISEDEPLACEMENTPIONS_H
