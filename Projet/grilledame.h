#ifndef GRILLEDAME_H
#define GRILLEDAME_H

#include "grille.h"
#include "piondame.h"
#include "pionsuperdame.h"
#include "casemonopion.h"
#include "autorisedeplacementpions.h"
#include "afficheurdame.h"
#include <algorithm>
#include <map>

class GrilleDame : public Grille, public AutoriseDeplacementPions
{
public:
    const static int NB_COLONNES = 10;
    const static int NB_LIGNES = 10;
    const static int NB_JOUEURS = 2;

    GrilleDame(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2);
    GrilleDame(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import);

    void TourJoueur(std::shared_ptr<Joueur> joueur) override;
    bool TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const override;
    std::string EnTexte() const override;

protected:
    void remplirListePossible(std::shared_ptr<Diagonale> diagonale, const int indexMin, const int indexMax);

private:
    /// case contenant le pion à bouger et case de destination du pion
    std::shared_ptr<Case> m_caseOrigine;
    std::shared_ptr<Case> m_caseCible;
    /// joueur pour le tour en cours
    std::shared_ptr<Joueur> m_joueur;
    /// listes contenant les cases candidates à chaque phase de jeu
    std::map<std::shared_ptr<Case>, std::shared_ptr<Case>> m_mapCasesCibles;
    std::vector<std::shared_ptr<Case>> m_casesSelectionnables;
    void demandeCaseOrigine() override;
    void demandeCaseCible() override;
    void deplacePion() override;
    bool doitDevenirDame() const;

    void remplitCasesSelectionnables();
    bool caseContientPionPouvantManger(std::shared_ptr<Case> c) const;
    bool caseContientPionPouvantAvancer(std::shared_ptr<Case> c) const;
    bool caseContientPionPouvantMangerDansDiagonale(std::shared_ptr<Diagonale> d, int iDiago) const;
    bool caseContientPionPouvantAvancerDansDiago(std::shared_ptr<Diagonale> d, int iDiago) const;

    void remplitMapCasesCibles();
    void remplitCasesMangeablesDansDiago(std::shared_ptr<Diagonale> d, int iDiago);
    void remplitCasesAtteignablesDansDiago(std::shared_ptr<Diagonale> d, int iDiago);

    bool testPresenceCaseDansVector(int x, int y,
                                    std::vector<std::shared_ptr<Case>> cases) const;
    bool testPresenceCaseDansMap(int x, int y,
                                 std::map<std::shared_ptr<Case>, std::shared_ptr<Case>> cases) const;
    void initialiseGrille();
    void initialiseCases(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2);
    void initialiseCasesImport(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2, Importer import) override;

    bool caseContientUnPionDuJoueur(std::shared_ptr<Case> aCase) const;
    bool caseContientUneDameDuJoueur(std::shared_ptr<Case> aCase) const;
};

#endif // GRILLEDAME_H
