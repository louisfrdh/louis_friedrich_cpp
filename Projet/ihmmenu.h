#ifndef IHMMENU_H
#define IHMMENU_H

#include <memory>
#include <QMainWindow>
#include <QMenu>
#include <QFileDialog>
#include <QtDebug>

#include "ihmmorpion.h"
#include "ihmpuissance4.h"
#include "ihmothello.h"
#include "ihmdame.h"
#include "importer.h"

//#include
namespace Ui
{
    class IHMMenu;
}
// Designed as SDI (Single Document Interface)// VUE
class IHMMenu : public QMainWindow
{
    Q_OBJECT

public:
    explicit IHMMenu(QWidget *parent = nullptr);
    ~IHMMenu();
public slots:
    void ActiveTailleMinimum();
    void ActiveTailleMoyenne();
    void ActiveTailleMaximum();
    void ActiveOuvrir();
    void ActiveSauvegarderSous();
    void ActiveJeuDeMorpion();
    void ActiveJeuDePuissance4();
    void ActiveJeuDeOthello();
    void ActiveJeuDeDame();
protected :
    const int HAUTEUR_MINIMUM =450;
    const int HAUTEUR_MOYENNE =500;
    const int HAUTEUR_MAXIMUM = 1000;
private :
    QString m_emplacementFichierSauvegarde; // D:/path/to/the/file.txt

    Ui::IHMMenu *ui;
    QLabel* afficheurInformationJeu = new QLabel(this);
    QWidget* afficheurCentrale = new QWidget;
    IHMJeuGrille* vueJeuGrille ;
    QVBoxLayout *miseEnPageCentrale = new QVBoxLayout;
    QMenu* menuFichier;
    QMenu* menuChoixJeu;
    QMenu* menuAffichage;
    QMenu* menuChangerJeu;
    QMenu* menuJeuDeGrille;
    QAction* actionQuitter = new QAction("Quitter", this) ;
    QAction* actionOuvrir = new QAction("Ouvrir...",this);
    QAction* actionSauvegarderSous = new QAction("Sauvegarder sous...",this);
    QAction* actionSauvegarder = new QAction("Sauvegarder",this);
    QAction* actionTailleMinimumFenetre = new QAction("Taille Minimum",this);
    QAction* actionTailleMoyenneFenetre = new QAction("Taille Moyenne",this);
    QAction* actionTailleGrandFenetre = new QAction("Taille Grande",this);
    QAction* commenceJeuDeMorpion = new QAction("Jeu de Morpion",this);
    QAction* commenceJeuDePuissance4 = new QAction("Jeu de Puissance 4",this);
    QAction* commenceJeuOthello = new QAction("Jeu de Othello",this);
    QAction* commenceJeuDeDame = new QAction("Jeu de Dame",this);
};

#endif // IHMMENU_H
