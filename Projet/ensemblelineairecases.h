#ifndef ENSEMBLELINEAIRECASES_H
#define ENSEMBLELINEAIRECASES_H

#include "case.h"
#include "utils.h"

#include <vector>
#include <memory>

class EnsembleLineaireCases
{
public:
    EnsembleLineaireCases();

    void AjouterCase(std::shared_ptr<Case> conteneurDePion);
    bool CaseEstVide(int index) const;
    bool EstComplete(std::shared_ptr<Joueur> joueur) const;
    bool EstComplete(std::shared_ptr<Joueur> joueur, int longueurRequise) const;

    int RetourneLongueur() const;
    int RetourneIndiceCase(int x, int y) const;
    int RetourneIdJoueur(int indice) const;

    std::shared_ptr<Joueur> RetourneJoueur(int indice) const;
    std::shared_ptr<Case> RetourneCasePtr(int indice) const;
    std::shared_ptr<Pion> RetournePionPtr(int indice) const;

    virtual void PlacePion(int i, std::shared_ptr<Pion> pion);
    virtual std::string EnTexte() const = 0;

protected:
    std::vector<std::shared_ptr<Case>> m_cases;
};

#endif // ENSEMBLELINEAIRECASES_H
