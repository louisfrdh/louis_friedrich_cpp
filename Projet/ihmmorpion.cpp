#include "constantes.h"

#ifdef IHM

#include "grillemorpion.h"
#include "ihmmorpion.h"

IHMMorpion::IHMMorpion(QWidget *parent) : IHMJeuGrille(parent, GrilleMorpion::NB_COLONNES, GrilleMorpion::NB_LIGNES)
{
    InitialiseDesCases();
    IHMJeuGrille::m_modelJeuGrille = new ThreadJeu(std::make_shared<GrilleMorpion>(GrilleMorpion()));
    IHMJeuGrille::m_modelJeuGrille->start(QThread::HighPriority);
    RafraichirToutesLesCases();
}

IHMMorpion::IHMMorpion(QWidget *parent, Importer import) : IHMJeuGrille(parent, import, GrilleMorpion::NB_COLONNES, GrilleMorpion::NB_LIGNES)
{
    InitialiseDesCases();
    m_modelJeuGrille = new ThreadJeu(import);
    m_modelJeuGrille->start(QThread::HighPriority);
    QTimer m_microPause;
    m_microPause.setSingleShot(true);
    m_microPause.start(150);

    RafraichirToutesLesCases();
}

IHMMorpion::~IHMMorpion()
{
}

void IHMMorpion::InitialiseDesCases()
{
    int abcisse, ordonne;
    for (int i = 0; i < IHMJeuGrille::m_caseLignes * IHMJeuGrille::m_casesColonnes; i++)
    {
        abcisse = i % IHMJeuGrille::m_casesColonnes;
        ordonne = static_cast<int>(i / IHMJeuGrille::m_casesColonnes);
        IHMCaseMonoPion *nouvelleCase = DefinitionCase(abcisse, ordonne);
        MiseEnFormeCase(nouvelleCase);
        IHMJeuGrille::m_affichageGrille->addWidget(nouvelleCase, ordonne, abcisse, Qt::AlignCenter);
    }
    m_affichageGrille->setSpacing(1);
    this->setLayout(IHMJeuGrille::m_affichageGrille);
}

void IHMMorpion::MiseEnFormeCase(IHMCaseMonoPion *nouvelleCase)
{
    nouvelleCase->setFlat(true);
    nouvelleCase->setIconSize(QSize(125, 125));
    nouvelleCase->setFixedSize(125, 125);
    QPalette pal = nouvelleCase->palette();
    pal.setColor(QPalette::Button, QColor(Qt::white));
    nouvelleCase->setAutoFillBackground(true);
    nouvelleCase->setPalette(pal);
}

void IHMMorpion::AssocieImagePion(std::shared_ptr<Pion> pion, IHMCaseMonoPion *caseCible)
{
    if(pion == nullptr)return;
    else if (pion->RetourneIdJoueur() == 1)
    {
        caseCible->setIcon(QIcon(":/Graphics/Pion/Morpion/croix.png"));
    }
    else if (pion->RetourneIdJoueur() == 2)
    {
        caseCible->setIcon(QIcon(":/Graphics/Pion/Morpion/rond.png"));
    }
}

IHMCaseMonoPion *IHMMorpion::DefinitionCase(int abcisse, int ordonne)
{
    IHMCaseMonoPion *nouvelleCase = new IHMCaseMonoPion(this, abcisse, ordonne);
    IHMJeuGrille::m_ensembleDeCase.push_back(nouvelleCase);
    QObject::connect(nouvelleCase, SIGNAL(pressed()), this, SLOT(EnvoiCordonne()));
    QObject::connect(nouvelleCase, SIGNAL(clicked()), this, SLOT(RafraichirToutesLesCases()));
    QObject::connect(nouvelleCase,SIGNAL(clicked()),this,SLOT(NotificationDeVictoire()));
    return nouvelleCase;
}
#endif //IHM
