#ifndef THREADJEU_H
#define THREADJEU_H

#include <QThread>

#include "jeugrille.h"

class ThreadJeu : public QThread
{
public:
    explicit ThreadJeu();
    ThreadJeu(std::shared_ptr<Grille> grille);
    ThreadJeu(int choix);
    ThreadJeu(Importer import);
    ~ThreadJeu();
    int FournitInformationJoueur(const int abcisse, const int ordonne);
    std::shared_ptr<Pion> FournitPion(const int abcisse,const int ordonne);
    bool PartieEstFinie();
protected:
    void run();

private:
    JeuGrille m_jeu = JeuGrille(2);
};

#endif // THREADJEU_H
