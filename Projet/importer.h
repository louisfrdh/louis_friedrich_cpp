#ifndef IMPORTER_H
#define IMPORTER_H

#include <iostream>
#include <vector>

class Importer
{
public:
    Importer(std::string pathToFile);

    static std::vector<std::string> ProchaineLigneEtSeparationEnToken(std::istream &str);

    int Valeur(int i) const;
    int Valeur(int x, int y, int nbColonnes) const;
    int IdJoueurTour() const;

    std::string getType() const;

private:
    std::string type;
    std::vector<int> valeurs;

    int idJoueurTour;
};

#endif // IMPORTER_H
