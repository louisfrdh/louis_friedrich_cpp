#ifndef IHMJEUGRILLE_H
#define IHMJEUGRILLE_H

#include <QWidget>
#include <QGridLayout>
#include <QDebug>
#include <QMessageBox>
#include "threadjeu.h"
#include "ihmcasemonopion.h"

//Template: abstract class a hériter pour crée un nouveau jeu
class IHMJeuGrille : public QWidget
{
    Q_OBJECT
public:
    IHMJeuGrille(QWidget *parent,int nbCaseColonne,int nbCaseLigne);
    IHMJeuGrille(QWidget *parent, Importer import,int nbCaseLongueur,int nbCaseLargueur);
    virtual ~IHMJeuGrille();
public slots:
    void EnvoiCordonne();
    void RafraichirToutesLesCases();
    void NotificationDeVictoire();
protected :
    std::vector<IHMCaseMonoPion*> m_ensembleDeCase;
    QGridLayout *m_affichageGrille = new QGridLayout;
    ThreadJeu* m_modelJeuGrille;
    const int m_casesColonnes;
    const int m_caseLignes;
    const int m_caseTotales;
    virtual IHMCaseMonoPion* DefinitionCase(int abcisse,int ordonne)=0; //Instanciation de case + attribution slot
    virtual void MiseEnFormeCase(IHMCaseMonoPion* nouvelleCase)=0; // Definition de l'aspect d'une case
    virtual void AssocieImagePion(std::shared_ptr<Pion> pion,IHMCaseMonoPion* caseCible)=0;// attribue une image de pion
    virtual void InitialiseDesCases()=0; // pour l'ensembleDeCase : DefinitionCase + MiseEnFormeCase
};

#endif // IHMJEUGRILLE_H
