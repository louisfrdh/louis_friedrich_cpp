#include "constantes.h"
#ifdef IHM
#include "ihmjeugrille.h"

IHMJeuGrille::IHMJeuGrille(QWidget *parent, int nbCaseColonne, int nbCaseLigne) : QWidget(parent), m_casesColonnes(nbCaseColonne), m_caseLignes(nbCaseLigne), m_caseTotales(nbCaseColonne * nbCaseLigne)
{
}

IHMJeuGrille::IHMJeuGrille(QWidget *parent, Importer import, int nbCaseLongueur, int nbCaseLargueur) : QWidget(parent), m_casesColonnes(nbCaseLongueur), m_caseLignes(nbCaseLargueur), m_caseTotales(nbCaseLongueur * nbCaseLargueur)
{
}

IHMJeuGrille::~IHMJeuGrille()
{
    while ((m_affichageGrille->takeAt(0) != 0))
    {
        delete m_affichageGrille->takeAt(0);
    }
    delete m_affichageGrille;
    m_modelJeuGrille->quit();
    if (!m_modelJeuGrille->wait(3000)) //Wait until it actually has terminated (max. 3 sec)
    {
        m_modelJeuGrille->terminate(); //Thread didn't exit in time, probably deadlocked, terminate it!
        m_modelJeuGrille->wait();      //We have to wait again here!
    }
}

void IHMJeuGrille::EnvoiCordonne()
{
    if(!m_modelJeuGrille->PartieEstFinie())
    {
        IHMCaseMonoPion *caseChoisie = qobject_cast<IHMCaseMonoPion *>(sender());
        AdaptateurGraphique::EnregistreAbscisse(caseChoisie->RenvoiAbcisse());
        AdaptateurGraphique::EnregistreOrdonne(caseChoisie->RenvoiOrdonne());
    }
}

void IHMJeuGrille::RafraichirToutesLesCases()
{
    for (auto actualiseCase : m_ensembleDeCase)
    {
        int abcisse = actualiseCase->RenvoiAbcisse();
        int ordonne = actualiseCase->RenvoiOrdonne();
        std::shared_ptr<Pion> pion = m_modelJeuGrille->FournitPion(abcisse, ordonne);
        AssocieImagePion(pion, actualiseCase);
    }
    m_affichageGrille->update();
    this->update();
}

void IHMJeuGrille::NotificationDeVictoire()
{
    if(m_modelJeuGrille->PartieEstFinie())
    {
         QMessageBox::information(this, "Notification", "La partie est <strong>finie !</strong>");
    }

}
#endif //IHM
