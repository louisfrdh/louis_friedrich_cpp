#ifndef PIONDAME_H
#define PIONDAME_H

#include "pion.h"

class PionDame : public Pion
{
public:
    PionDame(std::shared_ptr<Joueur> joueur);

    std::string EnTexte() const override;

    bool EstUneDame = false;
    virtual int RetourneValeur() const override;
};

#endif // PIONDAME_H
