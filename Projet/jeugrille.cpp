#include "jeugrille.h"
#include "fileManager.h"

JeuGrille::JeuGrille(int nbJoueurs)
{
    for (int i = 1; i <= nbJoueurs; i++)
    {
        std::string nom;
        //TO DO:  LET USER CHOOSE NAME;
        //std::cout << "Renseignez le nom du joueur "<< i <<
        //             " >>>" << std::endl;
        // std::cin >> nom;
        m_joueurs.push_back(std::make_shared<Joueur>(Joueur(i, "FAUXNOM")));
    }
    m_joueurCourant = m_joueurs[0];
    //std::cout << "DEBUT DE LA PARTIE" << std::endl << std::endl;
}

void JeuGrille::Jouer()
{
    //dstd::cout << "Debut du jeu !" << std::endl;

    if (!Joueur::joueurEstLePremier(m_joueurCourant))
    {
        for(int i = m_joueurCourant->RetourneId() - 1; i < (int)m_joueurs.size(); i++)
        {
            auto joueurCourant = m_joueurs[i];
            tourJoueur(joueurCourant);
            if (testFin())
            {
                return;
            }
        }
    }
    
    while (!m_grille->EstPleine())
    {
        for (auto joueurCourant : m_joueurs)
        {
            tourJoueur(joueurCourant);
            if (testFin())
            {
                return;
            }
        }
    }
}

void JeuGrille::AttribuerGrilleImporte(Importer import)
{
    std::string type = import.getType();

    if (type == "Morpion")
    {
        this->AttribuerGrille(std::make_shared<GrilleMorpion>(GrilleMorpion(this->RetourneJoueur(1), this->RetourneJoueur(2), import)));
    }
    else if (type == "Puissance4")
    {
        this->AttribuerGrille(std::make_shared<GrillePuissance4>(GrillePuissance4(this->RetourneJoueur(1), this->RetourneJoueur(2), import)));
    }
    else if (type == "Dames")
    {
        this->AttribuerGrille(std::make_shared<GrilleDame>(GrilleDame(this->RetourneJoueur(1), this->RetourneJoueur(2), import)));
    }
    else if(type == "Othello")
    {
         this->AttribuerGrille(std::make_shared<GrilleOthello>(GrilleOthello(this->RetourneJoueur(1), this->RetourneJoueur(2), import)));
    }
    else
    {
        std::cout << "Le type de jeu du fichier n'est pas reconnu" << std::endl;
    }

    m_joueurCourant = RetourneJoueur(import.IdJoueurTour());
}

void JeuGrille::tourJoueur(std::shared_ptr<Joueur> joueur)
{
    m_joueurCourant = joueur;

    m_grille->TourJoueur(joueur);

    m_joueurCourant = prochainJoueur(joueur);
    std::string saveString = getSaveString();
    std::string path = "text.grid.txt";

    fileManager::saveGame(saveString, path);
}

std::shared_ptr<Joueur> JeuGrille::prochainJoueur(std::shared_ptr<Joueur> joueur)
{
    int idCourant = joueur->RetourneId();
    int nextIndex;
    if(idCourant == (int)m_joueurs.size())
    {
        nextIndex = 0;
    }
    else
    {
        nextIndex = idCourant;
    }
    return m_joueurs[nextIndex];
}

bool JeuGrille::testFin() const
{
    for (auto joueurCourant : m_joueurs)
    {
        if (m_grille->TestVictoireJoueur(joueurCourant))
        {
            std::string nomJoueur = joueurCourant->RetourneNom();

            std::cout << std::endl //
                      << nomJoueur << " remporte la partie !" << std::endl;
            return true;
        }
    }

    if (m_grille->EstPleine())
    {
        std::cout << std::endl //
                  << "C'est un match nul !" << std::endl;
        return true;
    }
    return false;
}

std::string JeuGrille::getSaveString() const
{
    std::string saveString = "";

    if (m_joueurCourant != NULL && m_grille != NULL)
    {
        saveString = m_grille->getSaveString() + "," + m_joueurCourant->getSaveString();
    }

    return saveString;
}

void JeuGrille::AttribuerGrille(std::shared_ptr<Grille> grille)
{
    m_grille = grille;
};

int JeuGrille::RetourneJoueurDansCase(int abcisse, int ordonne)
{
    return m_grille->JoueurDansLaCase(abcisse, ordonne);
};
std::shared_ptr<Pion> JeuGrille::RetournePionDansCase(int abcisse,int ordonne)
{
    return m_grille->PionDansLaCase(abcisse, ordonne);
}


std::shared_ptr<Joueur> JeuGrille::RetourneJoueur(int id) const
{
    return m_joueurs[id - 1];
};
