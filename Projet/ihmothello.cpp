#include "constantes.h"
#ifdef IHM
#include "ihmothello.h"

IHMOthello::IHMOthello(QWidget *parent) : IHMJeuGrille(parent, GrilleOthello::NB_COLONNES, GrilleOthello::NB_LIGNES)
{
    InitialiseDesCases();
    IHMJeuGrille::m_modelJeuGrille = new ThreadJeu(4);
    IHMJeuGrille::m_modelJeuGrille->start(QThread::HighPriority);
    RafraichirToutesLesCases();
}


IHMOthello::IHMOthello(QWidget *parent, Importer import) : IHMJeuGrille(parent, import,GrilleOthello::NB_COLONNES,GrilleOthello::NB_LIGNES)
{
    InitialiseDesCases();
    m_modelJeuGrille = new ThreadJeu(import);
    m_modelJeuGrille->start(QThread::HighPriority);
    RafraichirToutesLesCases();
}

IHMOthello::~IHMOthello()
{
}

void IHMOthello::InitialiseDesCases()
{
    int abcisse, ordonne;
    for (int i = 0; i < IHMJeuGrille::m_caseLignes * IHMJeuGrille::m_casesColonnes; i++)
    {
        abcisse = i % IHMJeuGrille::m_casesColonnes;
        ordonne = static_cast<int>(i / IHMJeuGrille::m_casesColonnes);
        IHMCaseMonoPion *nouvelleCase = DefinitionCase(abcisse, ordonne);
        MiseEnFormeCase(nouvelleCase);
        IHMJeuGrille::m_affichageGrille->addWidget(nouvelleCase, ordonne, abcisse, Qt::AlignCenter);
    }
    this->setLayout(IHMJeuGrille::m_affichageGrille);
}

void IHMOthello::MiseEnFormeCase(IHMCaseMonoPion *nouvelleCase)
{
    nouvelleCase->setFlat(true);
    nouvelleCase->setIconSize(QSize(70, 70));
    nouvelleCase->setFixedSize(70, 70);
    //nouvelleCase->setStyleSheet("IHMCaseMonoPion {border: 2px solid;border-bottom-width: 0;}");
    QPalette pal = nouvelleCase->palette();
    pal.setColor(QPalette::Button, QColor(0, 125, 55, 100));
    nouvelleCase->setAutoFillBackground(true);
    nouvelleCase->setPalette(pal);
}

void IHMOthello::AssocieImagePion(std::shared_ptr<Pion> pion, IHMCaseMonoPion *caseCible)
{
    if (pion == nullptr) return;
    if (pion->RetourneIdJoueur() == 1)
        caseCible->setIcon(QIcon(":/Graphics/Pion/Othello/JetonOthelloBlanc.png"));
    else if (pion->RetourneIdJoueur() == 2)
        caseCible->setIcon(QIcon(":/Graphics/Pion/Othello/JetonOthelloNoir.png"));
}

IHMCaseMonoPion *IHMOthello::DefinitionCase(int abcisse, int ordonne)
{
    IHMCaseMonoPion *nouvelleCase = new IHMCaseMonoPion(this, abcisse, ordonne);
    IHMJeuGrille::m_ensembleDeCase.push_back(nouvelleCase);
    QObject::connect(nouvelleCase, SIGNAL(pressed()), this, SLOT(EnvoiCordonne()));
    QObject::connect(nouvelleCase, SIGNAL(clicked()), this, SLOT(RafraichirToutesLesCases()));
    QObject::connect(nouvelleCase,SIGNAL(clicked()),this,SLOT(NotificationDeVictoire()));
    return nouvelleCase;
}

#endif //IHM
