#include "afficheurdame.h"

int AfficheurDame::DemandeLigne(const std::shared_ptr<Joueur> UnJoueur)
{
    int choix = -1;
    bool choixConforme = false;
    while (!choixConforme)
    {
        choix = AttendUnEntier(UnJoueur->RetourneNom() + ", merci de saisir un numéro de ligne");
        if (utils::intInRange(choix, 1, 10))
        {
            choixConforme = true;
        }
        else
        {
            InformeUtilisateurDe(">>Etes vous sur de saisir un chiffre entre 1 et 10?");
        }
    }
    choix--; // convertit en la valeur correspondante pour le programme
    return choix;
}

int AfficheurDame::DemandeColonne(const std::shared_ptr<Joueur> UnJoueur)
{
    std::string choix = "";
    bool choixConforme = false;
    while (!choixConforme)
    {
        choix = AttendUnMot(UnJoueur->RetourneNom() + ", merci de saisir une lettre pour les colonnes ");
        choix[0] = toupper(choix[0]);
        if ((choix.length() == 1) && (utils::intInRange(choix[0], 'A', 'J')))
        {
            choixConforme = true;
        }
        else
        {
            InformeUtilisateurDe(">>Etes vous sur de saisir une lettre entre A et J?");
        }
    }
    return utils::ConversionLettreColonne(choix[0]); // convertit en la valeur correspondante pour le programme
}

char AfficheurDame::ConversionColonneLettre(const int numeroColonne)
{
    char lettre = utils::ConversionColonneLettre(numeroColonne, 'J');
    return lettre;
}

void AfficheurDame::Affiche(const Grille *grille)
{
    NettoyageConsole();

    std::cout << ".!!!..!!!..!!!..!!!." << std::endl;

    std::cout << "=== [DAME] ===" << std::endl
              << std::endl;

    for (int i = 'A'; i <= 'J'; i++)
    {
        std::cout << "| " << char(i) << " |";
    }

    std::cout << std::endl
              << grille->EnTexte() << std::endl;
    std::cout << "--------------------------------" << std::endl;
}
