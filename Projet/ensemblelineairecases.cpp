#include "ensemblelineairecases.h"

EnsembleLineaireCases::EnsembleLineaireCases() {}

bool EnsembleLineaireCases::CaseEstVide(int indice) const
{
    int sizeToCompare = m_cases.size() - 1;
    return utils::intInRange(indice, 0, sizeToCompare) && m_cases[indice]->EstVide();
};

void EnsembleLineaireCases::AjouterCase(std::shared_ptr<Case> laCase)
{
    m_cases.push_back(laCase);
}

bool EnsembleLineaireCases::EstComplete(std::shared_ptr<Joueur> joueur) const
{
    for (auto caseCourante : m_cases)
    {
        if (caseCourante->RetourneJoueurPtr() == NULL || caseCourante->RetourneJoueurPtr() != joueur)
        {
            return false;
        }
    }
    return true;
}

bool EnsembleLineaireCases::EstComplete(std::shared_ptr<Joueur> joueur, int longueurRequise) const
{
    int cpt = 0;
    for (auto caseCourante : m_cases)
    {
        if (caseCourante->RetourneJoueurPtr() == joueur)
        {
            cpt++;
        }
        else
        {
            cpt = 0;
        }

        if (cpt >= longueurRequise)
        {
            return true;
        }
    }
    return false;
}

int EnsembleLineaireCases::RetourneIndiceCase(int x, int y) const
{
    int i = 0;
    for (auto caseCourante : m_cases)
    {
        if (caseCourante->RetourneX() == x && caseCourante->RetourneY() == y)
        {
            return i;
        }
        i++;
    }
    return -1;
}

int EnsembleLineaireCases::RetourneIdJoueur(int indice) const
{
    int tailleCible = m_cases.size() - 1;
    bool estDansIntervalle = utils::intInRange(indice, 0, tailleCible);

    return estDansIntervalle ? m_cases[indice]->RetourneIdJoueur() : -1;
}

std::shared_ptr<Joueur> EnsembleLineaireCases::RetourneJoueur(int indice) const
{
    int tailleCible = m_cases.size() - 1;
    bool estDansIntervalle = utils::intInRange(indice, 0, tailleCible);

    return estDansIntervalle ? m_cases[indice]->RetourneJoueurPtr() : nullptr;
}

std::shared_ptr<Case> EnsembleLineaireCases::RetourneCasePtr(int indice) const
{

    int tailleCible = m_cases.size() - 1;
    bool estDansIntervalle = utils::intInRange(indice, 0, tailleCible);

    return estDansIntervalle ? m_cases[indice] : nullptr;
}

std::shared_ptr<Pion> EnsembleLineaireCases::RetournePionPtr(int indice) const
{

    int tailleCible = m_cases.size() - 1;
    bool estDansIntervalle = utils::intInRange(indice, 0, tailleCible);

    return estDansIntervalle ? m_cases[indice]->RetournePionPtr() : nullptr;
}

void EnsembleLineaireCases::PlacePion(int index, std::shared_ptr<Pion> pion)
{
    m_cases[index]->RetirePion();
    m_cases[index]->PlacePion(pion);
}

int EnsembleLineaireCases::RetourneLongueur() const
{
    return m_cases.size();
};
