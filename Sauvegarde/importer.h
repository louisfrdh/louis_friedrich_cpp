#ifndef IMPORTER_H
#define IMPORTER_H

#include <iostream>
#include <vector>

class Importer
{
public:
    Importer(std::string pathToFile);
    static std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream& str);
    int getValeur(int i);
    std::string getType();
    int getIdJoueurTour();
private:
    std::string type;
    std::vector<int> valeurs;
    int idJoueurTour;
};

#endif // IMPORTER_H
