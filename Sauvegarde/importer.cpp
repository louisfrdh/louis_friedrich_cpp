#include "importer.h"
#include <sstream>
#include <fstream>
#include <stdlib.h>

Importer::Importer(std::string pathToFile)
{
    std::filebuf fb;
    if(fb.open(pathToFile, std::ios::in))
    {
        std::istream is(&fb);
        std::istream& isConst = is;
        std::vector<std::string> line = Importer::getNextLineAndSplitIntoTokens(isConst);
        type = line[0];
        idJoueurTour = stoi(line[line.size()-1]);
        for(int i = 2; i<(int)line.size(); i++)
        {
            std::string cell = line[i];
            valeurs.push_back(stoi(cell, nullptr));
        }
    }
    else
    {
        std::cout << "Tentative de lire le fichier échouée" << std::endl;
    }
}

std::vector<std::string> Importer::getNextLineAndSplitIntoTokens(std::istream& str)
{
    std::vector<std::string>   result;
    std::string                line;
    std::getline(str,line);

    std::stringstream          lineStream(line);
    std::string                cell;

    while(std::getline(lineStream,cell, ','))
    {
        result.push_back(cell);
    }
    // This checks for a trailing comma with no data after it.
    if (!lineStream && cell.empty())
    {
        // If there was a trailing comma then add an empty element.
        result.push_back("");
    }
    return result;
}

int Importer::getValeur(int i)
{
    return valeurs[i];
}

int Importer::getIdJoueurTour()
{
    return idJoueurTour;
}

std::string Importer::getType()
{
    return type;
}

