# Sauvegarde:

La sauvegarde de partie se fera à l'aide de fichiers texte, qui respecteront la syntaxe suivante :

 - La fichier consiste en une ligne
 - Chaque valeur est séparée par une virgule
 - La première valeur est une chaine de caractères qui donne le type de jeu sauvegardé (ex : Dames, Morpion,etc)
 - La deuxième valeur donne le nombre de joueurs de la partie
 - Ensuite on a une liste des valeurs des cases de la grille dans l'ordre (de 0 a lignes*colonnes)
 - En dernière position on note l'id du joueur qui devra reprendre

| Jeu | Nombre de joueur | Description de la grille | Joueur courant |
|-----|------------------|--------------------------|----------------|
| Morpion | 2 | 0,0,0,1,2,1,0,1,2 | 1 |

Code associé : `Morpion,2,0,0,0,1,2,1,0,1,2,1`

## Import

Pour l'import du fichier, on passe par un objet de type Import, qui traduira le fichier à lire pour stocker les informations de la partie en attribut. Puis pour charger la partie avec ces attributs on utilise un constructeur de grille qui prend en paramètre une instance de l'objet Import, et celui ce chargera de placer les différentes pièces dans les bonnes cases.

La classe Importer à pour attributs : 
 - le type (en string)
 - un vecteur de nombre (int) pour les valeurs de la grille
 - un int pour l'id du joueur courant
 - elle ne stocke pas le nombre de joueur pour l'instant, mais son ajout sera très simple si l'on décide de le garder

## Export

L'export de la partie s'effectue après chaque tour de jeu, celle-ci se fait via la méthode saveGame du fileManager, celle-ci réalise un export de grille (qui s'effectue à l'aide du polymorphisme) puis la concatène au joueur courant qui est récupéré tour après tour par la logique de jeu.
Le tout est ensuite enregistré dans le fichier cible du `fileManager` à l'aide des flux IO proposés par les librairies standards C++
