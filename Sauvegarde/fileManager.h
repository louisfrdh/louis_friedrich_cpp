#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <fstream>
#include <iostream>
#include <memory>
#include <vector>
#include "importer.h"

class fileManager {
   public:
    fileManager();
    ~fileManager();
    static void saveGame(std::string saveString, std::string path);
   static Importer loadGame(std::string path);
   private:
};
#endif  // FILEMANAGER_H
