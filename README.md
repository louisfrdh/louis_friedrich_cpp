# Game Cpp

Projet : Theo Welsch - Guillaume Keller - Louis Friedrich - Solann Puygrenier

Choix du projet de Départ pour le TP5 disponible à l'adresse :
https://docs.google.com/document/d/1zOTF46CfvRUpNhL6P5N6S4TUz8Xrtc5eyEkYkoC2ZXg/edit#heading=h.gcgb97nxytpc


Répertoire du projet de jeux en C++ :
Morpion, Puissance 4, Dames ...

Les commits doivent avoir la forme suivante :

"[le type de commit] ( [Champ d'action du commit] ) : [le sujet du commit]"
