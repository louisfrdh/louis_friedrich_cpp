#include "pionpuissance4.h"

PionPuissance4::PionPuissance4(std::shared_ptr<Joueur> joueur)
{
    m_joueur = joueur;
}

void PionPuissance4::Afficher() const
{
    if (m_joueur != NULL) std::cout << m_joueur->RetourneId();
    else std::cout << " ";
}

std::string PionPuissance4::EnTexte() const
{
   std::string textePion;
   if (m_joueur != NULL) textePion = "(" + m_joueur->EnTexte() + ")" ;
   else textePion =  "   ";
   return textePion;
}


