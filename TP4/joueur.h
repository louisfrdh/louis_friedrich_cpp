#ifndef JOUEUR_H
#define JOUEUR_H

#include <iostream>
#include <string>
class Joueur
{
public:
    Joueur(int id) : m_id(id){};
    Joueur(int id, std::string nomDuJoueur) : m_nom(nomDuJoueur), m_id(id){};
    inline int RetourneId() const {return m_id;};
    inline std::string RetourneNom() const {return m_nom;};
    bool operator==(const Joueur &joueur){
        return (joueur.m_id == this->m_id);
    };
    std::string m_nom;
    inline std::string EnTexte() const { return std::to_string(m_id);}
    inline std::string getSaveString() const {return EnTexte();};

private:
    int m_id;
};

#endif // JOUEUR_H
