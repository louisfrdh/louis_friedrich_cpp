#include "ligne.h"

void Ligne::Afficher() const
{
    for(int i=0; i<(int)m_cases.size(); i++) std::cout << "--";
    std::cout << std::endl << "|";
    for(auto c : m_cases)
    {
        c->Afficher();
    }
    std::cout << std::endl;
}

std::string Ligne::EnTexte() const{
    std::string ligneEnTexte = "";
    for(auto c : m_cases)
    {
        ligneEnTexte = ligneEnTexte + c->EnTexte();
    }
    return ligneEnTexte;
}

std::string Ligne::getLineDescription(){
    std::string description="";
    for(auto currentBox : m_cases){
        description+=currentBox->getDescription()+",";
    }
    description = description.substr (0,description.size()-1);
    return description;
}
