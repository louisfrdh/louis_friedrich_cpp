#include "casemonopion.h"

CaseMonoPion::CaseMonoPion(int x, int y) : m_x(x), m_y(y) {}

bool CaseMonoPion::EstVide() const
{
    return (m_pion == NULL);
}

void CaseMonoPion::PlacePion(std::shared_ptr<Pion> pion)
{
    if(EstVide()) m_pion = pion;
}

void CaseMonoPion::RetirePion()
{
    if(!EstVide()) m_pion = NULL;
}

int CaseMonoPion::RetourneIdJoueur() const
{
    return !EstVide() ? m_pion->RetourneIdJoueur() : -1;
}

std::shared_ptr<Joueur> CaseMonoPion::RetourneJoueurPtr() const
{
    if(!EstVide()) return m_pion->RetourneJoueur();
    else return NULL;
}


std::shared_ptr<Pion> CaseMonoPion::RetournePionPtr() const
{
    if(!EstVide()) return m_pion;
    else return nullptr;
}

void CaseMonoPion::Afficher() const
{
    if(m_pion != NULL) m_pion->Afficher();
    else std::cout << " ";
    std::cout << "|";
}

std::string CaseMonoPion::EnTexte() const{
    std::string caseEnTexte;
    if(m_pion == NULL){
          caseEnTexte = "   ";
    }
    else caseEnTexte = m_pion->EnTexte();
    return "|" + caseEnTexte +"|" ;
}


std::string CaseMonoPion::getDescription() const{
    if(m_pion != NULL) {
        return std::to_string(m_pion->RetourneIdJoueur());
    }
    return "0";
}
