#ifndef GRILLE_H
#define GRILLE_H

#include "case.h"
#include "casemonopion.h"
#include "ligne.h"
#include "colonne.h"
#include "diagonale.h"
#include "joueur.h"
#include <vector>
#include <memory>


class Grille
{
public:
    Grille(int nbLignes, int nbColonnes, int nbJOueurs, std::string gameName);
    virtual void TourJoueur(std::shared_ptr<Joueur> joueur) =0;
    virtual bool TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const =0;
    inline int RetourneNbJoueurs() const { return m_nbJoueurs; }
    int RetourneNbLignes() const { return m_nbLignes; }
    int RetourneNbColonnes() const { return m_nbColonnes; }
    bool EstPleine() const;
    bool CaseEstVide(int x, int y) const;
    void Afficher() const;
    virtual std::string EnTexte() const = 0;
    std::string getSaveString();
protected:
    std::string getGridDescription();
    std::vector<std::shared_ptr<Case>> m_cases;
    std::vector<Ligne> m_lignes;
    std::vector<Colonne> m_colonnes;
    std::vector<Diagonale> m_diagonales;
    void initialiseVectors();
    void initialiseCases();
    int demanderIndiceCase(std::shared_ptr<Joueur> joueur, std::string ligneOuColonne);
    const int m_nbLignes, m_nbColonnes, m_nbJoueurs;
    std::string getGameName();
    std::string m_name;
};

#endif // GRILLE_H
