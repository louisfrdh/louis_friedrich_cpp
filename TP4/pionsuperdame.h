#ifndef PIONSUPERDAME_H
#define PIONSUPERDAME_H

#include "pion.h"

class PionSuperDame : public Pion
{
public:
    PionSuperDame(std::shared_ptr<Joueur> joueur);
    void Afficher() const override;
    std::string EnTexte() const override;
};

#endif // PIONSUPERDAME_H
