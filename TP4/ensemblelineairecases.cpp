#include "ensemblelineairecases.h"

EnsembleLineaireCases::EnsembleLineaireCases(){}

bool EnsembleLineaireCases::CaseEstVide(int indice) const
{
    if(indice >= 0 && indice < (int)m_cases.size()) return m_cases[indice]->EstVide();
    else return false;
};


void EnsembleLineaireCases::AjouterCase(std::shared_ptr<Case> laCase)
{
    m_cases.push_back(laCase);
}

bool EnsembleLineaireCases::EstComplete(std::shared_ptr<Joueur> joueur) const
{
    for(auto c : m_cases)
    {
        if (c->RetourneJoueurPtr() == NULL) return false;
        else if (c->RetourneJoueurPtr() != joueur) return false;
    }
    return true;
}

bool EnsembleLineaireCases::EstComplete(std::shared_ptr<Joueur> joueur, int longueurRequise) const
{
    int cpt = 0;
    for(auto c : m_cases)
    {
        if(c->RetourneJoueurPtr() == joueur) cpt++;
        else cpt = 0;
        if(cpt >= longueurRequise) return true;
    }
    return false;
}

int EnsembleLineaireCases::RetourneIndiceCase(int x, int y) const
{
    int i = 0;
    for(auto c : m_cases)
    {
        if (c->RetourneX() == x && c->RetourneY() == y) return i;
        i++;
    }
    return -1;
}

int EnsembleLineaireCases::RetourneIdJoueur(int indice) const
{
    if (indice >= 0 && indice < (int)m_cases.size()) return m_cases[indice]->RetourneIdJoueur();
    else return -1;
}

std::shared_ptr<Joueur> EnsembleLineaireCases::RetourneJoueur(int indice) const
{
    if (indice >= 0 && indice < (int)m_cases.size()) return m_cases[indice]->RetourneJoueurPtr();
    else return nullptr;
}

std::shared_ptr<Case> EnsembleLineaireCases::RetourneCasePtr(int indice) const
{
    if (indice >= 0 && indice < (int)m_cases.size()) return m_cases[indice];
    else return nullptr;
}

std::shared_ptr<Pion> EnsembleLineaireCases::RetournePionPtr(int indice) const
{
    if (indice >= 0 && indice < (int)m_cases.size()) return m_cases[indice]->RetournePionPtr();
    else return nullptr;
}

void EnsembleLineaireCases::PlacePion(int index, std::shared_ptr<Pion> pion){
    m_cases[index]->RetirePion();
    m_cases[index]->PlacePion(pion);
}
