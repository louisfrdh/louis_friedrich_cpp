#include "grillemorpion.h"
#include "pionmorpion.h"
#include "afficheurmorpion.h"

GrilleMorpion::GrilleMorpion() : Grille(NB_LIGNES, NB_COLONNES, NB_JOUEURS,"Morpion") {
    initialiseVectors();
    initialiseCases();
}

void GrilleMorpion::DeposerJeton(int x, int y, std::shared_ptr<Joueur> joueur)
{
    if(CaseEstVide(x, y))
        m_lignes[x].PlacePion(y, std::make_shared<PionMorpion>(PionMorpion(joueur)));
}


void GrilleMorpion::TourJoueur(std::shared_ptr<Joueur> joueur)
{
    AfficheurMorpion::Affiche(this);
    int x, y;
    do {
        x = AfficheurMorpion::DemandeCordonnee(joueur, "ligne");
        y = AfficheurMorpion::DemandeCordonnee(joueur, "colonne");
    } while (x<0 || x>=NB_LIGNES|| y<0 || y>=NB_COLONNES  || !CaseEstVide(x,y));
    DeposerJeton(x, y, joueur);
}

bool GrilleMorpion::TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const
{
    for(auto l : m_lignes) if (l.EstComplete(joueur)) return true;
    for(auto c : m_colonnes) if (c.EstComplete(joueur)) return true;
    for(auto d : m_diagonales) if (d.EstComplete(joueur, NB_LIGNES)) return true;
    return false;
}

std::string GrilleMorpion::EnTexte() const{
    std::string grilleEnTexte="---------------\n";
    for(Ligne l : m_lignes)
    {
        grilleEnTexte = grilleEnTexte + l.EnTexte() + "\n";
        grilleEnTexte = grilleEnTexte + "---------------\n";
    }
    return grilleEnTexte;
}
