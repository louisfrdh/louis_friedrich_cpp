#ifndef OUTILSCONSOLE_H
#define OUTILSCONSOLE_H

#include <string>
#include <iostream>
#include "grille.h"


class OutilsConsole{
public:
    static void NettoyageConsole();
    static void InformeUtilisateurDe(std::string);
    static int AttendUnEntier();
    static int AttendUnEntier(std::string messagePourUtilisateur);
    static std::string AttendUnMot(std::string messagePourUtilisateur);
    static std::string AttendUnNomPrenom();
protected :

    static std::string DemandeUnElement();
    static std::string DemandeUnePhrase();



    static bool EstUnEntierUniquement(std::string);
    static bool EstUnMotUniquement(std::string);


};

#endif // OUTILSCONSOLE_H
