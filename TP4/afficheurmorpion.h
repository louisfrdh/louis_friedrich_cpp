#ifndef AFFICHEURMORPION_H
#define AFFICHEURMORPION_H

#include "outilsconsole.h"
#include "joueur.h"
#include "grillemorpion.h"

class AfficheurMorpion :  OutilsConsole {
public:
    static int DemandeCordonnee(std::shared_ptr<Joueur> UnJoueur,std::string messagePourJoueur);
    static void Affiche(Grille* grille);
};

#endif // AFFICHEURMORPION_H
