#include "pionmorpion.h"

PionMorpion::PionMorpion(std::shared_ptr<Joueur> joueur)
{
    m_joueur = joueur;
}

void PionMorpion::Afficher() const
{
    if(m_joueur != NULL)
    {
        std::string affPion = m_joueur->RetourneId() == 1 ? "X" : "0";
        std::cout << affPion;
    }
    else std::cout << " ";
}

std::string PionMorpion::EnTexte() const{
    std::string textePion;
    if(m_joueur != NULL){
        textePion = m_joueur->RetourneId() == 1 ? " X " : " O ";
    }
    else textePion = " ";
    return textePion;
}
