#include "piondame.h"
#include <algorithm>

PionDame::PionDame(std::shared_ptr<Joueur> joueur)
{
    m_joueur = joueur;
}

void PionDame::Afficher() const
{
    if(m_joueur != NULL)
    {
        std::string affPion = m_joueur->RetourneId() == 1 ? "b" : "n";
        if(EstUneDame) std::transform(affPion.begin(), affPion.end(),affPion.begin(), ::toupper);
        std::cout << affPion;
    }
    else std::cout << " ";
}

std::string PionDame::EnTexte() const {
    if(m_joueur != NULL){
        return m_joueur->RetourneId() == 1 ? "(b)" : "(n)";
    }
    else return "   ";
}
