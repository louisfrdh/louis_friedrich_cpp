#include "afficheurmorpion.h"

int AfficheurMorpion::DemandeCordonnee(std::shared_ptr<Joueur> UnJoueur,std::string ligneOuColonne){
    int choix;
    bool choixNonconforme =true;
    while(choixNonconforme){
        choix = AttendUnEntier(UnJoueur->RetourneNom() + ",merci de saisir un numro de "+ ligneOuColonne + "[1,2 ou 3]");
        if ((choix >=1) && (choix <= 3)) choixNonconforme = false;
        else InformeUtilisateurDe(">>Etes vous sur de saisir un chiffre entre 1 et 3?");
    }
    choix = choix - 1; // convertit en la valeur correspondante pour le programme
    return choix;
}

void AfficheurMorpion::Affiche(Grille* grille){
    NettoyageConsole();
    std::cout << "=== [MORPION] ===" << std::endl<< std::endl;
    std::cout << grille->EnTexte() << std::endl;

}
