#ifndef CASEMONOPION_H
#define CASEMONOPION_H

#include <memory>
#include "case.h"

class CaseMonoPion : public Case
{
public:
    CaseMonoPion(){};
    CaseMonoPion(int x, int y);
    CaseMonoPion(std::shared_ptr<Pion> pion):m_pion(pion){};
    bool EstVide() const override;
    void PlacePion(std::shared_ptr<Pion> pion) override;
    void RetirePion() override;
    inline int RetourneX() const override {return m_x; };
    inline int RetourneY() const override {return m_y; };
    int RetourneIdJoueur() const override;
    std::shared_ptr<Joueur> RetourneJoueurPtr() const override;
    std::shared_ptr<Pion> RetournePionPtr() const override;
    void Afficher() const override;
    std::string EnTexte() const override;
    std::string getDescription() const override;
private :
    int m_x, m_y;
    std::shared_ptr<Pion> m_pion;
};

#endif // CASEMONOPION_H


