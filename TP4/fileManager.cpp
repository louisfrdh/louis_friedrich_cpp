#include "fileManager.h"

fileManager::fileManager() {
}

fileManager::~fileManager() {
}

static void fileManager::saveGame(std::string saveString, std::string path) {
    if (path.length() > 0) {
        std::ofstream file;
        file.open(path);
        file << saveString;
        file.close();
    }
}