#include "afficheurpuissance4.h"

int AfficheurPuissance4::DemandeCordonnee(std::shared_ptr<Joueur> UnJoueur,const int size){
    int choix;
    bool choixNonconforme =true;
    while(choixNonconforme){
        choix = AttendUnEntier(UnJoueur->RetourneNom() + ",merci de saisir un numro de colonne");
        if ((choix >0) && (choix <= size)) choixNonconforme = false;
        else InformeUtilisateurDe(">>Etes vous sur de saisir un chiffre dans les bornes ?");
    }
    choix -= 1; // convertit en la valeur correspondante pour le programme
    return choix;
}

void AfficheurPuissance4::Affiche(Grille* grille){
    NettoyageConsole();
    std::cout << "=== [PUISSANCE 4] ===" << std::endl<< std::endl;
    std::cout << grille->EnTexte() << std::endl;
    std::cout << "--------------------------------" << std::endl;
}

