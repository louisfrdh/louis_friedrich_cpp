#ifndef PION_PUISSANCE4_H
#define PION_PUISSANCE4_H

#include "pion.h"

class PionPuissance4 : public Pion {
public:
    PionPuissance4(std::shared_ptr<Joueur> joueur);
    void Afficher() const override;
    std::string EnTexte() const override;
};

#endif // PION_PUISSANCE4_H
