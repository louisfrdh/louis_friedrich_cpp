#include "pionsuperdame.h"

PionSuperDame::PionSuperDame(std::shared_ptr<Joueur> joueur)
{
        m_joueur = joueur;
}

void PionSuperDame::Afficher() const{

}

std::string PionSuperDame::EnTexte() const{
    if(m_joueur != NULL){
        return m_joueur->RetourneId() == 1 ? "(B)" : "(N)";
    }
    else return "   ";
}
