#include "grilledame.h"

GrilleDame::GrilleDame(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2)
    : Grille(NB_LIGNES, NB_COLONNES, NB_JOUEURS,"Dames")
{
    initialiseVectors();
    initialiseCases(j1, j2);
}

void GrilleDame::TourJoueur(std::shared_ptr<Joueur> joueur)
{
    m_joueur = joueur;
    remplitCasesSelectionnables();
    demandeCaseOrigine();
    remplitMapCasesCibles();
    demandeCaseCible();
    deplacePion();
    AfficheurDame::Affiche(this);
}

bool GrilleDame::TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const
{
    for(auto c : m_cases)
    {
        if (c->RetourneIdJoueur() != -1 && c->RetourneIdJoueur() != joueur->RetourneId()) return false;
    }
    return true;
}

void GrilleDame::demandeCaseOrigine()
{
    /// affiche possibilités
    OutilsConsole::InformeUtilisateurDe("Les cases selectionnables :");

    for(auto c : m_casesSelectionnables)
    {
        std::cout << "[" << AfficheurDame::ConversionLettreColonne(c->RetourneY()) << c->RetourneX()+1 << "] ";
    }
    std::cout << std::endl;

    int x, y;
    std::shared_ptr<Case> caseOrigine;
    do {
        y = AfficheurDame::DemandeColonne(m_joueur);
        x = AfficheurDame::DemandeLigne(m_joueur);
        caseOrigine = m_lignes[x].RetourneCasePtr(y);
    } while (!testPresenceCaseDansVector(x, y, m_casesSelectionnables));
    m_caseOrigine = caseOrigine;
}

void GrilleDame::demandeCaseCible()
{
    /// affiche possibilités
    OutilsConsole::InformeUtilisateurDe("Cases atteignables :");
    for(auto c : m_mapCasesCibles)
    {
         std::cout << "[" << AfficheurDame::ConversionLettreColonne(c.first->RetourneY()) << c.first->RetourneX()+1 << "] ";
    }
    std::cout << std::endl;

    int x, y;
    std::shared_ptr<Case> caseCible;
    do {
        y = AfficheurDame::DemandeColonne(m_joueur);
        x = AfficheurDame::DemandeLigne(m_joueur);
        caseCible = m_lignes[x].RetourneCasePtr(y);
    } while (!testPresenceCaseDansMap(x, y, m_mapCasesCibles));
    m_caseCible = caseCible;
    std::cout << caseCible->RetourneX() << m_caseCible->RetourneY() << std::endl;
    std::cout << m_caseCible->RetourneX() << m_caseCible->RetourneY() << std::endl;
}

void GrilleDame::deplacePion()
{
    bool aMange = false;
    std::shared_ptr<Pion> p = m_caseOrigine->RetournePionPtr();
    m_caseOrigine->RetirePion();
    /// si c'est une capture on retire le pion adverse
    if (m_mapCasesCibles[m_caseCible] != nullptr)
    {
        m_mapCasesCibles[m_caseCible]->RetirePion();
        aMange = true;
    }
    m_caseCible->PlacePion(p);
    m_caseOrigine = m_caseCible;
    m_casesSelectionnables.clear();
    m_mapCasesCibles.clear();
    /// on test si le joueur doit manger plusieurs pions d'affilée
    if (aMange && caseContientPionPouvantManger(m_caseOrigine))
    {
        AfficheurDame::Affiche(this);
        std::cout << "Vous devez continuez à manger !" << std::endl;
        remplitMapCasesCibles();
        demandeCaseCible();
        deplacePion();
    }
    else if(doitDevenirDame())
    {
        m_caseOrigine->RetirePion();
        m_caseOrigine->PlacePion(std::make_shared<PionSuperDame>(PionSuperDame(m_joueur)));
    }

}

void GrilleDame::remplitCasesSelectionnables()
{
    /// on cherche toutes les cases contenant un pion pouvant manger un pion adverse
    for(auto c : m_cases) if(caseContientPionPouvantManger(c))  m_casesSelectionnables.push_back(c);
    /// si aucun pion n'a d'obligation de capture, on cherche les pions pouvant avancer
    if(m_casesSelectionnables.empty())
    {
        for(auto c : m_cases) if(caseContientPionPouvantAvancer(c)) m_casesSelectionnables.push_back(c);
    }
}

/// test si la case suivante dans la diagonale contient un pion adverse
/// et si celle encore après est vide pour manger
bool GrilleDame::caseContientPionPouvantManger(std::shared_ptr<Case> c) const
{
    /// récupération des coordonnées de la case
    int x = c->RetourneX();
    int y = c->RetourneY();
    /// les diagonales dans lesquelles se trouve la case
    std::shared_ptr<Diagonale> d1 = std::make_shared<Diagonale>(m_diagonales[x+y]);
    std::shared_ptr<Diagonale> d2 = std::make_shared<Diagonale>(m_diagonales[28-x+y]);
    /// indice de la case dans chacune des diagonales
    int i1 = d1->RetourneIndiceCase(x,y);
    int i2 = d2->RetourneIndiceCase(x,y);
    /// test les possibilités sur les deux diagonales
    if (caseContientPionPouvantMangerDansDiago(d1,i1)) return true;
    if (caseContientPionPouvantMangerDansDiago(d2,i2)) return true;
    return false;
}

bool GrilleDame::caseContientPionPouvantMangerDansDiago(std::shared_ptr<Diagonale> d, int iCase) const
{
    auto caseTestee = d->RetourneCasePtr(iCase);
    if(caseContientUneDameDuJoueur(caseTestee))
    {
        for(int j=2-iCase ; j <= d->RetourneLongueur()-iCase-2 ; j++)
        {
            if( d->RetourneIdJoueur(iCase+j) != -1 )
            {
                if(d->RetourneIdJoueur(iCase+j) != m_joueur->RetourneId()
                        && d->CaseEstVide(iCase+2*j) )
                {
                    return true;
                }
                else break;
            }
        }
    }
    else if (caseContientUnPionDuJoueur(caseTestee))
    {
        for(int j=-1; j<=1; j+=2)
        {
            if( d->RetourneIdJoueur(iCase+j) != -1
                && d->RetourneIdJoueur(iCase+j) != m_joueur->RetourneId()
                && d->CaseEstVide(iCase+2*j) ) return true;
        }
    }
    return false;
}

bool GrilleDame::caseContientPionPouvantAvancer(std::shared_ptr<Case> c) const
{
    /// récupération des coordonnées de la case
    int x = c->RetourneX();
    int y = c->RetourneY();
    /// les diagonales dans lesquelles se trouve la case
    std::shared_ptr<Diagonale> d1 = std::make_shared<Diagonale>(m_diagonales[x+y]);
    std::shared_ptr<Diagonale> d2 = std::make_shared<Diagonale>(m_diagonales[28-x+y]);
    /// indice de la case dans chacune des diagonales
    int i1 = d1->RetourneIndiceCase(x,y);
    int i2 = d2->RetourneIndiceCase(x,y);
    /// test les possibilités sur les deux diagonales
    if (caseContientPionPouvantAvancerDansDiago(d1,i1)) return true;
    if (caseContientPionPouvantAvancerDansDiago(d2,i2)) return true;
    return false;
}

/// test si la case suivante est vide
bool GrilleDame::caseContientPionPouvantAvancerDansDiago(std::shared_ptr<Diagonale> d, int iCase) const
{
    auto caseTestee = d->RetourneCasePtr(iCase);
    if(caseContientUneDameDuJoueur(caseTestee))
    {
        for(int j=0; j<iCase; j++)
        {
            if (d->CaseEstVide(j)) return true;
            if (d->RetourneIdJoueur(j) == m_joueur->RetourneId()) break;
        }
        for(int j=iCase+1; j<d->RetourneLongueur(); j++)
        {
            if (d->CaseEstVide(j)) return true;
            if (d->RetourneIdJoueur(j) == m_joueur->RetourneId()) break;
        }
    }
    else if (caseContientUnPionDuJoueur(caseTestee))
    {
        int j = m_joueur->RetourneId() == 1 ? 1 : -1;
        return d->CaseEstVide(iCase+j);
    }
    return false;
}

void GrilleDame::remplitMapCasesCibles()
{
    /// récupération des coordonnées de caseOrigine
    int x = m_caseOrigine->RetourneX();
    int y = m_caseOrigine->RetourneY();
    /// les diagonales dans lesquelles se trouve caseOrigine
    std::shared_ptr<Diagonale> d1 = std::make_shared<Diagonale>(m_diagonales[x+y]);
    std::shared_ptr<Diagonale> d2 = std::make_shared<Diagonale>(m_diagonales[28-x+y]);
    /// indice de la case dans chacune des diagonales
    int i1 = d1->RetourneIndiceCase(x,y);
    int i2 = d2->RetourneIndiceCase(x,y);

    if(caseContientPionPouvantManger(m_caseOrigine))
    {
        remplitCasesMangeablesDansDiago(d1,i1);
        remplitCasesMangeablesDansDiago(d2,i2);
    }
    else
    {
        remplitCasesAtteignablesDansDiago(d1,i1);
        remplitCasesAtteignablesDansDiago(d2,i2);
    }
}

void GrilleDame::remplitCasesMangeablesDansDiago(std::shared_ptr<Diagonale> d, int iCase)
{
    auto caseTestee = m_caseOrigine;
    if(caseContientUneDameDuJoueur(caseTestee))
    {
        for(int j=2-iCase ; j <= d->RetourneLongueur()-iCase-2 ; j++)
        {
            if( d->RetourneIdJoueur(iCase+j) != -1 )
            {
                if(d->RetourneIdJoueur(iCase+j) != m_joueur->RetourneId()
                        && d->CaseEstVide(iCase+2*j) )
                {
                    m_mapCasesCibles.emplace(d->RetourneCasePtr(iCase+2*j),d->RetourneCasePtr(iCase+j));
                }
                else break;
            }
        }
    }
    else if (caseContientUnPionDuJoueur(caseTestee))
    {
        for(int j=-1; j<=1; j+=2)
        {
            if( d->RetourneIdJoueur(iCase+j) != -1
                && d->RetourneIdJoueur(iCase+j) != m_joueur->RetourneId()
                && d->CaseEstVide(iCase+2*j))
            {
                m_mapCasesCibles.emplace(d->RetourneCasePtr(iCase+2*j),d->RetourneCasePtr(iCase+j));
            }
        }
    }
}

void GrilleDame::remplitCasesAtteignablesDansDiago(std::shared_ptr<Diagonale> d, int iCase)
{
    auto caseTestee = d->RetourneCasePtr(iCase);
    if(caseContientUneDameDuJoueur(caseTestee))
    {
        for(int j=0; j<iCase; j++)
        {
            if (d->CaseEstVide(j)) m_mapCasesCibles.emplace(d->RetourneCasePtr(j), nullptr);
            if (d->RetourneIdJoueur(j) == m_joueur->RetourneId()) break;
        }
        for(int j=iCase+1; j<d->RetourneLongueur(); j++)
        {
            if (d->CaseEstVide(j)) m_mapCasesCibles.emplace(d->RetourneCasePtr(j), nullptr);
            if (d->RetourneIdJoueur(j) == m_joueur->RetourneId()) break;
        }
    }
    else if (caseContientUnPionDuJoueur(caseTestee))
    {
        int j = m_joueur->RetourneId() == 1 ? 1 : -1;
        if (d->CaseEstVide(iCase+j)) m_mapCasesCibles.emplace(d->RetourneCasePtr(iCase+j), nullptr);
    }
}

bool GrilleDame::testPresenceCaseDansVector(int x, int y,
                                         std::vector<std::shared_ptr<Case>> cases) const
{
    for(auto c : cases) if (c->RetourneX() == x && c->RetourneY() == y) return true;
    return false;
}

bool GrilleDame::testPresenceCaseDansMap(int x, int y,
                    std::map<std::shared_ptr<Case>, std::shared_ptr<Case>> cases) const
{
    for(auto c : cases) if (c.first->RetourneX() == x && c.first->RetourneY() == y) return true;
    return false;
}

bool GrilleDame::doitDevenirDame() const
{
    int x = m_caseOrigine->RetourneX();
    int y = m_caseOrigine->RetourneY();
    if(m_joueur->RetourneId() == 1) return m_lignes[9].RetourneIndiceCase(x,y) != -1;
    if(m_joueur->RetourneId() == 2) return m_lignes[0].RetourneIndiceCase(x,y) != -1;
    else return false;
}

void GrilleDame::initialiseGrille()
{
    initialiseVectors();
}

void GrilleDame::initialiseCases(std::shared_ptr<Joueur> j1, std::shared_ptr<Joueur> j2)
{
    int g = m_nbLignes-1 + m_nbLignes-1 + m_nbColonnes;
    int y = 2; /// permet de gérer le placement initial des pions
    for(int i=0; i<m_nbLignes; i++)
    {
        for(int j=0; j<m_nbColonnes; j++)
        {
            std::shared_ptr<CaseMonoPion> sptrCase = std::make_shared<CaseMonoPion>(CaseMonoPion(i, j));
            if(y % 2 == 0)
            {
                std::shared_ptr<PionDame> p;
                if(y <= 44) p = std::make_shared<PionDame>(PionDame(j1));
                else if(y >= 68) p = std::make_shared<PionDame>(PionDame(j2));
                if(p != nullptr) sptrCase->PlacePion(p);
            }
            m_cases.push_back(sptrCase);
            m_lignes[i].AjouterCase(sptrCase);
            m_colonnes[j].AjouterCase(sptrCase);
            m_diagonales[i+j].AjouterCase(sptrCase);
            m_diagonales[g+j].AjouterCase(sptrCase);
            y++;
        }
        g--;
        y++;
    }
}

std::string GrilleDame::EnTexte() const {
    int i = 1;
    std::string grilleEnTexte="--------------------------------------------------\n";
    for(Ligne l : m_lignes)
    {
        grilleEnTexte = grilleEnTexte + l.EnTexte() + "\t["+ std::to_string(i)+ "]\n";
        i++;
        grilleEnTexte = grilleEnTexte + "--------------------------------------------------\n";
    }
    return grilleEnTexte;
}

bool GrilleDame::caseContientUnPionDuJoueur(std::shared_ptr<Case> laCase) const {

    return ((laCase->RetourneIdJoueur() == m_joueur->RetourneId())
            && (std::dynamic_pointer_cast<PionDame>(laCase->RetournePionPtr()))) ;
}

bool GrilleDame::caseContientUneDameDuJoueur(std::shared_ptr<Case> laCase)const {
    return (laCase->RetourneIdJoueur() == m_joueur->RetourneId())
            && (std::dynamic_pointer_cast<PionSuperDame>(laCase->RetournePionPtr())) ;
}
