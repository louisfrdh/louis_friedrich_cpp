#include "outilsconsole.h"


void OutilsConsole::InformeUtilisateurDe(std::string unMessage){
    std::cout << unMessage << std::endl;
}

std::string OutilsConsole::DemandeUnElement(){
    std::string reponseUtilisateur;
    std::cout << "->" ;
    std::cin >> reponseUtilisateur;
    return reponseUtilisateur;
}

std::string OutilsConsole::DemandeUnePhrase(){
    std::string reponseUtilisateur;
    std::cout << "->" ;
    std::getline(std::cin,reponseUtilisateur);
    return reponseUtilisateur;
}

std::string OutilsConsole::AttendUnNomPrenom(){
    bool reponseNonConforme = true;
    std::string reponseUtilisateur;
    while(reponseNonConforme){
        InformeUtilisateurDe("Merci de saisir votre pseudo");
        reponseUtilisateur = DemandeUnElement();
        if(EstUnMotUniquement(reponseUtilisateur)) reponseNonConforme = false;
    }
    return reponseUtilisateur;
}
int OutilsConsole::AttendUnEntier(){
    bool reponseNonConforme = true;
    std::string reponseUtilisateur;
    while(reponseNonConforme){
       InformeUtilisateurDe("Merci de saisir un entier naturel");
       reponseUtilisateur = DemandeUnElement();
       if(EstUnEntierUniquement(reponseUtilisateur)) reponseNonConforme = false;
       else InformeUtilisateurDe(">>Etes vous sur de saisir un nombre ?");
    }
    return stoi(reponseUtilisateur);
}
int OutilsConsole::AttendUnEntier(std::string messagePourUtilisateur){
    bool reponseNonConforme = true;
    std::string reponseUtilisateur;
    while(reponseNonConforme){
       InformeUtilisateurDe(messagePourUtilisateur);
       reponseUtilisateur = DemandeUnElement();
       if(EstUnEntierUniquement(reponseUtilisateur)) reponseNonConforme = false;
       else InformeUtilisateurDe(">>Etes vous sur de saisir un nombre ?");
    }
    return stoi(reponseUtilisateur);
}
std::string OutilsConsole::AttendUnMot(std::string messagePourUtilisateur){
    bool reponseNonConforme = true;
    std::string reponseUtilisateur;
    while(reponseNonConforme){
       InformeUtilisateurDe(messagePourUtilisateur);
       reponseUtilisateur = DemandeUnElement();
       if(EstUnMotUniquement(reponseUtilisateur)) reponseNonConforme = false;
       else InformeUtilisateurDe(">>Etes vous sur de saisir une lettre ?");
    }
    return reponseUtilisateur;
}

bool OutilsConsole::EstUnEntierUniquement(std::string chaine){
    int longueur = chaine.length();
    for(int i =0 ; i<longueur;i++){
        if(!isdigit(chaine[i])){
           return false;
        }
    }
    return true; // Nombre uniquement
}

bool OutilsConsole::EstUnMotUniquement(std::string chaine){
    int longueur = chaine.length();
    for(int i =0 ; i<longueur;i++){
        if(isdigit(chaine[i])||ispunct(chaine[i])||isblank(chaine[i])||isspace(chaine[i])){
            return false;
        }
    }
    return true;
}

void OutilsConsole::NettoyageConsole(){
  std::system("cls");
}
