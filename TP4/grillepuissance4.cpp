#include "grillepuissance4.h"
#include "pionpuissance4.h"
#include "afficheurpuissance4.h"

GrillePuissance4::GrillePuissance4() : Grille(NB_LIGNES, NB_COLONNES, NB_JOUEURS,"Puissance4") {
    initialiseVectors();
    initialiseCases();
}

void GrillePuissance4::DeposerJeton(int i, std::shared_ptr<Joueur> joueur)
{
    for(int j=m_lignes.size()-1; j>=0 ;j--)
    {
        if(m_colonnes[i].CaseEstVide(j))
        {
            m_colonnes[i].PlacePion(j, std::make_shared<PionPuissance4>(PionPuissance4(joueur)));
            return;
        }
    }
}

void GrillePuissance4::TourJoueur(std::shared_ptr<Joueur> joueur)
{
    int i;
    AfficheurPuissance4::Affiche(this);
    i = AfficheurPuissance4::DemandeCordonnee(joueur,7);
    DeposerJeton(i, joueur);
}


bool GrillePuissance4::TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const
{
    for(auto l : m_lignes) if (l.EstComplete(joueur, NB_LIGNES)) return true;
    for(auto c : m_colonnes) if (c.EstComplete(joueur)) return true;
    for(auto d : m_diagonales) if (d.EstComplete(joueur, NB_LIGNES)) return true;
    return false;
}

std::string GrillePuissance4::EnTexte() const{
    std::string grilleEnTexte="";
    for(Ligne l : m_lignes)
    {
        grilleEnTexte = grilleEnTexte + l.EnTexte() + "\n";
    }
    for(int i=0;i<NB_COLONNES;i++){
        grilleEnTexte = grilleEnTexte + "-----";
    }
    return grilleEnTexte;
}
