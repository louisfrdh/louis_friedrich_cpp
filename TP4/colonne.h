#ifndef COLONNE_H
#define COLONNE_H

#include "ensemblelineairecases.h"
#include <vector>
#include <memory>

class Colonne : public EnsembleLineaireCases
{
public:
    void Afficher() const override;
    std::string EnTexte() const override;
};

#endif // COLONNE_H
