#include "grille.h"
#include <iostream>

Grille::Grille(int nbLignes, int nbColonnes, int nbJoueurs, std::string gameName) :
    m_nbLignes(nbLignes), m_nbColonnes(nbColonnes), m_nbJoueurs(nbJoueurs),m_name(gameName)
{}

bool Grille::CaseEstVide(int x, int y) const
{
    return m_lignes[x].CaseEstVide(y);
}

bool Grille::EstPleine() const
{
    for(auto c : m_cases)
    {
        if (c->EstVide()) return false;
    }
    return true;
}

void Grille::Afficher() const
{
    for(Ligne l : m_lignes)
    {
        l.Afficher();
    }
    for(int i=0; i<(int)m_colonnes.size(); i++) std::cout << "--";
    std::cout << std::endl << std::endl;
}

void Grille::initialiseVectors()
{
    m_lignes.assign(m_nbLignes, Ligne());
    m_colonnes.assign(m_nbColonnes, Colonne());
    m_diagonales.assign((m_nbLignes+m_nbColonnes-1)*2, Diagonale());
}

void Grille::initialiseCases()
{
    int g = m_nbLignes-1 + m_nbLignes-1 + m_nbColonnes;
    for(int i=0; i<m_nbLignes; i++)
    {
        for(int j=0; j<m_nbColonnes; j++)
        {
            std::shared_ptr<CaseMonoPion> sptrCase = std::make_shared<CaseMonoPion>(CaseMonoPion());
            m_cases.push_back(sptrCase);
            m_lignes[i].AjouterCase(sptrCase);
            m_colonnes[j].AjouterCase(sptrCase);
            m_diagonales[i+j].AjouterCase(sptrCase);
            m_diagonales[g+j].AjouterCase(sptrCase);
        }
        g--;
    }
}

int Grille::demanderIndiceCase(std::shared_ptr<Joueur> joueur, std::string ligneOuColonne)
{
    int x;
    std::cout << joueur->RetourneNom() << " renseignez la "<< ligneOuColonne <<
              " dans laquelle vous souhaitez jouer (entre 0 et " << RetourneNbColonnes()-1
              << ") >>>" << std::endl;
    std::cin >> x;
    return x;
}

std::string Grille::getSaveString(){
    std::string nbPlayers=std::to_string(RetourneNbJoueurs());

    return getGameName()+","+nbPlayers+","+getGridDescription();
}


std::string Grille::getGridDescription() {
    std::string description = "";

    for(auto l : m_lignes){
        description+=l.getLineDescription()+",";
    }
    description = description.substr (0,description.size()-1);
    return description;
}

std::string Grille::getGameName() {
    return m_name;
}
