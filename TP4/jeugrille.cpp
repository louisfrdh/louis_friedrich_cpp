#include "jeugrille.h"
#include "fileManager.h"

JeuGrille::JeuGrille(int nbJoueurs)
{
    for(int i=1 ; i<=nbJoueurs ; i++)
    {
        std::string nom;
        std::cout << "Renseignez le nom du joueur "<< i <<
                     " >>>" << std::endl;
        std::cin >> nom;
        m_joueurs.push_back(std::make_shared<Joueur>(Joueur(i, nom)));
    }
    std::cout << "DEBUT DE LA PARTIE" << std::endl << std::endl;
}

void JeuGrille::Jouer()
{
    std::cout << "Debut du jeu !" << std::endl;
    while(!m_grille->EstPleine())
    {
        for(auto j : m_joueurs)
        {
            //m_grille->Afficher();
            AfficheurDame::Affiche(&*m_grille);
            tourJoueur(j);
            if(testFin()) return ;
        }
    }
}

void JeuGrille::tourJoueur(std::shared_ptr<Joueur> joueur)
{
    m_grille->TourJoueur(joueur);
    m_joueurCourant = joueur;

    std::string saveString = getSaveString();
    std::string path = "text.grid.txt";
        if (path.length() > 0) {
        std::ofstream file;
        file.open(path);
        file << saveString;
        file.close();
    }
}

bool JeuGrille::testFin() const
{
    for(auto j : m_joueurs)
    {
        if (m_grille->TestVictoireJoueur(j))
        {
            std::cout << std::endl;
            std::cout << j->RetourneNom() << " remporte la partie !" << std::endl;
            return true;
        }
    }
    if(m_grille->EstPleine())
    {
        std::cout << std::endl;
        std::cout <<"C'est un match nul !" << std::endl;
        return true;
    }
    return false;
}

std::string JeuGrille::getSaveString(){
    std::string saveString ="";
    if (m_joueurCourant != NULL && m_grille != NULL)
    {
        saveString=m_grille->getSaveString()+","+m_joueurCourant->getSaveString();
    }
    
    return saveString;
}