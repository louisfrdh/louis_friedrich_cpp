#ifndef GRILLEPUISSANCE4_H
#define GRILLEPUISSANCE4_H

#include "grille.h"

class GrillePuissance4 : public Grille
{
public:
    const static int NB_COLONNES = 7;
    const static int NB_LIGNES = 4;
    const static int NB_JOUEURS = 2;
    GrillePuissance4();
    void TourJoueur(std::shared_ptr<Joueur> joueur) override;
    bool TestVictoireJoueur(std::shared_ptr<Joueur> joueur) const override;
    std::string EnTexte() const override;
private:
    void DeposerJeton(int i, std::shared_ptr<Joueur> joueur);
};

#endif // GRILLEPUISSANCE4_H
