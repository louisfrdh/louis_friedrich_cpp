#ifndef JEUGRILLE_H
#define JEUGRILLE_H

#include <vector>
#include "jeu.h"
#include "afficheurmorpion.h"
#include "afficheurdame.h"

class JeuGrille : Jeu
{
public:
    JeuGrille(int nbJoueurs);
    inline void AttribuerGrille(std::shared_ptr<Grille> grille) { m_grille = grille; };
    virtual void Jouer() override;
    inline std::shared_ptr<Joueur> RetourneJoueur(int id) const { return m_joueurs[id-1]; };
    std::string getSaveString();
private:
    std::shared_ptr<Grille> m_grille;
    std::vector<std::shared_ptr<Joueur>> m_joueurs;
    std::shared_ptr<Joueur> m_joueurCourant;
    void tourJoueur(std::shared_ptr<Joueur> joueur);
    bool testFin() const override;
};

#endif // JEUGRILLE_H
