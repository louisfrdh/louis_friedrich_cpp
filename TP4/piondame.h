#ifndef PIONDAME_H
#define PIONDAME_H

#include "pion.h"

class PionDame : public Pion
{
public:
    PionDame(std::shared_ptr<Joueur> joueur);
    bool EstUneDame = false;
    void Afficher() const override;
    std::string EnTexte() const override;
};

#endif // PIONDAME_H
