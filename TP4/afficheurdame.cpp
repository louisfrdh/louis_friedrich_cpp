#include "afficheurdame.h"

int AfficheurDame::DemandeLigne(std::shared_ptr<Joueur> UnJoueur){
    int choix;
    bool choixNonconforme =true;
    while(choixNonconforme){
        choix = AttendUnEntier(UnJoueur->RetourneNom() + ",merci de saisir un numero de ligne");
        if ((choix >=1) && (choix <= 10)) choixNonconforme = false;
        else InformeUtilisateurDe(">>Etes vous sur de saisir un chiffre entre 1 et 10?");
    }
    choix = choix - 1; // convertit en la valeur correspondante pour le programme
    return choix;
}
int AfficheurDame::DemandeColonne(std::shared_ptr<Joueur> UnJoueur){
    std::string choix;
    bool choixNonconforme =true;
    while(choixNonconforme){
        choix = AttendUnMot(UnJoueur->RetourneNom() + ",merci de saisir une lettre pour les colonnes ");
        if ((choix.length()==1) && ((choix[0] <='a' || choix[0] <='j')||(choix[0] <='A' || choix[0] <='J')))
            choixNonconforme = false;
        else InformeUtilisateurDe(">>Etes vous sur de saisir une lettre entre A et J?");
    }
    return ConversionColonne(choix[0]);// convertit en la valeur correspondante pour le programme
}

int AfficheurDame::ConversionColonne(char lettreDeColonne){ // ASCII CONVERSION
    int x = static_cast<int>(std::toupper(lettreDeColonne) - 'A' + 1);
    return x-1;
}

char AfficheurDame::ConversionLettreColonne(int numeroColonne){ // ASCII CONVERSION
     switch(numeroColonne){
        case 0: return 'A';
            break;
        case 1: return 'B';
            break;
        case 2: return 'C';
            break;
        case 3: return 'D';
             break;
        case 4: return 'E';
            break;
        case 5: return 'F';
            break;
        case 6: return 'G';
            break;
        case 7: return 'H';
            break;
        case 8: return 'I';
            break;
        case 9: return 'J';
            break;
        default :return -1;
     }

    char x = numeroColonne;
     return x;
}

void AfficheurDame::Affiche(Grille* grille){
    NettoyageConsole();

    std::cout << "=== [DAME] ===" << std::endl<< std::endl;
    std::cout << "| A || B || C || D || E || F || G || H || I || J |" <<std::endl;
    std::cout << grille->EnTexte() << std::endl;
    std::cout << "--------------------------------" << std::endl;
}
