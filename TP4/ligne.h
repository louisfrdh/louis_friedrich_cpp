#ifndef LIGNE_H
#define LIGNE_H

#include "ensemblelineairecases.h"
#include <vector>
#include <memory>

class Ligne : public EnsembleLineaireCases
{
public:
    void Afficher() const override;
    std::string EnTexte() const override;
    std::string getLineDescription();
private:
};

#endif // LIGNE_H
