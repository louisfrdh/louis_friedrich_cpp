TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        afficheurdame.cpp \
        afficheurmorpion.cpp \
        afficheurpuissance4.cpp \
        casemonopion.cpp \
        colonne.cpp \
        diagonale.cpp \
        ensemblelineairecases.cpp \
        grille.cpp \
        grilledame.cpp \
        grillemorpion.cpp \
        grillepuissance4.cpp \
        jeugrille.cpp \
        ligne.cpp \
        main.cpp \
        outilsconsole.cpp \
        piondame.cpp \
        pionmorpion.cpp \
        pionpuissance4.cpp \
        pionsuperdame.cpp

HEADERS += \
    afficheurdame.h \
    autorisedeplacementpions.h \
    case.h \
    afficheurmorpion.h \
    afficheurpuissance4.h \
    casemonopion.h \
    colonne.h \
    diagonale.h \
    echantillonsDeCode.h \
    ensemblelineairecases.h \
    grille.h \
    grilledame.h \
    grillemorpion.h \
    grillepuissance4.h \
    jeu.h \
    jeugrille.h \
    joueur.h \
    ligne.h \
    outilsconsole.h \
    pion.h \
    piondame.h \
    pionmorpion.h \
    pionpuissance4.h \
    pionsuperdame.h
