#include "outilsconsole.h"
#include "jeugrille.h"
#include "grillepuissance4.h"
#include "grilledame.h"

int main()
{
    JeuGrille jeu = JeuGrille(2);
    int choixJeu;
    do
    {
        OutilsConsole::NettoyageConsole();
        OutilsConsole::InformeUtilisateurDe("A quel jeu souhaitez vous jouer ?");
        OutilsConsole::InformeUtilisateurDe("1 - Morpion");
        OutilsConsole::InformeUtilisateurDe("2 - Puissance 4");
        OutilsConsole::InformeUtilisateurDe("3 - Dame");
        OutilsConsole::InformeUtilisateurDe("0 - Quitter");
        choixJeu = OutilsConsole::AttendUnEntier();
    } while(choixJeu < 0 || choixJeu > 5);
    switch(choixJeu)
    {
        case 1:
            jeu.AttribuerGrille(std::make_shared<GrilleMorpion>(GrilleMorpion()));
            break;
        case 2:
            jeu.AttribuerGrille(std::make_shared<GrillePuissance4>(GrillePuissance4()));
            break;
        case 3:
            jeu.AttribuerGrille(std::make_shared<GrilleDame>
                                (GrilleDame(jeu.RetourneJoueur(1), jeu.RetourneJoueur(2))));
            break;
        case 0:
            return 1;
    }
    jeu.Jouer();
    main();
    return 0;
}
