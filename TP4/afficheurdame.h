#ifndef AFFICHEURDAME_H
#define AFFICHEURDAME_H

#include "outilsconsole.h"
#include "grilledame.h"

class AfficheurDame:  OutilsConsole {
public:
    static int DemandeLigne(std::shared_ptr<Joueur> UnJoueur);
    static int DemandeColonne(std::shared_ptr<Joueur> UnJoueur);
    static void Affiche(Grille* grille);
    static int ConversionColonne(char lettreDeColonne);
    static char ConversionLettreColonne(int numeroColonne);
};

#endif // AFFICHEURDAME_H
