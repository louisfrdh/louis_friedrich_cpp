#ifndef DIAGONALE_H
#define DIAGONALE_H

#include "ensemblelineairecases.h"
#include <vector>
#include <memory>

class Diagonale : public EnsembleLineaireCases
{
public:
    void Afficher() const override;
    std::string EnTexte() const override;
private:
};

#endif // DIAGONALE_H
